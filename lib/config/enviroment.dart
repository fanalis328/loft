enum Enviroment { dev, prod }

abstract class Constants {
  static Enviroment _env = _getDefaultEnviroment();

  static Enviroment get env => _env;

  static Enviroment _getDefaultEnviroment() {
    const bool isProduction = bool.fromEnvironment('dart.vm.product');
    return isProduction ? Enviroment.prod : Enviroment.dev;
  }

  static setEnviroment(Enviroment enviroment) {
    _env = enviroment;
  }
}
