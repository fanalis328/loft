import 'enviroment.dart';

final _devBaseUrl = '://api.dev.denimapp.ru';
final _prodBaseUrl = '://api.denimapp.ru';
final _apiVersion = '/v0/ws';

final _uploadImageEndpoint = '/v0/file/u/';
final _downloadImageEndpoint = '/v0/file/s/';

class AppConfig {
  final String serverAdress;
  final String baseUrl;
  final String uploadImageEndpoint;
  final String downloadImageEndpoint;
  AppConfig({
    this.serverAdress,
    this.baseUrl,
    this.uploadImageEndpoint,
    this.downloadImageEndpoint,
  });
}

final _prodConfig = AppConfig(
    baseUrl: 'https' + _prodBaseUrl,
    serverAdress: 'ws' + _prodBaseUrl + _apiVersion,
    uploadImageEndpoint: _uploadImageEndpoint,
    downloadImageEndpoint: _downloadImageEndpoint);

final _devConfig = AppConfig(
    baseUrl: 'https' + _devBaseUrl,
    serverAdress: 'wss' + _devBaseUrl + _apiVersion,
    uploadImageEndpoint: _uploadImageEndpoint,
    downloadImageEndpoint: _downloadImageEndpoint);

final appConfig = Constants.env == Enviroment.prod ? _prodConfig : _devConfig;
