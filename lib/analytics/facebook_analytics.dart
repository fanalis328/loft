import 'package:facebook_app_events/facebook_app_events.dart';
import 'facebook_analytics_interface.dart';

class FacebookAnalytics implements IFacebookAnalytics {
  final FacebookAppEvents facebookAppEvents = FacebookAppEvents();
  //  Event names
  final String evenInitiatedCheckout = "EVENT_NAME_INITIATED_CHECKOUT";
  final String eventNameSearched = "EVENT_NAME_SEARCHED";
  final String eventViewedContent = "EVENT_NAME_VIEWED_CONTENT";
  final String eventCompletedRegistrationM =
      "EVENT_NAME_COMPLETED_REGISTRATION";
  final String eventSubmitApplicationW = "EVENT_NAME_SUBMIT_APPLICATION";

  final String standardEventCompletedRegistration =
      FacebookAppEvents.eventNameCompletedRegistration;
  final String standardEventViewedContent =
      FacebookAppEvents.eventNameViewedContent;

  void sendFacebookAnalyticsEvent(String name) async {
    facebookAppEvents.logEvent(name: name);
    print('Facebook event succeeded');
  }

  void sendFacebookAnalyticsEventWithParams(
      String name, double value, String currency) async {
    facebookAppEvents.logEvent(
        name: name, parameters: {'value': value, 'currency': currency});
    print('Facebook event with params succeeded');
  }

  void activatedAppEvent() {
    facebookAppEvents.logActivatedApp();
    print('Facebook event app activetad');
  }
}
