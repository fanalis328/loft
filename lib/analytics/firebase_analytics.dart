import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'firebase_analytics_interface.dart';

class FirebaseAnalyticsImpl implements IFirebaseAnalytics {
  FirebaseAnalyticsImpl() {
    _observer = FirebaseAnalyticsObserver(analytics: _analytics);
  }

  FirebaseAnalytics _analytics = FirebaseAnalytics();
  FirebaseAnalyticsObserver _observer;

  FirebaseAnalyticsObserver get observer => _observer;

  Future<void> sendAnalyticsEvent(String name, [Map parameters]) async {
    await _analytics.logEvent(
      name: name,
      parameters: parameters,
    );
    print('logEvent succeeded');
  }

  Future<void> logAppOpen() async {
    await _analytics.logAppOpen();
    print('logAppOpen logAppOpen');
  }

  Future<void> logLogin() async {
    await _analytics.logLogin();
    print(['logLogin']);
  }
}
