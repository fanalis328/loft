import 'package:appmetrica_sdk/appmetrica_sdk.dart';
import 'package:loft/analytics/yandex_analytics_interface.dart';

class YandexAnalytics implements IYandexAnalytics {
  //  Event names
  final String eventRegistration = "registration";
  final String eventLogin = "log_in";
  final String eventPurchase = "purchase";
  final String eventRegStep4M = "reg_step_4_M";
  final String eventRegStep4W = "reg_step_4_W";
  final String eventPurchaseSuccess = "purchase_success";
  final String eventRegStep9PhotoconfW = "reg_step_9_photoconf_W";
  final String eventRegStep10proSuccessM = "reg_step_10_pro_success_M";
  final String eventRegStep10proSuccessW = "reg_step_10_pro_success_W";
  final String eventRegSuccessM = "reg_success_M";
  final String eventRegSuccessW = "reg_success_W";

  void sendYandexAnalyticsEventWithParams(String name, [Map parameters]) async {
    AppmetricaSdk().reportEvent(name: name, attributes: parameters);
    print('Yandex event with params succeeded');
  }

  void sendYandexAnalyticsEvent(String name) async {
    AppmetricaSdk().reportEvent(name: name);
    print('Yandex event succeeded');
  }

  @override
  Future<void> initialize() {
    // LOFT
    return AppmetricaSdk()
        .activate(apiKey: '99cdab6e-6af8-402d-bd08-3b99f677a143');
  }

  @override
  Future<String> getLibraryVersion() {
    return AppmetricaSdk().getLibraryVersion();
  }
}
