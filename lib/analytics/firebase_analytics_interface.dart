abstract class IFirebaseAnalytics {
  get observer;

  Future<void> sendAnalyticsEvent(String name, [Map parameters]);

  Future<void> logAppOpen();

  Future<void> logLogin();
}
