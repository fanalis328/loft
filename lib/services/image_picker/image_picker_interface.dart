import 'package:image_picker/image_picker.dart';
import 'image_file_model.dart';

abstract class IImagePicker {
  Future<ImageFile> pickImage({ImageSource source});
}
