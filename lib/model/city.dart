import 'package:equatable/equatable.dart';

class GetCities {
  int id;
  String method;

  GetCities({this.id, this.method = 'getCities'});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
    };
  }
}

class GetAllCitiesByCountry {
  int id;
  String method;
  GetAllCitiesByCountryData data;

  GetAllCitiesByCountry({
    this.id,
    this.method = 'getAllCities',
    this.data,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data?.toJson() ?? null,
    };
  }
}

class GetAllCitiesByCountryData {
  String countryId;

  GetAllCitiesByCountryData({this.countryId});

  Map<String, dynamic> toJson() {
    return {
      'country': countryId,
    };
  }
}

class GetCountries {
  int id;
  String method;

  GetCountries({this.id, this.method = 'getCountries'});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
    };
  }
}

class Country {
  String id;
  String title;

  Country({this.id, this.title});

  static Country fromJson(Map<String, dynamic> json) {
    return Country(
      id: json['id'],
      title: json['title'],
    );
  }
}

class City extends Equatable {
  String id;
  String title;
  String region;
  String country;

  City({
    this.id,
    this.title,
    this.region,
    this.country,
  }) : super([id, title, region, country]);

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'];
    region = json['region'];
    country = json['country'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'region': region,
      'country': country,
    };
  }
}

class Cities {
  List<City> listCity;
  Cities({this.listCity});

  static Cities fromJson(Map<String, dynamic> json) {
    return Cities(
      listCity: json['listCity']?.cast<City>(),
    );
  }
}
