import 'package:equatable/equatable.dart';

class PhoneCode extends Equatable {
  String code;
  String name;

  PhoneCode({
    this.code,
    this.name,
  });

  PhoneCode.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
  }
}
