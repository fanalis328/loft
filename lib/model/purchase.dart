import 'package:loft/data/app.dart';

class PurchaseItem {
  int id;
  int position;
  bool isSelected;
  String productId;
  String tooltip;
  int units;
  String price;
  String title;
  String subtitle;
  String subtitle2;

  PurchaseItem({
    this.id,
    this.position,
    this.isSelected = false,
    this.productId = '',
    this.tooltip = '',
    this.units,
    this.price = '',
    this.title = '',
    this.subtitle = '',
    this.subtitle2 = '',
  });
}


class UpdatePurchase {
  int id;
  String method;
  UpdatePurchaseData data;

  UpdatePurchase({
    this.id,
    this.method = 'updatePurchase',
    this.data,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data.toJson(),
    };
  }
} 

class UpdatePurchaseData {
  String owner;
  String token;
  String product;

  UpdatePurchaseData({
    this.owner,
    this.token,
    this.product,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': owner,
      'token': token,
      'product': product,
    };
  }

  static UpdatePurchaseData fromJson(Map<String, dynamic> json) {
    return UpdatePurchaseData(
      owner: json == null ? null : json['owner'],
      product: json == null ? null : json['product'],
      token: json == null ? null : json['token'],
    );
  }
}


class TrialPurchaseStatus {
  String owner;
  bool status;
  
  TrialPurchaseStatus({
    this.owner,
    this.status = false,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': owner,
      'status': status,
    };
  }

  static TrialPurchaseStatus fromJson(Map<String, dynamic> json) {
    return TrialPurchaseStatus(
      owner: json['owner'],
      status: json['status'],
    );
  }
}