//-----------------------------------------
// GET ACCOUNT
//-----------------------------------------

import 'package:loft/services/image_picker/image_file_model.dart';

class GetAccount {
  int id;
  String method;

  GetAccount({
    this.id,
    this.method = 'getAccount',
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
    };
  }
}

//-----------------------------------------
// UPDATE
//-----------------------------------------

class UpdateAccount {
  int id;
  String method;
  UpdateAccountData data;

  UpdateAccount({
    this.id,
    this.method = 'updateAccount',
    this.data,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data.toJson(),
    };
  }
}

class UpdateAccountData {
  Map profile;
  ProfileSettings settings;
  Preferences preferences;

  UpdateAccountData({
    this.profile,
    this.settings,
    this.preferences,
  });

  Map<String, dynamic> toJson() {
    return {
      'profile': profile,
      'settings': settings?.toJson() ?? null,
      'preferences': preferences?.toJson() ?? null,
    };
  }
}

// ______________METHODS END_______________

//-----------------------------------------
// PROFILE
//-----------------------------------------

class ProfileData {
  bool blocked;
  bool deleted;
  String id;
  int moderated;
  Profile profile;
  ProfileSettings settings;
  String registered;
  String subscription;
  Preferences preferences;

  ProfileData({
    this.blocked,
    this.deleted,
    this.id,
    this.moderated,
    this.profile,
    this.settings,
    this.registered,
    this.subscription,
    this.preferences,
  });

  Map<String, dynamic> toJson() {
    return {
      'blocked': blocked,
      'deleted': deleted,
      'id': id,
      'moderated': moderated,
      'profile': profile == null ? null : profile.toJson(),
      'settings': settings == null ? null : settings.toJson(),
      'registered': registered,
      'subscription': subscription,
      'preferences': preferences == null ? null : preferences.toJson(),
    };
  }

  static ProfileData fromJson(Map<String, dynamic> json) {
    return ProfileData(
      blocked: json['blocked'],
      deleted: json['deleted'],
      id: json['id'],
      moderated: json['moderated'],
      profile:
          json['profile'] == null ? null : Profile.fromJson(json['profile']),
      settings: json['settings'] == null
          ? null
          : ProfileSettings.fromJson(json['settings']),
      registered: json['registered'] == null || json['registered'] == ''
          ? null
          : json['registered'],
      subscription: json['subscription'],
      preferences: json['preferences'] == null
          ? null
          : Preferences.fromJson(json['preferences']),
    );
  }
}

class UserLocation {
  String title;
  String id;

  UserLocation({this.id, this.title});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
    };
  }

  static UserLocation fromJson(Map<String, dynamic> json) {
    return UserLocation(id: json['id'], title: json['title']);
  }
}

class EditProfileLocation {
  UserLocation country;
  UserLocation city;

  EditProfileLocation({
    this.country,
    this.city,
  });
}

class Profile {
  String name;
  int gender;
  int birthday;
  int height;
  int weight;
  int online;
  UserLocation citizenship;
  UserLocation country;
  UserLocation city;
  int zodiac;
  String about;
  UserPhoto avatar;
  List<UserPhoto> photos;
  List<int> albums;

  Profile({
    this.name,
    this.gender,
    this.birthday,
    this.height,
    this.weight,
    this.citizenship,
    this.country,
    this.city,
    this.zodiac,
    this.about,
    this.avatar,
    this.photos,
    this.albums,
    this.online,
  });

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'gender': gender,
      'birthday': birthday,
      'height': height,
      'weight': weight,
      'citizenship': citizenship?.toJson() ?? null,
      'country': country?.toJson() ?? null,
      'city': city?.toJson() ?? null,
      'zodiac': zodiac,
      'about': about,
      'online': online,
      'avatar': avatar?.toJson(),
      'photos': photos?.map((item) => item.toJson())?.toList() ?? null,
      'albums': albums?.map((album) => album)?.toList() ?? null,
    };
  }

  static Profile fromJson(Map<String, dynamic> json) {
    return Profile(
      name: json['name'],
      gender: json['gender'],
      birthday: json['birthday'],
      height: json['height'],
      weight: json['weight'],
      citizenship: json['citizenship'] == null
          ? null
          : UserLocation.fromJson(json['citizenship']),
      country: json['country'] == null
          ? null
          : UserLocation.fromJson(json['country']),
      city: json['city'] == null ? null : UserLocation.fromJson(json['city']),
      zodiac: json['zodiac'],
      about: json['about'],
      online: json['online'],
      avatar:
          json['avatar'] == null ? null : UserPhoto.fromJson(json['avatar']),
      photos: json['photos'] == null
          ? null
          : List<UserPhoto>.from(
              json['photos']?.map((item) => UserPhoto.fromJson(item))),
      albums: json['albums']?.cast<int>(),
    );
  }
}

//-----------------------------------------
// Preferences
//-----------------------------------------

class Preferences {
  int gender;

  Preferences({this.gender});

  Map<String, dynamic> toJson() {
    return {
      'gender': gender,
    };
  }

  static Preferences fromJson(Map<String, dynamic> json) {
    return Preferences(
      gender: json['gender'] == null ? null : json['gender'],
    );
  }
}

//-----------------------------------------
// CITIZENSHIP
//-----------------------------------------

class Citizenship {
  int id;
  String title;

  Citizenship({
    this.id,
    this.title,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
    };
  }

  static Citizenship fromJson(Map<String, dynamic> json) {
    return Citizenship(
      id: json['id'],
      title: json['title'],
    );
  }
}
//-----------------------------------------
// SHORT USER PROFILE
//-----------------------------------------

class ShortUserProfile {
  String id;
  String name;
  int age;
  UserPhoto avatar;
  String country;
  String city;
  int photosCount;
  int online;
  int gender;
  bool blocked;
  bool deleted;

  ShortUserProfile({
    this.id,
    this.name,
    this.age,
    this.avatar,
    this.country,
    this.city,
    this.photosCount,
    this.online,
    this.gender,
    this.blocked,
    this.deleted,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'age': age,
      'avatar': avatar?.toJson(),
      'country': country,
      'city': city,
      'photosCount': photosCount,
      'online': online,
      'gender': gender,
      'blocked': blocked,
      'deleted': deleted,
    };
  }

  static ShortUserProfile fromJson(Map<String, dynamic> json) {
    return ShortUserProfile(
      id: json['id'],
      name: json['name'],
      age: json['age'],
      avatar:
          json['avatar'] == null ? null : UserPhoto.fromJson(json['avatar']),
      country: json['country'],
      city: json['city'],
      photosCount: json['photosCount'],
      online: json['online'],
      gender: json['gender'],
      blocked: json['blocked'],
      deleted: json['deleted'],
    );
  }
}

//-----------------------------------------
// PROFILE SETTINGS
//-----------------------------------------

class ProfileSettings {
  bool hideHeight;
  bool hideWeight;
  bool pushMsg;
  bool pushView;

  ProfileSettings({
    this.hideHeight = false,
    this.hideWeight = false,
    this.pushMsg = true,
    this.pushView = true,
  });

  Map<String, dynamic> toJson() {
    return {
      'hideHeight': hideHeight,
      'hideWeight': hideWeight,
      'pushMsg': pushMsg,
      'pushView': pushView,
    };
  }

  static ProfileSettings fromJson(Map<String, dynamic> json) {
    return ProfileSettings(
      hideHeight: json['hideHeight'],
      hideWeight: json['hideWeight'],
      pushMsg: json['pushMsg'],
      pushView: json['pushView'],
    );
  }
}

//-----------------------------------------
// USER PHOTO
//-----------------------------------------

class UserPhoto {
  String id;
  int seqID;
  String preview;
  ImageFile original;
  int moderated;
  bool blocked;

  UserPhoto({
    this.id,
    this.seqID,
    this.preview,
    this.original,
    this.moderated,
    this.blocked,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'seqID': seqID,
      'preview': preview,
      'original': original.path,
      'moderated': moderated,
      'blocked': blocked,
    };
  }

  static UserPhoto fromJson(Map<String, dynamic> json) {
    return UserPhoto(
      id: json['id'],
      seqID: json['seqID'],
      preview: json['preview'],
      original:
          json['original'] == null ? null : ImageFile(path: json['original']),
      moderated: json == null ? null : json['moderated'],
      blocked: json['blocked'],
    );
  }
}
