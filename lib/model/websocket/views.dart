import 'dart:convert';

import 'package:loft/model/websocket/account.dart';

class ShortViewUserProfile {
  String id;
  String name;
  int age;
  UserPhoto avatar;
  String country;
  String city;
  int photosCount;
  int online;
  String time;
  int gender;
  bool blocked;
  bool deleted;

  ShortViewUserProfile({
    this.id,
    this.name,
    this.age,
    this.avatar,
    this.country,
    this.city,
    this.photosCount,
    this.online,
    this.time,
    this.gender,
    this.blocked = false,
    this.deleted = false,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'age': age,
      'avatar': avatar?.toJson(),
      'country': country,
      'city': city,
      'photosCount': photosCount,
      'online': online,
      'time': time,
      'gender': gender,
      'blocked': blocked,
      'deleted': deleted,
    };
  }

  static ShortViewUserProfile fromJson(Map<String, dynamic> json) {
    return ShortViewUserProfile(
      id: json['id'],
      name: json['name'],
      age: json['age'],
      avatar: json['avatar'] == null ? null : UserPhoto.fromJson(json['avatar']),
      country: json['country'],
      city: json['city'],
      photosCount: json['photosCount'],
      online: json['online'],
      time: json['time'],
      gender: json['gender'],
      blocked: json['blocked'],
      deleted: json['deleted'],
    );
  }
}

class ViewsUsersList {
  final List<ShortViewUserProfile> users;
  ViewsUsersList({this.users = const []});

  static ViewsUsersList fromJson(usersData) {
    var usersMapData = json.decode(usersData);
    return ViewsUsersList(
      users: usersMapData == null ? [] : List<ShortViewUserProfile>.from(usersMapData.map((item) => ShortViewUserProfile.fromJson(item))),
    );
  }
}