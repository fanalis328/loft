import 'dart:convert';
import 'package:loft/data/app.dart';
import 'package:loft/model/websocket/account.dart';

class SearchFilters {
  static UserLocation myCity =
      App.me.user.profile.city.id == '1' || App.me.user.profile.city.id == '2'
          ? App.me.user.profile.city
          : null;

  int offset = 0;
  int gender = searchUserByGenderPreferences(App.me.user.preferences?.gender);
  List<int> age;
  UserLocation country;
  UserLocation city = myCity;
  UserLocation citizenship;
  List<int> height;
  List<int> weight;
  int online;
  bool newUsers = false;
  int zodiac;

  static final SearchFilters _singleton = SearchFilters._internal();

  factory SearchFilters() {
    return _singleton;
  }

  SearchFilters._internal();

  static int searchUserByGenderPreferences(int genderPref) {
    switch (genderPref) {
      case 1:
        return 1;
        break;
      case 2:
        return 2;
        break;
      case 3:
        return null;
      default:
        return null;
    }
  }

  Map<String, dynamic> toJson() {
    return {
      'offset': offset,
      'gender': gender,
      'age': age,
      'country': country == null ? null : country.id,
      'city': city == null ? null : city.id,
      'citizenship': citizenship == null ? null : citizenship.id,
      'height': height,
      'weight': weight,
      'online': online,
      'new': newUsers == null ? false : newUsers,
      'zodiac': zodiac,
    };
  }
}

class SearchUsersList {
  final List<ShortUserProfile> users;
  SearchUsersList({this.users = const []});

  static SearchUsersList fromJson(usersData) {
    var usersMapData = json.decode(usersData);
    return SearchUsersList(
      users: usersMapData == null
          ? <ShortUserProfile>[]
          : List<ShortUserProfile>.from(
              usersMapData.map((item) => ShortUserProfile.fromJson(item))),
    );
  }
}
