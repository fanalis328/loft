class Login {
  int id;
  String method;
  LoginData data;

  Login({
    this.id,
    this.method = 'login',
    this.data
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
      'data': data.toJson(),
    };
  }
}

class LoginData {
  String scheme;
  var secret;

  LoginData({
    this.scheme,
    this.secret,
  });

  Map<String, dynamic> toJson() {
    return {
      'scheme': scheme,
      'secret': secret,
    };
  }
}


class Logout {
  int id;
  String method;

  Logout({
    this.id,
    this.method = 'logout',
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'method': method,
    };
  }
}