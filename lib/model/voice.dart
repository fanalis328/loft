import 'package:loft/data/app.dart';

class VoiceStatus {
  String owner;
  bool voiceStatus;

  VoiceStatus({
    this.owner,
    this.voiceStatus = false,
  });

  Map<String, dynamic> toJson() {
    return {
      'owner': App.me.user.id,
      'voiceStatus': voiceStatus,
    };
  }

  static VoiceStatus fromJson(Map<String, dynamic> json) {
    return VoiceStatus(
      owner: json == null ? null : json['owner'],
      voiceStatus: json == null ? null : json['voiceStatus'],
    );
  }
}