import 'dart:convert';
import 'dart:io';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/city.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import 'package:path_provider/path_provider.dart';

class UserApi {
  updateAccount(ProfileData profileData, [dynamic callback]) {
    UserLocation city = profileData.profile.city;
    UserLocation citizenship = profileData.profile.citizenship;
    UserLocation country = profileData.profile.country;

    Map userJson = profileData.profile?.toJson();
    if (profileData.profile != null) {
      userJson.remove('avatar');
      userJson.remove('photos');
      userJson.remove('zodiac');
      userJson['city'] = city?.id;
      userJson['citizenship'] = citizenship?.id;
      userJson['country'] = country?.id;
      userJson.remove('country');
    }

    userJson.removeWhere(
        (key, value) => value == null || value == '' || value == 0);

    App.me.sendLegacy(
      UpdateAccount(
        id: socket.requestId(),
        data: UpdateAccountData(
            profile: userJson,
            settings: profileData.settings,
            preferences: profileData.preferences),
      ).toJson(),
      callback,
    );
  }

  getAccount([callback]) {
    App.me.sendLegacy(GetAccount(id: socket.requestId()).toJson(), callback);
  }

  getCities([dynamic callback]) {
    App.me.sendLegacy(GetCities(id: socket.requestId()).toJson(), callback);
  }

  getAllCitiesByCountry(String countryId, [dynamic callback]) {
    App.me.sendLegacy(
        GetAllCitiesByCountry(
                id: socket.requestId(),
                data: GetAllCitiesByCountryData(countryId: countryId))
            .toJson(),
        callback);
  }

  getCity(String name, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'findCity',
      'data': {'city': name}
    }, callback);
  }

  getCountries([dynamic callback]) {
    App.me.sendLegacy(GetCountries(id: socket.requestId()).toJson(), callback);
  }

  deleteAccount(String reason, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'deleteAccount',
      'data': {'reason': reason},
    }, callback);
  }

  reportUser(String userId, String reportType, String userMessage,
      [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'reportUser',
      'data': {
        'user': userId,
        "payload": {"type": reportType, "message": userMessage}
      }
    }, callback);
  }

  Future uploadImage(ImageFile image, [dynamic errorCallback]) async {
    String url = appConfig.baseUrl + appConfig.uploadImageEndpoint;
    String token = await authDao.getToken();
    Map<String, String> headers = {'Authorization': 'token $token'};

    var request = http.MultipartRequest("POST", Uri.parse(url));

    final img = await _createMultipartFile(image);
    request.headers.addAll(headers);
    request.files.add(img);

    return await request.send().then((data) async {
      String responseDataStr = await data.stream.bytesToString();
      print(['____UPLOAD IMAGE___:', responseDataStr]);
      Map responseData = jsonDecode(responseDataStr);
      if (responseData['ctrl']['code'] == 200) {
        return responseData['data']['file_id'];
      } else {
        if (errorCallback != null) errorCallback();
      }
    }).catchError((error) {
      if (errorCallback != null) errorCallback();
    });
  }

  Future<File> downloadImage(String path, [dynamic errorCallback]) async {
    // not used
    // TODO: implement for web
    File file;
    String url = appConfig.baseUrl + appConfig.downloadImageEndpoint;
    String token = await authDao.getToken();
    Map<String, String> headers = {
      'Authorization': 'token $token',
    };
    String imageName = path.split("/").last;

    Uri uri = Uri.parse(url +
        '?file=$imageName&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}');

    try {
      final response = await http.get(
        uri,
        headers: headers,
      );
      if (response.statusCode == 200) {
        Directory tempDir = await getTemporaryDirectory();
        String tempPath = tempDir.path;
        file = new File('$tempPath/$imageName');
        await file.writeAsBytes(response.bodyBytes);
      } else {
        if (errorCallback != null) errorCallback();
      }
    } catch (error) {
      if (errorCallback != null) errorCallback();
    }

    return file;
  }

  validateUser(ImageFile image, [dynamic callback]) async {
    String fileId = await userApi.uploadImage(image);

    if (fileId != null) {
      App.me.sendLegacy(
        {
          'id': socket.requestId(),
          'method': 'validateUser',
          'data': {'id': fileId, 'num': 1}
        },
        callback,
      );
    }
  }

  deletePhoto(String fileId, [dynamic callback]) {
    App.me.sendLegacy(
      {
        'id': socket.requestId(),
        'method': 'deletePhoto',
        'data': {'id': fileId}
      },
      callback,
    );
  }

  setAvatar(String fileId, [dynamic callback]) {
    App.me.sendLegacy(
      {
        'id': socket.requestId(),
        'method': 'setAvatar',
        'data': {'id': fileId}
      },
      callback,
    );
  }

  addUserToSV([dynamic callback]) {
    App.me.sendLegacy(
      {
        'id': socket.requestId(),
        'method': 'addUserToSV',
      },
      callback,
    );
  }

  addPhoto(String fileId, [dynamic callback]) {
    App.me.sendLegacy(
      {
        'id': socket.requestId(),
        'method': 'addPhoto',
        'data': {'id': fileId}
      },
      callback,
    );
  }
}

Future<http.MultipartFile> _createMultipartFile(ImageFile file) {
  if (kIsWeb)
    return Future.value(http.MultipartFile.fromBytes(
        "file", base64.decode(file.data),
        filename: file.name));

  return http.MultipartFile.fromPath("file", file.path);
}
