import 'package:loft/data/app.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/purchase.dart';

class PurchaseApi {
  updatePurchase(UpdatePurchaseData purchaseData, [dynamic callback]) {
    App.me.sendLegacy(
        UpdatePurchase(id: socket.requestId(), data: purchaseData).toJson(),
        callback);
  }

  getPurchase([dynamic callback]) {
    App.me.sendLegacy(
        {'id': socket.requestId(), 'method': 'getPurchase'}, callback);
  }

  raiseProfile(String purchaseToken, [dynamic callback]) async {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'raiseProfile',
      "data": {"token": purchaseToken}
    }, callback);
  }

  getSearchPosition([dynamic callback]) {
    App.me.sendLegacy(
        {'id': socket.requestId(), 'method': 'getSearchPosition'}, callback);
  }

  getProducts([dynamic callback]) {
    App.me.sendLegacy(
        {'id': socket.requestId(), 'method': 'getProducts'}, callback);
  }

  buyOnlineSearch(String purchaseToken, [dynamic callback]) {
    App.me.sendLegacy({
      'id': socket.requestId(),
      'method': 'buyOnlineSearch',
      'data': {'token': purchaseToken}
    }, callback);
  }
}
