import 'package:loft/config/config.dart';
import 'package:loft/websocket/websocket_common.dart';
import 'package:web_socket_channel/html.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class WebSocketHtml extends WebSocketCommon {
  @override
  WebSocketChannel channelFactory() {
    return HtmlWebSocketChannel.connect(appConfig.serverAdress);
  }
}

WebSocketCommon getWebSocket() {
  return WebSocketHtml();
}
