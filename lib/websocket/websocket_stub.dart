import 'package:loft/websocket/websocket_common.dart';

WebSocketCommon getWebSocket() => throw UnsupportedError(
    'Cannot create a websocket, dart:html or dart:io required!');
