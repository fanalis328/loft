import 'package:loft/model/purchase.dart';
import 'package:loft/utils/interface/purchase_utils_interface.dart';
import 'package:in_app_purchase/in_app_purchase.dart';

class PurchaseUtilsWeb implements IPurchaseUtils {
  @override
  Future<void> afterProductPurchase(Map purchaseData, [callback]) {
    // TODO: implement afterProductPurchase
    // throw UnimplementedError();
  }

  @override
  void buyProduct(ProductDetails product) {
    // TODO: implement buyProduct
  }

  @override
  Future<void> onlineSearchPurchaseCallback(Map purchaseData, [dynamic callback]) {
    // TODO: implement onlineSearchPurchaseCallback
  }
  
  @override
  Future<List<PurchaseDetails>> getPastPurchases() {
    // TODO: implement getPastPurchases
    // throw UnimplementedError();
  }

  @override
  Future<List<ProductDetails>> getProducts(List<String> productIdList) {
    // TODO: implement getProducts
    // throw UnimplementedError();
  }

  @override
  bool getPurchaseStatus(String purchaseExpiredTs) {
    // TODO: implement getPurchaseStatus
    // throw UnimplementedError();
    return false;
  }

  @override
  PurchaseDetails hasPurchased(
      String productID, List<PurchaseDetails> purchases) {
    // TODO: implement hasPurchased
    // throw UnimplementedError();
  }

  purchaseListener(context, String id, callback) {}

  @override
  Future<void> successPurchseDialog(context, [String title]) {
    // TODO: implement successPurchseDialog
    // throw UnimplementedError();
  }

  @override
  Future<void> updateCurrentPurchase() {
    // TODO: implement updateCurrentPurchase
    // throw UnimplementedError();
  }

  @override
  Future<void> checkAndUpdateCurrentDenimBlackPurchase(UpdatePurchaseData purchaseData) {
     // TODO: implement checkAndUpdateCurrentDenimBlackPurchase
  }
  
  @override
  InAppPurchaseConnection iap;
}
