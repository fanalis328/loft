import 'dart:convert';
import 'dart:io';
import 'package:compressimage/compressimage.dart';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/widgets/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:transparent_image/transparent_image.dart';

import '../locator.dart';

class ImageUtils {
  static Future<ImageFile> getPhotoFromCamera() async {
    final image = await imagePicker.pickImage(source: ImageSource.camera);
    return image;
  }

  static Future<ImageFile> getPhotoFromGallery() async {
    final image = await imagePicker.pickImage(source: ImageSource.gallery);
    return image;
  }

  static Future<ImageFile> saveImage(ImageFile image) async {
    // TODO: implement for web
    if (kIsWeb) return image;

    final directory = await getApplicationDocumentsDirectory();
    String imageName = image.path.split("/").last;
    File newImage = await File(image.path).copy('${directory.path}/$imageName');
    return ImageFile(path: newImage.path);
  }

  static Future<ImageFile> compressImage(ImageFile _imageFile) async {
    // TODO: implement for web
    if (kIsWeb) return _imageFile;

    await CompressImage.compress(imageSrc: _imageFile.path, desiredQuality: 80);
    return _imageFile;
  }

  static imageWidget(UserPhoto image, [int genderForMatch]) {
    ProfileData user = App.me.user;
    if (image == null || image?.original?.path == null)
      return Center(
          child: genderForMatch != null
              ? Image.asset(
                  genderForMatch == 1
                      ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                      : 'assets/images/icons/universal-userphoto-rounded-female.png',
                  fit: BoxFit.cover,
                )
              : Image.asset(
                  user.profile.gender == 1
                      ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                      : 'assets/images/icons/universal-userphoto-rounded-female.png',
                  fit: BoxFit.cover,
                ));

    if (image.original.data != null)
      return Image.memory(base64.decode(image.original.data),
          fit: BoxFit.cover);

    if (image.original.path.contains(appConfig.downloadImageEndpoint))
      return DImage(
        imageUrl:
            '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${image.preview != null ? image.preview.split("/").last : image.original.path.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
        httpHeaders: {'Authorization': 'token ${App.me.token}'},
        errorWidget: (context, url, error) => Center(
            child: genderForMatch != null
                ? Image.asset(
                    genderForMatch == 1
                        ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                        : 'assets/images/icons/universal-userphoto-rounded-female.png',
                    fit: BoxFit.cover,
                  )
                : Image.asset(
                    user.profile.gender == 1
                        ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                        : 'assets/images/icons/universal-userphoto-rounded-female.png',
                    fit: BoxFit.cover,
                  )),
        fit: BoxFit.cover,
      );

    if (image.original.path.contains('http'))
      return DImage(
        imageUrl: image.original.path,
        errorWidget: (context, url, error) => Center(
            child: genderForMatch != null
                ? Image.asset(
                    genderForMatch == 1
                        ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                        : 'assets/images/icons/universal-userphoto-rounded-female.png',
                    fit: BoxFit.cover,
                  )
                : Image.asset(
                    user.profile.gender == 1
                        ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                        : 'assets/images/icons/universal-userphoto-rounded-female.png',
                    fit: BoxFit.cover,
                  )),
        fit: BoxFit.cover,
      );

    return FadeInImage(
      placeholder: MemoryImage(kTransparentImage),
      image: FileImage(File(image.original.path)),
      fadeInDuration: Duration(milliseconds: 200),
      fit: BoxFit.cover,
    );
  }
}
