import 'dart:async';
import 'package:loft/data/app.dart';
import 'package:loft/db/user_dao.dart';
import 'package:loft/model/purchase.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/services/image_editor/image_editor_interface.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/services/navigation_service.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/validators.dart';
import 'package:loft/route_paths.dart' as routes;

import '../locator.dart';

class ProfileUtils {
  // _____________________ SERVER __________________________
  static logoutCallback(Map data) async {
    NavigationService _navigationService = locator<NavigationService>();
    if (data['ctrl']['code'] == 200) {
      await authDao.deleteCurrentUserId();
      await authDao.deleteToken();
      App.me.unreadMessageController.close();
      App.me.newViewsController.close();
      App.me.user = ProfileData();
      _navigationService.navigateTo(routes.HomeRoute);
      return;
    } else {
      return;
    }
  }

  // _____________________ lOCAL __________________________

  static String getAge(int birthDate) {
    // String age;
    if (birthDate == null) return '';
    DateTime birthDateTime =
        DateTime.fromMillisecondsSinceEpoch(birthDate * 1000);
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDateTime.year;
    int month1 = currentDate.month;
    int month2 = birthDateTime.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDateTime.day;
      if (day2 > day1) {
        age--;
      }
    }

    return age.toString();
  }

  static String getCleanBirthday(int birthDay) {
    if (birthDay == null) return '';
    DateTime fromTimestamp =
        DateTime.fromMillisecondsSinceEpoch(birthDay * 1000);
    String handleBirthday = fromTimestamp.toString();
    var cleanData = handleBirthday.split(' ').first;
    handleBirthday = cleanData;
    return handleBirthday;
  }

  static String getCleanPhone(String phone) {
    if (phone == null) return '';

    String cleanPhone;
    RegExp clearPhoneNumberExp = ValidatorUtils.clearPhoneNumberExp;
    cleanPhone = phone.replaceAll(clearPhoneNumberExp, '');

    return cleanPhone;
  }

  static Future<List<UserPhoto>> getUserImages() async {
    ProfileData user = App.me.user;
    List<UserPhoto> images = [];
    ProfileData response = await userDao.getCurrentUser(user);

    if (response != null) {
      response?.profile?.photos?.map((item) => images.add(item))?.toList();
    }

    return images;
  }

  static addImageToProfile(image, callback) async {
    String imageId = await userApi.uploadImage(image);
    if (imageId != null) {
      await ProfileUtils.addNewImageToPhotos(imageId, callback);
    }
  }

  static editPhoto(UserPhoto item, [dynamic callback]) async {
    ProfileData user = App.me.user;
    UserPhoto modifiedImage;

    ImageFile cropedImage =
        await locator.get<IImageEditor>().cropImage(item.original);
    if (cropedImage != null) {
      ProfileData userFromDb = await userDao.getCurrentUser(user);
      for (var img in userFromDb.profile.photos) {
        if (img.id == item.id) {
          img.original = ImageFile(path: cropedImage.path);
          modifiedImage = img;
        }
      }

      if (modifiedImage.id == userFromDb.profile.avatar.id) {
        user.profile.avatar = modifiedImage;
      }

      user.profile.photos = userFromDb.profile.photos;
      await userDao.updateUser(user);
      if (callback != null) callback(modifiedImage);
    }
  }

  static setLastModeratedPhotoOnAvatar([dynamic callback]) async {
    ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
    UserPhoto photoOnAvatar;

    if (currentUser == null ||
        currentUser.profile == null ||
        currentUser.profile.photos == null ||
        currentUser.profile.photos.length < 1) return;
    List<UserPhoto> moderatedPhotos = currentUser.profile.photos
        .where((item) => item.moderated == 1)
        .toList();
    if (moderatedPhotos.length == 0) {
      photoOnAvatar = currentUser.profile.photos.first;
    } else {
      photoOnAvatar = moderatedPhotos?.first;
    }

    userApi.setAvatar(photoOnAvatar.id, (Map data) async {
      if (data['ctrl']['code'] == 200) {
        App.me.user.profile.avatar = photoOnAvatar;
        await userDao.updateUser(App.me.user);
      }
    });
    if (callback != null) callback();
    return App.me.user;
  }

  static setAddedPhotoOnAvatar(UserPhoto item, [dynamic callback]) {
    userApi.setAvatar(item.id, (Map data) async {
      ProfileData profile = App.me.user;
      if (data['ctrl']['code'] == 200) {
        ProfileData userFromDb = await userDao.getCurrentUser(profile);
        var listWithNewAvatar =
            userFromDb.profile.photos.where((img) => img.id == item.id);
        profile.profile.avatar = listWithNewAvatar.first;
        await userDao.updateUser(profile);
        if (callback != null) callback();
      }
    });
  }

  static updateAvatar(String fileId,
      [dynamic callback, dynamic errorCallback]) async {
    ProfileUtils.addNewImageToPhotos(fileId, () {
      userApi.setAvatar(fileId, (Map data) async {
        if (data['ctrl']['code'] == 200) {
          ProfileData userFromDb = await userDao.getCurrentUser(App.me.user);
          if (userFromDb == null && errorCallback != null) errorCallback();
          UserPhoto avatar = userFromDb.profile.photos
              .firstWhere((image) => image.id == fileId);
          if (avatar != null) {
            userFromDb.profile.avatar = avatar;
          }
          App.me.user = userFromDb;
          await userDao.updateUser(userFromDb);
          if (callback != null) callback();
        } else {
          if (errorCallback != null) errorCallback();
        }
      });
    }, errorCallback);
  }

  static Future<void> registrationPhotosValidation() async {
    ProfileData user = App.me.user;
    ProfileData profile = await userDao.getCurrentUser(user);
    List<UserPhoto> profileImages = [];

    if (profile.profile.photos != null &&
        profile.profile.photos.length > 1 &&
        user.profile.avatar != null) {
      for (var item in profile?.profile?.photos) {
        if (item.id != user.profile.avatar.id) {
          userApi.deletePhoto(item.id);
        }
      }

      profileImages = profile.profile.photos
          .where((image) => image.id == user.profile.avatar.id)
          .toList();

      profile.profile.photos = profileImages;
      user.profile.photos = profileImages;
      profile.profile.avatar = user.profile.avatar;

      await userDao.updateUser(profile);
    }
  }

  static Future<void> downloadAndUpdateUserPhoto(UserPhoto image,
      [callback]) async {
    // ImageFile downloadedImage = await userApi.downloadImage(image.original);
    ImageFile downloadedImage;
    if (downloadedImage == null) return;
    ImageFile savedImage = await ImageUtils.saveImage(downloadedImage);
    if (savedImage == null) return;
    for (var item in App.me.user?.profile?.photos) {
      if (item.id == image.id) {
        item.original = ImageFile(path: savedImage.path);
        if (App.me.user?.profile?.avatar?.id == image.id) {
          App.me.user?.profile?.avatar?.original =
              ImageFile(path: savedImage.path);
          App.me.user?.profile?.avatar?.preview = savedImage.path;
        }
      }
    }
    await userDao.updateUser(App.me.user);
    if (callback != null) callback();
  }

  static addNewImageToPhotos(String fileId,
      [dynamic callback, dynamic errorCallback]) async {
    userApi.addPhoto(fileId, (Map data) async {
      if (data['ctrl']['code'] == 200) {
        await userApi.getAccount((Map profileData) async {
          ProfileData userFromDb = await userDao.getCurrentUser(App.me.user);
          if (profileData['ctrl']['code'] == 200) {
            ProfileData profile = ProfileData.fromJson(profileData['data']);
            UserPhoto currentImage =
                profile.profile.photos.firstWhere((item) => item.id == fileId);
            userFromDb.profile.photos == null
                ? userFromDb.profile.photos = [currentImage]
                : userFromDb.profile.photos.add(currentImage);
            App.me.user = userFromDb;
            await userDao.updateUser(userFromDb);
            if (callback != null) callback();
          } else {
            if (errorCallback != null) errorCallback();
            return;
          }
        });
      } else {
        if (errorCallback != null) errorCallback();
        return;
      }
    });
  }

  static deleteGalleryItem(List<UserPhoto> galleryItems, ProfileData user,
      UserDao _userDao, UserPhoto item,
      [callback, errorCallback]) {
    userApi.deletePhoto(item.id, (Map data) async {
      if (data['ctrl']['code'] == 200) {
        galleryItems.removeWhere((img) => img.id == item.id);
        user.profile.photos = galleryItems;
        await userDao.updateUser(user);
        setLastModeratedPhotoOnAvatar();
        if (callback != null) callback();
      } else {
        if (errorCallback != null) errorCallback();
      }
    });
  }

  static updateUserInstanceFromServer(Map data, [callback]) async {
    if (data != null && data['ctrl']['code'] == 200) {
      App.me.user = ProfileData.fromJson(data['data']);
      ProfileData currentUser = await userDao.getCurrentUser(App.me.user);

      if (currentUser == null) {
        await userDao.insertUser(App.me.user);
      } else {
        await userDao.updateUser(App.me.user);
      }

      if(App.me.user.subscription != null) {
        await purchaseDao.setTrialPurchaseStatus(TrialPurchaseStatus(owner: App.me.user.id, status: true));
      }
      if (callback != null) callback(App.me.user);
      return App.me.user;
    } else {
      return;
    }
  }

  static updateUserProfile([Map data, dynamic callback]) async {
    String userId = await authDao.getCurrentUserId();
    App.me.user.id = userId;
    ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
    if (data != null && data['ctrl']['code'] != 200) return;

    if (currentUser == null &&
        data != null &&
        data['ctrl']['code'] == 200 &&
        data['data'] != null) {
      await userDao.insertUser(ProfileData.fromJson(data['data']));
      return;
    }

    if (data == null && currentUser != null && currentUser.profile != null) {
      userApi.getAccount((Map userData) async {
        if (userData != null &&
            userData['ctrl']['code'] == 200 &&
            userData['data'] != null) {
          App.me.user = ProfileData.fromJson(userData['data']);
          if(App.me.user.subscription != null) {
            await purchaseDao.setTrialPurchaseStatus(TrialPurchaseStatus(owner: App.me.user.id, status: true));
          }
          if (App.me.user.profile.photos != null &&
              App.me.user.profile.photos.length > 0) {
            App.me.user.profile.photos = await updateUserProfileHelper(App.me.user, currentUser);
            if (App.me.user.profile.avatar != null) {
              App.me.user.profile.avatar = App.me.user.profile.photos.firstWhere((image) => image.id == App.me.user.profile.avatar.id);
            }
          }
        }
      });
    } else if (data != null &&
        currentUser != null &&
        currentUser.profile != null) {
      if (data != null && data['ctrl']['code'] == 200 && data['data'] != null) {
        App.me.user = ProfileData.fromJson(data['data']);
        if (App.me.user.profile.photos != null &&
            App.me.user.profile.photos.length > 0) {
          App.me.user.profile.photos =
              await updateUserProfileHelper(App.me.user, currentUser);
          if (App.me.user.profile.avatar != null) {
            App.me.user.profile.avatar = App.me.user.profile.photos.firstWhere(
                (image) => image.id == App.me.user.profile.avatar.id);
          }
        }
      }
    }

    await userDao.updateUser(App.me.user);
    if (callback != null) callback(App.me.user);
    return App.me.user;
  }

  static Future<List<UserPhoto>> updateUserProfileHelper(
      ProfileData remoteUserData, ProfileData currentUser) async {
    List<UserPhoto> remoteUserPhotos = remoteUserData.profile.photos;
    List<UserPhoto> currentUserPhotos = currentUser.profile.photos;
    List<UserPhoto> updatedPhotos = await updateUserPhotosStatus(
        remoteUserPhotos, currentUserPhotos, currentUser);
    return updatedPhotos;
  }

  static Future<List<UserPhoto>> updateUserPhotosStatus(
      List<UserPhoto> photosRemote,
      List<UserPhoto> currentUserPhotos,
      ProfileData currentUser) async {
    for (var image in photosRemote) {
      for (var item in currentUserPhotos) {
        if (image.id == item.id) {
          item.moderated = image.moderated;
          item.blocked = image.blocked;
          if (currentUser.profile.avatar != null &&
              image.id == currentUser.profile.avatar.id) {
            App.me.user.profile.avatar.moderated = image.moderated;
            App.me.user.profile.avatar.blocked = image.blocked;
          }
        }
      }
    }

    return currentUserPhotos;
  }
}
