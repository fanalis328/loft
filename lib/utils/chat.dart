import 'package:loft/data/app.dart';

import 'package:loft/model/websocket/chat.dart';

import '../locator.dart';

class ChatUtils {
  static String getCleanMessageTs(String ts) {
    if (ts == '' || ts == null) return '';
    String cleanTs = DateTime.parse(ignoreSubMicro(ts))
        .toLocal()
        .toString()
        .split(' ')
        .last
        .substring(0, 5);
    return cleanTs;
  }

  static String getCleanMessageTsForDialogItem(String ts) {
    if (ts == '' || ts == null) return '';
    String cleanTs = DateTime.parse(ignoreSubMicro(ts))
        .toLocal()
        .toString()
        .substring(0, 16);
    return cleanTs;
  }

  static String ignoreSubMicro(String s) {
    if (s.length > 27) return s.substring(0, 26) + s[s.length - 1];
    return s;
  }

// ____________________________________________________________________________________________
  static Future<void> newMessageListener(Map message,
      [bool notification = true, dynamic callback]) async {
    String userId;
    if (notification) {
      userId = message['notify']['src'];
      await ChatUtils.updateSingleChat(userId);
      if (callback != null) await callback();
    } else {
      userId = message['chat']['topic'];
      await ChatUtils.updateSingleChat(userId);
      if (callback != null) await callback();
    }
  }

  static Future<void> addNewChats(List chats) async {
    for (var chat in chats) {
      updateSingleChat(chat['topic'], true);
    }
  }

  static Future<void> updateDialogs() async {
    List<ChatDialogData> chats = await chatDao.getAllChatsSortedById();
    chats.removeWhere((chat) => chat.owner != App.me.user.id);
    List remoteChats = [];
    chatApi.getChats(App.me.user.id, (Map chatsData) async {
      if (chatsData != null && chatsData['ctrl']['code'] != 200 ||
          chatsData['data']['chats'].length == 0) return;
      remoteChats.addAll(chatsData['data']['chats']);
      await updatedChatsInfo(remoteChats);
      if (chats.length == 0 &&
          chatsData['data'] != null &&
          chatsData['data']['chats'].length > 0) {
        await addNewChats(remoteChats);
      } else if (chats.length > 0 &&
          chatsData['data']['chats'].length > chats.length) {
        for (var chat in chats) {
          chatsData['data']['chats']
              .removeWhere((item) => chat.topic == item['topic']);
        }
        await addNewChats(chatsData['data']['chats']);
      }
    });
  }

  static updatedChatsInfo(List remoteChats) async {
    List<ChatDialogData> chats = await chatDao.getAllChatsSortedById();
    for (var item in remoteChats) {
      for (var chat in chats) {
        if (chat.topic.contains(item['topic']) &&
            DateTime.parse(item['touched'])
                .isAfter(DateTime.parse(chat.touched))) {
          await updateSingleChat(chat.topic);
        }
      }
    }
  }

  static Future<void> updateSingleChat(String topic,
      [bool insert = false]) async {
    chatApi.subscribeChat(topic, (Map subscribedData) {
      if (subscribedData == null ||
          subscribedData['ctrl']['code'] != 200 &&
              subscribedData['ctrl']['code'] != 304) return;
      chatApi.getChatInfo(topic, (Map chatInfo) {
        if (chatInfo['ctrl']['code'] != 200 ||
            chatInfo == null ||
            chatInfo['data'] == null) return;
        ChatDialogData newChat = ChatDialogData.fromJson(chatInfo['data']);
        int before = newChat.seq == null ? 0 : newChat.seq;
        int since =
            newChat.seq == null || newChat.seq <= 1 ? 0 : newChat.seq - 1;
        chatApi.getMessages(newChat.topic, before, since,
            (Map messagesData) async {
          if (messagesData['ctrl']['code'] == 200 &&
              messagesData['data']['count'] > 0) {
            newChat.lastMessage =
                LastMessage.fromJson(messagesData['data']['messages'][0]);
            newChat.lastMessage.content == '' ||
                    messagesData['data']['messages'][0]['head'] != null
                ? newChat.lastMessage.type = 1
                : newChat.lastMessage.type = 0;
          }
          insert
              ? await chatDao.insertChatDialog(newChat)
              : await chatDao.updateChatDialog(newChat);
        });
      });
    });
  }
}
