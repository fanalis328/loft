import 'package:loft/locale/app_translations.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/verification.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/services/navigation_service.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/route_paths.dart' as routes;
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/popups/male_premium.dart';
import 'package:loft/widgets/popups/profile_moderation.dart';
import 'package:flutter/material.dart';

class VerificationUtils {
  static verificationListener([context, dynamic callback]) async {
    userApi.getAccount((Map data) async {
      checkVerificationCases(data, context, callback);
    });
  }

  static checkVerificationCases(Map data, [context, dynamic callback]) async {
    VerificationApproved approve = await verificationPhotoDao.getVerificationApproved();
    NavigationService _navigationService = locator<NavigationService>();
    makeNewVerificationPhoto() {
      _navigationService.navigateTo(routes.VerificationPhoto, arguments: true);
    }

    makeNewAvatar() {
      _navigationService.navigateTo(routes.LoadNewAvatar, arguments: true);
    }

    if (data != null && data['ctrl']['code'] == 200) {
      ProfileUtils.updateUserProfile(data, (ProfileData userData) async {
        switch (userData.profile.gender) {
          case 1:
            DateTime date = DateTime.now();
            bool purchaseExpired;
            if (userData.subscription != null) {
              purchaseExpired = date.isAfter(DateTime.parse(userData.subscription));
            }

            if (userData.subscription == null && context != null ||
                context != null && purchaseExpired) {
              showDialog(
                context: context,
                builder: (context) {
                  return MalePremiumDialog();
                },
              );
              return;
            }
            break;
          case 2:
            if (userData.deleted) {
              _navigationService.navigateTo(routes.BlockingProfile,
                  arguments: 'Ваша анкета удалена \nпо решению администрации');
              return;
            } else if (userData.blocked) {
              _navigationService.navigateTo(routes.BlockingProfile,
                  arguments:
                      'Ваша анкета заблокирована \nпо решению администрации');
              return;
            } else if (userData.profile.avatar == null ||
                userData.profile.avatar.moderated == 2) {
              makeNewAvatar();
              await verificationPhotoDao.setVerificationApproved(
                  VerificationApproved(owner: userData.id, approved: false));
              return;
            } else if (userData.moderated == 2 || userData.moderated == 3) {
              _navigationService
                  .navigateTo(routes.ModerationProfile, arguments: {
                "title":
                    "Фото для \nподтверждения личности \nне прошло модерацию",
                "description":
                    "Сделайте новое фото \nдля подтверждения и дождитесь \nответа модерации, чтобы открыть \nвозможность писать другим пользователям",
                "btnTitle": "Сделать снова",
                "onTap": makeNewVerificationPhoto,
                "backArrowTap": () {
                  ProfileUtils.updateUserProfile();
                  return Navigator.pop;
                },
                "showHeader": true,
                "moderated": false,
              });
              await verificationPhotoDao.setVerificationApproved(
                  VerificationApproved(owner: userData.id, approved: false));
              return;
            } else if (userData.moderated == 0 ||
                userData.profile.avatar.moderated == 0) {
              if (context == null) return;
              showDialog(
                context: context,
                builder: (context) {
                  return ProfileModerationDialog(moderated: false);
                },
              );
              return;
            } else if (userData.moderated == 1 &&
                userData.profile.avatar.moderated == 1) {
              if (approve.owner == userData.id &&
                      approve.approved == false &&
                      context != null ||
                  approve.owner == null &&
                      approve.approved == null &&
                      context != null) {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return ProfileModerationDialog(moderated: true);
                  },
                );
                if (callback != null) callback();
                return;
              }

              if (approve.owner == userData.id &&
                      approve.approved == false &&
                      context == null ||
                  approve.owner == null &&
                      approve.approved == null &&
                      context == null) {
                _navigationService.navigateTo(routes.Account,
                    arguments: VerificationInfo(index: 3, verification: true));
                return;
              }
            }
            break;
          default:
            return;
        }

        if (callback != null) callback();
      });
    }
  }

  static deleteOrBlockedUserTap(context, bool deleted, bool blocked,
      [dynamic callback]) {
    if (!blocked && !deleted) {
      if (callback != null) callback();
    } else if (blocked || deleted) {
      return showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              blocked
                  ? AppTranslations.of(context).text('user_is_blocked')
                  : AppTranslations.of(context).text('user_is_deleted'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
          );
        },
      );
    }
  }
}

class VerificationInfo {
  int index;
  bool verification;

  VerificationInfo({this.index = 0, this.verification = false});
}
