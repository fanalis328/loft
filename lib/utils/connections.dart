import 'package:connectivity/connectivity.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/services/connectivity_service.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';

class ConnectionUtils {
  static Future<bool> checkInternetConnectivity(context) async {
    var result = await checkConnectivity();
    if (result == ConnectivityResult.none) {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
            title: Text(AppTranslations.of(context).text('blocking_error')),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                AppTranslations.of(context).text('blocking_error_connection_3'),
                style: DenimFonts.titleBody(
                  context,
                  DenimColors.colorBlackSecondary,
                ),
              ),
            ),
          );
        },
      );
      return false;
    }

    return true;
  }
}
