import 'package:intl/intl_browser.dart';
import 'locale_service_interface.dart';

class LocaleServiceWeb implements ILocaleService {
  Future<String> getLocale() {
    return findSystemLocale();
  }
}

ILocaleService getLocaleService() {
  return LocaleServiceWeb();
}
