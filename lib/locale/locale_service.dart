import 'package:devicelocale/devicelocale.dart';

import 'locale_service_interface.dart';

class LocaleService implements ILocaleService {
  Future<String> getLocale() async {
    final locale = await Devicelocale.currentLocale;
    return locale.split('_').first;
  }
}

ILocaleService getLocaleService() {
  return LocaleService();
}
