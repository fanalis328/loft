import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class DenimInput extends StatelessWidget {
  final TextEditingController controller;
  final dynamic onChanged;
  final int maxLength;
  final String hintText;
  final String labelText;
  final TextInputType keyboardType;
  final bool autofocus;
  final dynamic errorText;
  final dynamic suffixIcon;
  final dynamic prefixIcon;
  final FocusNode focusNode;
  final bool obscureText;

  DenimInput({
    this.controller,
    this.onChanged,
    this.maxLength,
    this.hintText = '',
    this.labelText = '',
    this.keyboardType,
    this.autofocus = false,
    this.errorText,
    this.suffixIcon,
    this.prefixIcon,
    this.focusNode,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: TextField(
        textCapitalization: TextCapitalization.sentences,
        controller: controller,
        keyboardType: keyboardType,
        autofocus: autofocus,
        focusNode: focusNode,
        onChanged: (val) => onChanged(val),
        decoration: InputDecoration(
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          contentPadding: labelText.length == 0
              ? EdgeInsets.fromLTRB(0, 0, 0, 15)
              : EdgeInsets.fromLTRB(0, 3, 0, 15),
          errorText: errorText,
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 2,
              color: Colors.black,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: DenimColors.colorBlackLine,
            ),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: DenimColors.colorBlackLine,
            ),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(
              color: DenimColors.colorBlackLine,
            ),
          ),
          errorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DenimColors.colorSpecialErrorDefault),
          ),
          focusedErrorBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: DenimColors.colorPrimary),
          ),
          hintText: hintText,
          hintStyle: DenimFonts.titleBody(
            context,
            DenimColors.colorBlackSecondary,
          ),
          labelText: labelText,
          labelStyle: DenimFonts.titleBody(
            context,
            DenimColors.colorBlackSecondary,
          ),
        ),
        obscureText: obscureText,
      ),
    );
  }
}
