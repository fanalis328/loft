import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';

class ResponseStatusDialogs {

  defaultError(context,) {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          title: Text(
            AppTranslations.of(context).text('error_response_default'),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
          cancelBtnCallBack: () => Navigator.pop(context),
        );
      },
    );
  }

  successWithImage(context,
      [String cancelBtnTitle,
      String imagePath,
      String title,
      TextStyle styleTitle]) {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          centerButtons: true,
          cancelBtnTitle: cancelBtnTitle,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  imagePath,
                  width: 120,
                ),
              ),
              SizedBox(height: 16),
              Text(
                title,
                style: DenimFonts.titleDefault(context),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 35),
            ],
          ),
        );
      },
    );
  }
}
