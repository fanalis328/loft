import 'package:loft/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class Countdown extends AnimatedWidget {
  final Animation<int> animation;
  final TextStyle style;

  Countdown({ Key key, this.animation, this.style}) : super(key: key, listenable: animation);

  @override
  build(BuildContext context){
    return new Text(
      animation.value.toString(),
      style: style != null ? style : DenimFonts.titleBody(context),
    );
  }
}