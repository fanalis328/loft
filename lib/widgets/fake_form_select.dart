import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class FakeFormSelect extends StatefulWidget {
  final String value;
  final String label;
  final GestureTapCallback onTap;
  final bool disable;

  FakeFormSelect({
    this.value = '',
    this.label = '',
    this.onTap,
    this.disable = false,
  });

  @override
  _FakeFormSelectState createState() => _FakeFormSelectState();
}

class _FakeFormSelectState extends State<FakeFormSelect> {
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.disable ? () {return;} : widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: DenimColors.colorBlackLine,
            ),
          ),
        ),
        height: 50,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: widget.value.length == 0
                  ? Alignment.centerLeft
                  : Alignment.topLeft,
              child: Text(
                widget.label,
                style: widget.value.length == 0
                    ? DenimFonts.titleBody(
                        context,
                        DenimColors.colorBlackSecondary,
                      )
                    : DenimFonts.titleCaption(
                        context,
                        widget.disable ? DenimColors.colorBlackLine : DenimColors.colorBlackSecondary,
                      ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: Text(
                widget.value,
                style: DenimFonts.titleBody(
                  context,
                  widget.disable ? DenimColors.colorBlackLine : Colors.black,
                ),
              ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Icon(
                Icons.keyboard_arrow_down,
                color: widget.disable ? DenimColors.colorBlackLine : DenimColors.colorBlackTertiary,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
