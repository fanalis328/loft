import 'dart:io';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/gallery/base_gallery.dart';
import 'package:loft/widgets/gallery/gallery_overlay_view.dart';
import 'package:flutter/material.dart';

import 'package:transparent_image/transparent_image.dart';

import '../image.dart';

class Gallery extends StatefulWidget {
  final List<UserPhoto> galleryItems;
  final String token;
  final int moderatedUser;

  Gallery({this.galleryItems, this.token, this.moderatedUser});

  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  int currentIndex = 0;
  ProfileData user = App.me.user;
  CarouselSlider galleryInstance;
  double opacityLevel = 1.0;
  List<UserPhoto> filteredGalleryItemList = [];
  String avatarId;
  String noUserImage;

  @override
  void initState() {
    getUserAvatar();
    super.initState();
  }

  List<UserPhoto> getUserAvatar() {
    if (widget.galleryItems.length > 0) {
      avatarId = user.profile.avatar?.id;
      filteredGalleryItemList = BaseGallery.filterGalleryItems(
          widget.galleryItems, user.profile.avatar);
    }
    return filteredGalleryItemList;
  }

  void openViewGallery(BuildContext context, final int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          galleryItems: filteredGalleryItemList,
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: index,
        ),
      ),
    ).then((currentIndex) {
      galleryInstance?.jumpToPage(currentIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: filteredGalleryItemList != null &&
                    filteredGalleryItemList.length >= 1
                ? AspectRatio(
                    aspectRatio: 1 / 1,
                    child: Container(
                      color: Colors.black,
                      child: galleryInstance = CarouselSlider(
                        items: filteredGalleryItemList?.map((i) {
                          return Builder(
                            builder: (BuildContext context) {
                              return Container(
                                constraints: BoxConstraints.expand(),
                                child: GestureDetector(
                                  child: imageWidget(i),
                                  onTap: () {
                                    openViewGallery(context, currentIndex);
                                  },
                                ),
                              );
                            },
                          );
                        })?.toList(),
                        initialPage: 0,
                        aspectRatio: 1 / 1,
                        viewportFraction: 1.0,
                        enableInfiniteScroll:
                            filteredGalleryItemList.length <= 1 ? false : true,
                        onPageChanged: (val) {
                          setState(() {
                            currentIndex = val;
                          });
                        },
                      ),
                    ),
                  )
                : AspectRatio(
                    aspectRatio: 1 / 1,
                    child: Container(
                      color: Colors.black,
                      child: Image.asset(
                        'assets/images/universal-userphoto-hero-${App.me.user.profile.gender == 2 ? 'female' : 'male'}.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
          ),
          Visibility(
            visible: filteredGalleryItemList != null &&
                    filteredGalleryItemList.length >= 1
                ? true
                : false,
            child: Container(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    height: 32,
                    decoration: BoxDecoration(
                      color: DenimColors.colorTranslucentPrimary,
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: Center(
                      child: Text(
                        '${currentIndex + 1} / ${filteredGalleryItemList?.length}',
                        style: DenimFonts.titleBody(
                          context,
                          Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 15,
            left: 15,
            child: Visibility(
              visible: user.profile.gender == 2 ? true : false,
              child: widget.moderatedUser == 1 &&
                      user?.profile?.avatar?.moderated == 1
                  ? AnimatedOpacity(
                      opacity: opacityLevel,
                      duration: Duration(milliseconds: 220),
                      child: Container(
                        height: 32,
                        padding: EdgeInsets.fromLTRB(8, 0, 12, 0),
                        decoration: BoxDecoration(
                          color: DenimColors.colorTranslucentPrimary,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              'assets/images/icons/section-profileview-photoverified.png',
                              width: 16,
                            ),
                            SizedBox(width: 8),
                            Text(
                              AppTranslations.of(context).text('profile_approved'),
                              style: DenimFonts.titleFootnote(
                                context,
                                Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : AnimatedOpacity(
                      opacity: opacityLevel,
                      duration: Duration(milliseconds: 220),
                      child: Container(
                        height: 32,
                        padding: EdgeInsets.fromLTRB(8, 0, 12, 0),
                        decoration: BoxDecoration(
                          color: DenimColors.colorTranslucentPrimary,
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Row(
                          children: <Widget>[
                            Image.asset(
                              'assets/images/icons/section-profileview-photonoverified.png',
                              width: 16,
                            ),
                            SizedBox(width: 8),
                            Text(
                              AppTranslations.of(context).text('profile_not_approved'),
                              style: DenimFonts.titleFootnote(
                                context,
                                Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }

  imageWidget(UserPhoto item) {
    if (item.original.path != null &&
        item.original.path.contains(appConfig.downloadImageEndpoint)) {
      ProfileUtils.downloadAndUpdateUserPhoto(item);
      return DImage(
        imageUrl:
            '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${item.preview.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
        fit: BoxFit.cover,
        httpHeaders: {'Authorization': 'token ${widget.token}'},
        errorWidget: (context, url, error) => Icon(
          Icons.error,
          size: 60,
          color: Colors.white,
        ),
      );
    } else {
      return FadeInImage(
        //FIXME:
        placeholder: MemoryImage(kTransparentImage),
        //FIXME:
        image: FileImage(File(item.original.path)),
        fit: BoxFit.cover,
      );
    }
  }
}
