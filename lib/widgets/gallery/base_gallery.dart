import 'package:loft/model/websocket/account.dart';

class BaseGallery {
  static filterGalleryItems(List<UserPhoto> items, [UserPhoto avatar]) {
    List<UserPhoto> firstAvatar;
    List<UserPhoto> moderated;
    if(items.length == 0) return [];

    moderated = items.where((item) => item.moderated < 2 && item?.id != avatar?.id).toList();

    if (avatar == null || avatar.moderated == 2) {
      return moderated;
    } else {
      firstAvatar = items.where((item) => item.id == avatar.id).toList();
      if(firstAvatar.first.moderated > 1) {
        return moderated;
      }
      firstAvatar.addAll(moderated);
      return firstAvatar;
    }
  }
}
