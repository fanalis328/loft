import 'package:loft/theme/theme_colors.dart';
import 'package:flutter/material.dart';

class Textarea extends StatelessWidget {
  final TextEditingController controller;
  final int maxLength;
  final int maxLines;
  final int minLines;
  final String hintText;
  final String labelText;
  final bool autofocus;
  final TextAlign textAlign;
  final String errorText;
  final Function onChanged;

  Textarea({
    this.controller,
    this.maxLength = 150,
    this.maxLines = 5,
    this.minLines = 3,
    this.hintText = '',
    this.labelText,
    this.autofocus = false,
    this.textAlign = TextAlign.left,
    this.errorText,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      autofocus: autofocus,
      maxLines: maxLines,
      minLines: minLines,
      maxLength: maxLength,
      onChanged: (val) => onChanged(val),
      keyboardType: TextInputType.multiline,
      controller: controller,
      textAlign: textAlign,
      decoration: InputDecoration(
        errorText: errorText,
        hintText: hintText,
        border: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1,
            color: DenimColors.colorBlackLine,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 2,
            color: Colors.black,
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        contentPadding: EdgeInsets.all(12),
      ),
    );
  }
}
