import 'package:loft/theme/theme_colors.dart';
import 'package:flutter/material.dart';

class DefaultSlider extends StatefulWidget {
  final Slider slider;
  DefaultSlider({this.slider});

  @override
  _DefaultSliderState createState() => _DefaultSliderState();
}

class _DefaultSliderState extends State<DefaultSlider> {
  final Slider slider;
  _DefaultSliderState({this.slider});

  @override
  Widget build(BuildContext context) {
    return DefaultSliderTheme(slider: widget.slider);
  }
}


class DefaultSliderTheme extends StatelessWidget {
  final slider;

  DefaultSliderTheme({this.slider});

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: Colors.black,
        inactiveTrackColor: DenimColors.colorBlackLine,
        trackHeight: 2,
        rangeThumbShape: RoundRangeSliderThumbShape(enabledThumbRadius: 12),
        thumbColor: Colors.black,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 8),
        overlappingShapeStrokeColor: DenimColors.colorPrimary,
      ),
      child: slider,
    );
  }
}