import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class DenimButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String title;
  final Color color;
  final Color textColor;
  final bool disable;
  final Icon icon;
  final TextStyle font;

  DenimButton({
    @required this.onPressed,
    this.title,
    this.color = DenimColors.colorPrimary,
    this.textColor = Colors.white,
    this.disable = false,
    this.icon,
    this.font,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      child: FlatButton(
        onPressed: disable ? null : onPressed,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
              visible: icon == null ? false : true,
              child: icon ?? Text(''),
            ),
            Visibility(
              visible: icon == null ? false : true,
              child: SizedBox(width: 5),
            ),
            Text(
            title == null ? AppTranslations.of(context).text('btn_сontinue_title') : title,
            style: font == null ? DenimFonts.titleBtnDefault(context, textColor) : font,
          ),
          ],
        ),
        color: color,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        disabledColor: DenimColors.colorBlackTertiary,
      ),
    );
  }
}
