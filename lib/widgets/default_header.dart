import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/locator.dart';
import 'package:loft/widgets/popups/new_support.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class DefaultHeader extends StatelessWidget {
  final GestureTapCallback backArrowTap;
  final String actionTitle;
  final GestureTapCallback actionTitleCallBack;
  final String mainTitle;
  final bool visibleActionTitle;
  final bool visibleBackArrow;
  final Icon backArrowIcon;

  DefaultHeader({
    this.backArrowTap,
    this.actionTitle,
    this.actionTitleCallBack,
    this.mainTitle = '',
    this.visibleActionTitle = true,
    this.visibleBackArrow = true,
    this.backArrowIcon = const Icon(
      DenimIcon.universal_back,
      color: Colors.black,
      size: 30,
    ),
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        height: 56,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Positioned(
                    left: -11,
                    child: Container(
                      child: Visibility(
                          visible: visibleBackArrow,
                          child: IconButton(
                            icon: backArrowIcon,
                            onPressed: backArrowTap,
                          )),
                    ),
                  ),
                  Text(
                    mainTitle,
                    textAlign: TextAlign.center,
                    style: DenimFonts.titleDefault(context),
                  ),
                  Positioned(
                    right: 0,
                    child: Container(
                      child: Visibility(
                        visible: visibleActionTitle,
                        child: GestureDetector(
                          onTap: () => actionTitleOnTap(context),
                          child: Text(
                            actionTitle == null
                                ? AppTranslations.of(context)
                                    .text('default_header_action_title')
                                : actionTitle,
                            textAlign: TextAlign.end,
                            style: DenimFonts.titleBtnSmallSecondary(
                              context,
                              Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  actionTitleOnTap(context) async {
    if (actionTitleCallBack != null) {
      return actionTitleCallBack();
    } else {
      firebaseAnalytics.sendAnalyticsEvent('support');
      return showDialog(
        context: context,
        builder: (context) {
          return SupportDialogs();
        },
      );
    }
  }
}
