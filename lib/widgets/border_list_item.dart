import 'package:loft/theme/theme_colors.dart';
import 'package:flutter/material.dart';

class BorderListItem extends StatefulWidget {
  final Widget title;
  final GestureTapCallback onTap;
  final double height;
  final double padding;
  final double margin;

  BorderListItem({
    this.title,
    this.onTap,
    this.height = 48,
    this.padding = 0,
    this.margin = 6,
  });

  @override
  _BorderListItemState createState() => _BorderListItemState();
}

class _BorderListItemState extends State<BorderListItem> {
  bool hover = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.symmetric(vertical: widget.margin),
      shape: hover
          ? RoundedRectangleBorder(
              side: BorderSide(color: Colors.black, width: 2),
              borderRadius: BorderRadius.circular(10),
            )
          : RoundedRectangleBorder(
              side: BorderSide(
                color: DenimColors.colorBlackLine,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
      child: InkWell(
        borderRadius: BorderRadius.circular(10),
        onTap: widget.onTap,
        onHighlightChanged: (bool val) {
          setState(() {
            hover = val;
          });
        },
        highlightColor: Colors.white,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: widget.padding),
          height: widget.height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[widget.title],
          ),
        ),
      ),
    );
  }
}
