import 'package:loft/denim_icons.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:flutter/material.dart';

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    this.label,
    this.padding = const EdgeInsets.all(0),
    this.value,
    this.onChanged,
  });

  final String label;
  final EdgeInsets padding;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onChanged(!value);
      },
      child: Padding(
        padding: padding,
        child: Row(
          children: <Widget>[
            Container(
              width: 20,
              height: 20,
              child: RawMaterialButton(
                child: Visibility(
                  visible: value ? true : false,
                  child: Center(
                    child: Icon(
                      DenimIcon.universal_done,
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                ),
                onPressed: () => onChanged(!value),
                fillColor: value ? Colors.black : Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(100),
                  side: value
                      ? BorderSide()
                      : BorderSide(
                          width: 1,
                          color: DenimColors.colorBlackSecondary,
                        ),
                ),
              ),
            ),
            SizedBox(width: 14),
            Expanded(
              child: Text(
                label,
                style: DenimFonts.titleBody(context),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
