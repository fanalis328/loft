import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/denim_black/loft_premium.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';

class MalePremiumDialog extends StatelessWidget {
  final bool moderated;
  final ProfileData user = App.me.user;
  MalePremiumDialog({this.moderated = false});

  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      centerButtons: true,
      cancelBtnTitle: AppTranslations.of(context).text('denim_black_block_popup_male_btn'),
      cancelBtnCallBack: () {
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => LoftPremium()));
      },
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(top: 25),
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: 104,
                    height: 104,
                    child: Image.asset('assets/images/icons/section-subscription-symbol.jpg', fit: BoxFit.contain,),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              AppTranslations.of(context).text('denim_black_block_popup_male_1'),
              style: DenimFonts.titleDefault(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 12),
            Text(
              AppTranslations.of(context).text('denim_black_block_popup_male_2'),
              style: DenimFonts.titleBody(context),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 32),
          ],
        ),
      ),
    );
  }
}
