import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';

class PostingPhotoRules extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      title: Text(
        AppTranslations.of(context).text('popup_posting_photo_rules_main_title'),
        style: DenimFonts.titleDefault(context),
      ),
      cancelBtnTitle: AppTranslations.of(context).text('clear'),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Wrap(
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('popup_posting_photo_rules_subtitle_1'),
              style: DenimFonts.titleSection(context),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
            ),
            SizedBox(height: 10),
            Text(
              '1. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_1_1')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '2. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_1_2')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '3. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_1_3')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
            ),
            Text(
              AppTranslations.of(context).text('popup_posting_photo_rules_subtitle_2'),
              style: DenimFonts.titleSection(context),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
            ),
            Text(
              '1. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_1')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '2. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_2')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '3. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_3')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '4. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_4')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '5. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_5')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '6. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_6')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '7. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_7')}',
              style: DenimFonts.titleFootnote(context),
            ),
            Text(
              '8. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_8')}',
              style: DenimFonts.titleFootnote(context),
            ),
            // Text(
            //   '9. ${AppTranslations.of(context).text('popup_posting_photo_rules_title_2_9')}',
            //   style: DenimFonts.titleFootnote(context),
            // ),
            Container(
              margin: EdgeInsets.only(top: 15),
            ),
            Text(
              AppTranslations.of(context).text('popup_posting_photo_rules_bottom_description'),
              style: DenimFonts.titleFootnote(context),
            ),
          ],
        ),
      ),
    );
  }
}
