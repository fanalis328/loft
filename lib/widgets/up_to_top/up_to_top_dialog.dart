import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:loft/locator.dart';
import 'package:loft/data/app.dart';

class UpToTopDialog extends StatefulWidget {
  final ProductDetails product;
  final bool available;

  UpToTopDialog({
    this.product,
    this.available,
  });

  @override
  _UpToTopDialogState createState() => _UpToTopDialogState();
}

class _UpToTopDialogState extends State<UpToTopDialog> {
  @override
  Widget build(BuildContext context) {
    return DefaultAlertDialog(
      hiddenButtons: true,
      title: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
            child: Text(
              AppTranslations.of(context).text('raise_main_title'),
              textAlign: TextAlign.center,
              style: DenimFonts.titleDefault(context),
            ),
          ),
          Positioned(
            top: -17,
            left: -17,
            child: IconButton(
              icon: Icon(
                DenimIcon.universal_close,
                size: 24,
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(26, 0, 26, 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  'assets/images/icons/topup-heart.jpg',
                  width: 60,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    '${AppTranslations.of(context).text('raise_item_1_title')} ${App.me.user.profile.gender == 1 ? AppTranslations.of(context).text('women') : AppTranslations.of(context).text('men')}',
                    style: DenimFonts.titleFootnote(context),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(26, 8, 26, 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  'assets/images/icons/topup-chat.jpg',
                  width: 60,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    AppTranslations.of(context).text('raise_item_2_title'),
                    style: DenimFonts.titleFootnote(context),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(26, 8, 26, 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  'assets/images/icons/topup-fast.jpg',
                  width: 60,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    AppTranslations.of(context).text('raise_item_3_title'),
                    style: DenimFonts.titleFootnote(context),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 26),
          Text(
            '${AppTranslations.of(context).text('price')}: ${widget?.product?.price}',
            style: DenimFonts.titleFootnote(
              context,
              DenimColors.colorBlackSecondary,
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(0, 12, 0, 16),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: AppTranslations.of(context)
                        .text('search_raise_profile_btn'),
                    onPressed: () {
                      print(widget.available);
                      if (widget.available) {
                        Navigator.pop(context);
                      }
                      purchaseUtils.buyProduct(widget.product);
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
