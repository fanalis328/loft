import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:flutter/material.dart';

class UpToTopSearch extends StatelessWidget {
  final GestureTapCallback onTap;
  final int position;
  
  UpToTopSearch({
    this.onTap,
    this.position,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(16, 15, 16, 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: DenimColors.colorBlackCell,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RichText(
                              text: TextSpan(children: [
                                TextSpan(
                                    text: AppTranslations.of(context).text('search_raise_profile_1'),
                                    style: DenimFonts.titleBody(context)),
                                TextSpan(
                                  text: position == null || position > 200 ? ' 200+' : ' $position',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                ),
                              ]),
                            ),
                            Text(
                              AppTranslations.of(context).text('search_raise_profile_2'),
                              style: DenimFonts.titleBody(context),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 36,
                    width: 135,
                    child: DenimButton(
                      title: AppTranslations.of(context).text('search_raise_profile_btn'),
                      font: DenimFonts.titleBtnSmall(context, Colors.white),
                      onPressed: () => onTap(),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
