import 'config/enviroment.dart';
import 'main.dart' as app;

void main() {
  Constants.setEnviroment(Enviroment.dev);
  app.main();
}
