abstract class IPushNotification {
  Future<String> getToken();

  void initialize();
}
