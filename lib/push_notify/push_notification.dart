import 'package:loft/push_notify/push_notification_interface.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotification implements IPushNotification {
  final FirebaseMessaging _push = FirebaseMessaging();
  Future<String> getToken() {
    return _push.getToken();
  }

  void initialize() {
    _push.configure(
      onMessage: (Map<String, dynamic> message) async {},
      onLaunch: (Map<String, dynamic> message) async {},
      onResume: (Map<String, dynamic> message) async {},
    );
  }
}
