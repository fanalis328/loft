import 'package:loft/model/purchase.dart';

abstract class IPurchaseDao {
  Future<void> setPurchaseData(UpdatePurchaseData purchaseData);

  Future<UpdatePurchaseData> getPurchaseData();
  
  Future<TrialPurchaseStatus> getTrialPurchaseStatus();

  Future<void> setTrialPurchaseStatus(TrialPurchaseStatus purchaseStatus);

  Future<void> updatePurchase(UpdatePurchaseData purchaseData);
}
