abstract class ILangDao {
  Future<void> setLang([String lang = 'ru']);

  Future<String> getLang();
}
