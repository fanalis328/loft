import 'package:loft/data/app.dart';
import 'package:sembast/sembast.dart';
import 'countries_list_dao_interface.dart';

class CountriesListDao implements ICountriesListDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setCountriesList(List countries) async {
    await store.record('countriesList').put(await db, countries);
  }

  getCountriesList() async {
    var countriesList = await store.record('countriesList').get(await db);
    return countriesList;
  }

  deleteCountriesList() async {
    await store.record('countriesList').delete(await db);
  }
}
