import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as json;
import 'crud_db_interface.dart';

class CrudLocalStorage<T> implements ICrudDb<T> {
  final dynamic Function(T) getKey;
  final String storeName;
  final T Function(Map<String, dynamic> map) parseDelegate;

  CrudLocalStorage({this.storeName, this.parseDelegate, this.getKey});
  Future<SharedPreferences> get _db => SharedPreferences.getInstance();

  List<dynamic> _parseJson(String str) {
    try {
      return json.jsonDecode(str);
    } catch (e) {
      return [];
    }
  }

  String _collectionToString(List<T> collection) {
    return json.jsonEncode(
        collection.map((item) => (item as dynamic).toJson()).toList());
  }

  Future<void> _setCollection(List<T> collection) async {
    final db = await _db;
    db.setString(storeName, _collectionToString(collection));
  }

  Future<List<T>> getAll() async {
    final db = await _db;
    return _parseJson(db.getString(storeName))
        .map((item) => parseDelegate(item))
        .toList();
  }

  Future<void> add(T item) async {
    final items = await getAll();
    items.add(item);
    await _setCollection(items);
  }

  Future<void> update(T item, [String id]) async {
    final items = await getAll();
    await _setCollection(items.map((itm) {
      if (getKey(itm) == getKey(item)) return item;
      if (getKey(itm) == id && id != null) return item;
      return itm;
    }).toList());
  }

  Future<void> delete(dynamic key) async {
    final items = await getAll();
    await _setCollection(items.where((itm) {
      if (getKey(itm) != key) return true;
      return false;
    }).toList());
  }

  Future<void> deleteAll() {
    return _setCollection([]);
  }

  Future<T> getItem(dynamic key) async {
    final items = await getAll();
    try {
      return items.firstWhere((itm) => getKey(itm) == key);
    } catch (_) {
      return null;
    }
  }
}
