import 'auth_dao_interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthDaoWeb implements IAuthDao {
  Future<SharedPreferences> get _db => SharedPreferences.getInstance();

  @override
  setToken([String token]) async {
    final db = await _db;
    await db.setString('token', token);
  }

  @override
  getToken() async {
    final db = await _db;
    return db.getString('token');
  }

  @override
  deleteToken() async {
    final db = await _db;
    await db.remove('token');
  }

  @override
  setCurrentUserId([String id]) async {
    final db = await _db;
    await db.setString('currentUserId', id);
  }

  @override
  getCurrentUserId() async {
    final db = await _db;
    return db.getString('currentUserId');
  }

  @override
  deleteCurrentUserId() async {
    final db = await _db;
    await db.remove('currentUserId');
  }
}
