import 'package:loft/data/app.dart';
import 'package:loft/model/websocket/chat.dart';
import 'package:sembast/sembast.dart';

import 'chat_dao_interface.dart';

class ChatDao implements IChatDao {
  var _chatStore = intMapStoreFactory.store('chats');
  Future<Database> get _db async => await App.me.db.database;

  Future insertChatDialog(ChatDialogData chat) async {
    var finder = Finder(filter: Filter.equals('topic', chat.topic));
    ChatDialogData chatDialog;

    final recordSnapshot = await _chatStore.find(
      await _db,
      finder: finder,
    );

    recordSnapshot
        .map((snapshot) => chatDialog = ChatDialogData.fromJson(snapshot.value))
        .toList();
    if (chatDialog?.topic == chat?.topic && chatDialog?.owner == App.me.user.id)
      return;
    await _chatStore.add(await _db, chat.toJson());
  }

  Future updateChatDialog(ChatDialogData chat) async {
    var finder = Finder(filter: Filter.equals('topic', chat.topic));

    _chatStore.findKey(await _db);
    await _chatStore.update(
      await _db,
      chat.toJson(),
      finder: finder,
    );
  }

  Future<ChatDialogData> getChatDialog(String topic) async {
    var finder = Finder(filter: Filter.equals('topic', topic));
    ChatDialogData currentChat;

    final recordSnapshot = await _chatStore.find(
      await _db,
      finder: finder,
    );

    if (recordSnapshot == null) return null;

    recordSnapshot.map((snapshot) {
      currentChat = ChatDialogData.fromJson(snapshot.value);
    }).toList();
    return currentChat;
  }

  Future deleteChatDialog(String topic) async {
    final finder = Finder(filter: Filter.equals('topic', topic));
    await _chatStore.delete(await _db, finder: finder);
  }

  Future deleteAllChats() async {
    await _chatStore.delete(
      await _db,
    );
  }

  Future<List<ChatDialogData>> getAllChatsSortedById() async {
    final finder = Finder(sortOrders: [
      SortOrder('id'),
    ]);

    final recordSnapshots = await _chatStore.find(
      await _db,
      finder: finder,
    );

    return recordSnapshots.map((snapshot) {
      final chat = ChatDialogData.fromJson(snapshot.value);
      return chat;
    }).toList();
  }
}
