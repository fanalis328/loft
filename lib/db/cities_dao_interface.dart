abstract class ICitiesDao {
  Future<void> setCities(List cities);

  //TODO: add type
  Future<dynamic> getCities();

  Future<void> deleteCities();
}
