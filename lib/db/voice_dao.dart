import 'package:loft/data/app.dart';
import 'voice_dao_interface.dart';
import 'package:loft/model/voice.dart';
import 'package:sembast/sembast.dart';

class VoiceDao implements IVoiceDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  Future<void> setVoiceStatus(VoiceStatus voiceStatus) async {
    store.record('voiceStatus').put(await db, voiceStatus.toJson());
  }

  Future<VoiceStatus> getVoiceStatus() async {
    var voiceStatus = await store.record('voiceStatus').get(await db);
    return VoiceStatus.fromJson(voiceStatus);
  }

  deleteVoiceStatus() async {
    await store.record('voiceStatus').delete(await db);
  }
}
