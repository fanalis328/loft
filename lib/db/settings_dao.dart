import 'package:loft/data/app.dart';
import 'settings_dao_interface.dart';
import 'package:sembast/sembast.dart';

class SettingsDao implements ISettingsDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setSettings(Map settings) async {
    await store.record('settings').put(await db, settings);
  }

  getSettings() async {
    var settings = await store.record('settings').get(await db);
    return settings;
  }

  deleteSettings() async {
    await store.record('settings').delete(await db);
  }
}
