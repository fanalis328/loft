import 'package:loft/data/app.dart';
import 'package:sembast/sembast.dart';
import 'countries_dao_interface.dart';

class CountriesDao implements ICountriesDao {
  var store = StoreRef.main();
  Future<Database> db = App.me.db.database;

  setCountries(List countries) async {
    await store.record('countries').put(await db, countries);
  }

  getCountries() async {
    var countries = await store.record('countries').get(await db);
    return countries;
  }

  deleteCountries() async {
    await store.record('countries').delete(await db);
  }
}
