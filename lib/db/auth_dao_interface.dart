abstract class IAuthDao {
  Future<void> setToken([String token]);

  Future<String> getToken();

  Future<void> deleteToken();

  Future<void> setCurrentUserId([String id]);

  Future<String> getCurrentUserId();

  Future<void> deleteCurrentUserId();
}
