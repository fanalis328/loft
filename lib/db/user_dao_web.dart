import 'package:loft/db/crud_db_interface.dart';
import 'crud_db_web_localstorage.dart';
import 'user_dao_interface.dart';
import 'package:loft/model/websocket/account.dart';

class UserDaoWeb implements IUserDao {
  final ICrudDb<ProfileData> _db = CrudLocalStorage<ProfileData>(
      getKey: (usr) => usr.id,
      storeName: 'user',
      parseDelegate: ProfileData.fromJson);

  @override
  Future deleteAll() {
    return _db.deleteAll();
  }

  @override
  Future deleteUser(ProfileData user) {
    return _db.delete(user.id);
  }

  @override
  Future<List<ProfileData>> getAllSortedById() async {
    final items = await _db.getAll();
    return items;
  }

  @override
  Future<ProfileData> getCurrentUser(ProfileData user) {
    return _db.getItem(user.id);
  }

  @override
  Future insertUser(ProfileData user) {
    return _db.add(user);
  }

  @override
  Future updateUser(ProfileData user) {
    return _db.update(user, user.id);
  }
}
