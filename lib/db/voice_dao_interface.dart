import 'package:loft/model/voice.dart';

abstract class IVoiceDao {
  Future<void> setVoiceStatus(VoiceStatus voiceStatus);

  Future<VoiceStatus> getVoiceStatus();

  Future<void> deleteVoiceStatus();
}
