abstract class ICrudDb<T> {
  Future<List<T>> getAll();

  Future<void> add(T item);

  Future<void> update(T item, [String id]);

  Future<void> delete(dynamic key);

  Future<void> deleteAll();

  Future<T> getItem(dynamic key);
}
