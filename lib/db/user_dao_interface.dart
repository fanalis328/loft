import 'package:loft/model/websocket/account.dart';

abstract class IUserDao {
  Future insertUser(ProfileData user);

  Future updateUser(ProfileData user);

  Future<ProfileData> getCurrentUser(ProfileData user);

  Future deleteUser(ProfileData user);

  Future deleteAll();

  Future<List<ProfileData>> getAllSortedById();
}
