import 'verification_photo_dao_interface.dart';
import 'package:loft/model/verification.dart';

class VerificationPhotoDaoWeb implements IVerificationPhotoDao {
  @override
  Future<void> deleteVerificationApprove() {
    // TODO: implement deleteVerificationApprove
    //throw UnimplementedError();
  }

  @override
  Future<void> deleteVerificationPhotos() {
    // TODO: implement deleteVerificationPhotos
    //throw UnimplementedError();
  }

  @override
  Future<VerificationApproved> getVerificationApproved() {
    // TODO: implement getVerificationApproved
    //throw UnimplementedError();
  }

  @override
  Future<VerificationPhoto> getVerificationImage() {
    // TODO: implement getVerificationImage
    //throw UnimplementedError();
  }

  @override
  Future<void> setVerificationApproved(VerificationApproved approved) {
    // TODO: implement setVerificationApproved
    //throw UnimplementedError();
  }

  @override
  Future<void> setVerificationImage(VerificationPhoto verificationImagePath) {
    // TODO: implement setVerificationImage
    //throw UnimplementedError();
  }
}
