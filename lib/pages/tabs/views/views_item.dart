import 'package:loft/denim_icons.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/chat.dart';
import 'package:loft/utils/image.dart';
import 'package:flutter/material.dart';

class ViewsItem extends StatefulWidget {
  final GestureTapCallback onTapCallback;
  final GestureTapCallback startChatCallback;
  final String name;
  final String city;
  final String country;
  final String time;
  final String token;
  final int age;
  final int online;
  final UserPhoto avatar;
  final int gender;
  final bool blocked;
  final bool deleted;

  ViewsItem({
    this.onTapCallback,
    this.startChatCallback,
    this.name,
    this.city,
    this.country,
    this.time,
    this.token,
    this.age,
    this.online,
    this.avatar,
    this.gender,
    this.blocked = false,
    this.deleted = false,
  });

  @override
  _ViewsItemState createState() => _ViewsItemState();
}

class _ViewsItemState extends State<ViewsItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTapCallback,
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 80,
              height: 80,
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.center,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Container(
                      color: Colors.black,
                      child: AspectRatio(
                        aspectRatio: 1 / 1,
                        child: ImageUtils.imageWidget(widget.avatar, widget.gender)
                      ),
                    ),
                  ),
                  Positioned(
                    right: 4,
                    bottom: 4,
                    child: Visibility(
                      visible: widget.online == 1 ? true : false,
                      child: Container(
                        width: 14,
                        height: 14,
                        decoration: BoxDecoration(
                          color: DenimColors.colorSpecialOnlineDefault,
                          borderRadius: BorderRadius.circular(100),
                          border: Border.all(
                            color: Colors.white,
                            width: 2,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: false,
                    child: Positioned(
                      left: -11,
                      child: Container(
                        width: 6,
                        height: 6,
                        decoration: BoxDecoration(
                          color: DenimColors.colorPrimary,
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(width: 12),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${widget.name}, ${widget.age}',
                    overflow: TextOverflow.ellipsis,
                    style: DenimFonts.titleBody(context),
                  ),
                  Text(
                    '${widget.city}, ${widget.country}',
                    overflow: TextOverflow.ellipsis,
                    style: DenimFonts.titleCaption(
                        context, DenimColors.colorBlackSecondary),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    ChatUtils.getCleanMessageTsForDialogItem(widget.time),
                    overflow: TextOverflow.ellipsis,
                    style: DenimFonts.titleFootnote(
                        context, DenimColors.colorBlackSecondary),
                  ),
                ],
              ),
            ),
            Visibility(
              visible: true,
              child: IconButton(
                icon: Icon(
                  DenimIcon.section_tabbar_tab3,
                  size: 32,
                  color: Colors.black,
                ),
                onPressed: widget.startChatCallback,
              ),
            )
          ],
        ),
      ),
    );
  }
}
