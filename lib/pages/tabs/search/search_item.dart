import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/tabs/profile/profile_view.dart';
import 'package:loft/pages/tabs/search/search_full_profile.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/verification.dart';
import 'package:loft/widgets/image.dart';
import 'package:flutter/material.dart';

class SearchPageItem extends StatefulWidget {
  final ShortUserProfile shortUser;
  final String token;

  SearchPageItem({this.shortUser, this.token});

  @override
  _SearchPageItemState createState() => _SearchPageItemState();
}

class _SearchPageItemState extends State<SearchPageItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      onTap: () {
        VerificationUtils.deleteOrBlockedUserTap(
            context, widget.shortUser.blocked, widget.shortUser.deleted, () {
          if (widget.shortUser.id == App.me.user.id) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProfileViewPage(),
              ),
            );
            return;
          }
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SearchFullProfileScreen(userId: widget.shortUser.id),
            ),
          );
        });
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        verticalDirection: VerticalDirection.down,
        children: <Widget>[
          Stack(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  color: Colors.white,
                  child: AspectRatio(
                      aspectRatio: 1 / 1,
                      child: widget.shortUser.avatar != null
                          ? DImage(
                              imageUrl:
                                  '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${widget.shortUser.avatar.preview.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
                              httpHeaders: {
                                'Authorization': 'token ${widget.token}'
                              },
                              errorWidget: (context, url, error) => Image.asset(
                                  'assets/images/icons/universal-userphoto-rounded-male.png'),
                              fit: BoxFit.cover,
                            )
                          : Image.asset(
                              'assets/images/universal-userphoto-hero-${widget.shortUser.gender == 1 || null ? 'male' : 'female'}.jpg',
                              fit: BoxFit.cover,
                            )),
                ),
              ),
              Positioned(
                right: 0,
                bottom: 0,
                child: Visibility(
                  visible: widget?.shortUser?.online == 1 ? true : false,
                  child: Image.asset(
                    'assets/images/icons/section-search-statusonline.png',
                    width: 20,
                  ),
                ),
              ),
              Positioned(
                left: 8,
                bottom: 8,
                child: Visibility(
                  visible: widget.shortUser.photosCount > 0 ? true : false,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                    decoration: BoxDecoration(
                        color: DenimColors.colorTranslucentPrimary,
                        borderRadius: BorderRadius.circular(6)),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          DenimIcon.section_search_carduser_photovalue,
                          color: Colors.white,
                          size: 14,
                        ),
                        SizedBox(width: 5),
                        Text(
                          '${widget.shortUser.photosCount}',
                          style:
                              DenimFonts.titleFootnote(context, Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '${widget.shortUser.name}, ${widget.shortUser.age}',
                  overflow: TextOverflow.ellipsis,
                  style: DenimFonts.titleBody(context),
                ),
                SizedBox(height: 2),
                Text(
                  '${widget.shortUser.city}, ${widget.shortUser.country}',
                  overflow: TextOverflow.ellipsis,
                  style: DenimFonts.titleFootnote(
                    context,
                    DenimColors.colorBlackSecondary,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
