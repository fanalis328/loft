import 'package:loft/denim_icons.dart';
import 'package:flutter/material.dart';

class SearchPageHeader extends StatefulWidget {
  final GestureTapCallback onTap;

  SearchPageHeader({this.onTap});

  @override
  _SearchPageHeaderState createState() => _SearchPageHeaderState();
}

class _SearchPageHeaderState extends State<SearchPageHeader> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Stack(
        children: <Widget>[
          Container(
            height: 56,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/loft-logo.jpg',
                  width: 96,
                ),
              ],
            ),
          ),
          Positioned(
            right: 10,
            top: 0,
            child: Container(
              height: 56,
              child: IconButton(
                icon: Icon(DenimIcon.section_search_filter, size: 22),
                onPressed: widget.onTap,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
