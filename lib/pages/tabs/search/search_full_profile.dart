import 'package:carousel_slider/carousel_slider.dart';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/tabs/chat/chat_dialog.dart';
import 'package:loft/pages/tabs/profile/base_profile_layout.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/locator.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/utils/verification.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/gallery/gallery_overlay_view.dart';
import 'package:loft/widgets/image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loft/pages/report_user/report_user.dart';

class SearchFullProfileScreen extends StatefulWidget {
  final String userId;
  SearchFullProfileScreen({this.userId});

  @override
  _SearchFullProfileScreenState createState() =>
      _SearchFullProfileScreenState();
}

class _SearchFullProfileScreenState extends State<SearchFullProfileScreen> {
  CarouselSlider galleryInstance;
  bool toggleText = true;
  String userId;
  int currentIndex = 0;
  double opacityLevel = 1.0;
  Profile searchUserData;
  bool blocked;
  bool deleted;
  String token;
  ProfileData me = App.me.user;

  @override
  void initState() {
    getUser();
    super.initState();
  }

  getToken() async {
    token = await authDao.getToken();
  }

  getUser() {
    getToken();
    searchApi.getUserById(widget?.userId, (Map data) {
      if (data['ctrl']['code'] == 200) {
        setState(() {
          blocked = data['data']['user']['blocked'];
          deleted = data['data']['user']['deleted'];
          userId = data['data']['user']['id'];
          searchUserData = Profile.fromJson(data['data']['user']);
        });
      } else {
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: searchUserScreenWidget(context));
  }

  Widget searchUserScreenWidget(context) {
    if (searchUserData == null) {
      return CircularLoadingIndicator();
    } else {
      return Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Container(
              child: Column(
                children: <Widget>[
                  SafeArea(
                    top: true,
                    child: Stack(
                      children: <Widget>[
                        searchUserData?.photos != null &&
                                searchUserData.photos.length > 0
                            ? galleryInstance = CarouselSlider(
                                items: searchUserData?.photos?.map((i) {
                                  return Builder(
                                    builder: (BuildContext context) {
                                      return Container(
                                        color: Colors.black,
                                        constraints: BoxConstraints.expand(),
                                        child: GestureDetector(
                                          child: DImage(
                                            imageUrl:
                                                '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${i.preview.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
                                            httpHeaders: {
                                              'Authorization': 'token $token'
                                            },
                                            fit: BoxFit.cover,
                                            errorWidget:
                                                (context, url, error) => Icon(
                                              Icons.error,
                                              size: 60,
                                              color: Colors.white,
                                            ),
                                          ),
                                          onTap: () {
                                            openViewGallery(
                                              context,
                                              currentIndex,
                                            );
                                          },
                                        ),
                                      );
                                    },
                                  );
                                })?.toList(),
                                initialPage: 0,
                                aspectRatio: 1 / 1,
                                viewportFraction: 1.0,
                                enableInfiniteScroll:
                                    searchUserData.photos.length <= 1
                                        ? false
                                        : true,
                                onPageChanged: (val) {
                                  setState(() {
                                    currentIndex = val;
                                  });
                                },
                              )
                            : AspectRatio(
                                aspectRatio: 1 / 1,
                                child: Container(
                                  color: Colors.black,
                                  child: Image.asset(
                                    'assets/images/universal-userphoto-hero-${searchUserData.gender == 1 || searchUserData.gender == null ? 'male' : 'female'}.jpg',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                        Positioned(
                          right: 0,
                          top: 0,
                          child: Visibility(
                            visible: searchUserData.photos != null &&
                                    searchUserData.photos.length > 0
                                ? true
                                : false,
                            child: Container(
                              padding: EdgeInsets.all(15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 15),
                                    height: 32,
                                    decoration: BoxDecoration(
                                      color:
                                          DenimColors.colorTranslucentPrimary,
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Center(
                                      child: Text(
                                        '${currentIndex + 1} / ${searchUserData.photos.length}',
                                        style: DenimFonts.titleBody(
                                          context,
                                          Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          left: 15,
                          bottom: 15,
                          child: AnimatedOpacity(
                            opacity: searchUserData.gender == 2 &&
                                    searchUserData.photos != null &&
                                    searchUserData.photos.length > 0
                                ? opacityLevel
                                : 0,
                            duration: Duration(milliseconds: 220),
                            child: Container(
                              height: 32,
                              padding: EdgeInsets.fromLTRB(8, 0, 12, 0),
                              decoration: BoxDecoration(
                                color: DenimColors.colorTranslucentPrimary,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Image.asset(
                                    'assets/images/icons/section-profileview-photoverified.png',
                                    width: 16,
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    AppTranslations.of(context)
                                        .text('profile_approved'),
                                    style: DenimFonts.titleFootnote(
                                      context,
                                      Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              '${searchUserData.name}, ${ProfileUtils.getAge(searchUserData.birthday)}',
                              style: DenimFonts.titleLarge(context),
                              textAlign: TextAlign.left,
                            ),
                            SizedBox(height: 3),
                            Text(
                              searchUserData.online == 1
                                  ? AppTranslations.of(context).text('online')
                                  : AppTranslations.of(context).text('offline'),
                              style: DenimFonts.titleFootnote(
                                context,
                                DenimColors.colorBlackTertiary,
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 120),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Visibility(
                          visible:
                              searchUserData.about.length <= 1 ? false : true,
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              toggleText && searchUserData.about.length > 80
                                  ? searchUserData.about.substring(0, 80) +
                                      '...'
                                  : searchUserData.about,
                              maxLines: 100,
                              style: DenimFonts.titleBody(context),
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        SizedBox(height: 4),
                        Visibility(
                          visible:
                              searchUserData.about.length < 80 ? false : true,
                          child: Align(
                            alignment: Alignment.bottomLeft,
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  toggleText = !toggleText;
                                });
                              },
                              child: Text(
                                toggleText
                                    ? AppTranslations.of(context).text('expand')
                                    : AppTranslations.of(context)
                                        .text('roll_up'),
                                style: DenimFonts.titleFootnote(
                                  context,
                                  DenimColors.colorPrimary,
                                ),
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        buildUserInformationRow(
                          AppTranslations.of(context)
                              .text('profile_country_title'),
                          searchUserData.country.title.toString(),
                        ),
                        buildUserInformationRow(
                          AppTranslations.of(context)
                              .text('profile_location_title'),
                          searchUserData.city.title.toString(),
                        ),
                        // buildUserInformationRow(
                        //   AppTranslations.of(context)
                        //       .text('profile_citizenship_title'),
                        //   searchUserData.citizenship.title.toString(),
                        // ),
                        Visibility(
                          visible: searchUserData.zodiac == null ? false : true,
                          child: buildUserInformationRow(
                            AppTranslations.of(context)
                                .text('profile_zodiak_title'),
                            AppTranslations.of(context)
                                .text('zodiac_${searchUserData.zodiac}'),
                          ),
                        ),
                        Visibility(
                          visible: searchUserData.height == 0 &&
                                  searchUserData.weight == 0
                              ? false
                              : true,
                          child: buildUserInformationRow(
                            AppTranslations.of(context)
                                .text('profile_appearance_title'),
                            '${AppTranslations.of(context).text('profile_height_title')} ${searchUserData.height == 0 ? 'скрыт' : searchUserData.height.toString() + AppTranslations.of(context).text('height_registration_step_unit_title')}, ${AppTranslations.of(context).text('profile_weight_title')} ${searchUserData.weight == 0 ? 'скрыт' : searchUserData.weight.toString() + AppTranslations.of(context).text('weight_registration_step_unit_title')}',
                          ),
                        ),
                        SizedBox(height: 16),
                        Visibility(
                          visible: true,
                          child: BorderListItem(
                            title: Text(
                              AppTranslations.of(context).text('сomplain'),
                              style: DenimFonts.titleBtnDefault(
                                context,
                                DenimColors.colorBlackSecondary,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ReportUser(
                                  userId: userId,
                                  userName: searchUserData.name,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Visibility(
              visible: chatBtnVisibility(),
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 0, 20),
                child: buidProfileActionButtonList([
                  ProfileActionButton(
                    icon: Icon(
                      DenimIcon.section_profileview_action_message,
                      color: DenimColors.colorPrimary,
                      size: 40,
                    ),
                    onTap: () => goToChat(),
                  ),
                ]),
              ),
            ),
          ),
          SafeArea(
            child: Row(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(10),
                  width: 40,
                  height: 40,
                  child: RawMaterialButton(
                    shape: CircleBorder(),
                    fillColor: DenimColors.colorTranslucentPrimary,
                    child: Icon(
                      DenimIcon.universal_close,
                      size: 24,
                      color: Colors.white,
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }

  bool chatBtnVisibility() {
    print(['_____ PREFS ___:', me.preferences?.gender]);
    bool visibility = false;
    if (me.preferences?.gender == searchUserData.gender ||
        me.preferences?.gender == 3) visibility = true;
    if (me.preferences?.gender == null || me.preferences?.gender == 0) {
      me.profile.gender == searchUserData.gender
          ? visibility = false
          : visibility = true;
    }
    return visibility;
  }

  reportUser(Map data) {
    if (data != null && data['ctrl']['code'] == 200) {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context).text('report_success_sent'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context).text('report_error_title'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
          );
        },
      );
    }
  }

  Widget buildUserInformationRow(String title, String subtitle, [callback]) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Card(
              elevation: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    callback == null ? title : callback(),
                    style: DenimFonts.titleDefault(context),
                  ),
                  SizedBox(height: 4),
                  Text(
                    subtitle,
                    style: DenimFonts.titleBody(
                      context,
                      DenimColors.colorBlackSecondary,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Row buidProfileActionButtonList(List<ProfileActionButton> buttons) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: buttons
          .map(
            (button) => buildProfileActionButton(
              button.icon,
              button.onTap,
            ),
          )
          .toList(),
    );
  }

  Widget buildProfileActionButton(
    Icon icon,
    GestureTapCallback onTap,
  ) {
    return Container(
      width: 72,
      height: 72,
      margin: EdgeInsets.symmetric(horizontal: 6),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(50),
        border: Border.all(width: 1, color: DenimColors.colorTranslucentLine),
        boxShadow: [
          BoxShadow(
            color: DenimColors.colorTranslucentLine,
            blurRadius: 10,
            offset: Offset(0, 6),
          ),
        ],
      ),
      child: RawMaterialButton(
        shape: CircleBorder(),
        child: icon,
        onPressed: onTap,
      ),
    );
  }

  void openViewGallery(BuildContext context, final int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          token: token,
          search: true,
          galleryItems: searchUserData.photos,
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: index,
        ),
      ),
    ).then((currentIndex) {
      int index = currentIndex < 0 ? 0 : currentIndex;
      galleryInstance?.jumpToPage(index);
    });
  }

  goToChat() async {
    VerificationUtils.deleteOrBlockedUserTap(context, deleted, blocked,
        () async {
      ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
      if (currentUser.profile.gender == 2 && currentUser.moderated != 1 ||
          currentUser.profile.gender == 2 &&
              currentUser.profile.avatar.moderated != 1 ||
          currentUser.profile.gender == 1 && currentUser.subscription == null ||
          currentUser.profile.gender == 1 &&
              purchaseUtils.getPurchaseStatus(currentUser.subscription)) {
        await VerificationUtils.verificationListener(context, () {
          afterAllChecks();
        });
      } else {
        afterAllChecks();
      }
    });
  }

  afterAllChecks() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChatDialogPage(
          topic: userId,
        ),
      ),
    );

    if (me.profile.gender == 1) {
      firebaseAnalytics.sendAnalyticsEvent('dialog_start_M');
    } else {
      firebaseAnalytics.sendAnalyticsEvent('dialog_start_W');
    }
  }
}
