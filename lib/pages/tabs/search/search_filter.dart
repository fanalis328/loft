import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/model/websocket/search_filter.dart';
import 'package:loft/pages/registration_steps/country_registration.dart';
import 'package:loft/pages/registration_steps/location_registration.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/locator.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/default_radio_button_group.dart';
import 'package:loft/widgets/default_range_slider.dart';
import 'package:loft/widgets/fake_form_select.dart';
import 'package:loft/widgets/payment_radio_button_group.dart';
import 'package:flutter/material.dart';

class SearchPageFilter extends StatefulWidget {
  @override
  _SearchPageFilterState createState() => _SearchPageFilterState();
}

class _SearchPageFilterState extends State<SearchPageFilter> {
  GlobalKey _scaffold = GlobalKey();
  ProfileData user;
  // UserLocation _citizenshipValue;
  UserLocation _countryValue;
  UserLocation _locationValue;
  SearchFilters filters = SearchFilters();
  RangeValues _startAgeValuesFemale = RangeValues(18, 65);
  RangeValues _startHeightValuesFemale = RangeValues(150, 190);
  RangeValues _startWeightValuesFemale = RangeValues(40, 110);

  RangeValues _startAgeValuesMale = RangeValues(18, 65);
  RangeValues _startHeightValuesMale = RangeValues(150, 190);
  RangeValues _startWeightValuesMale = RangeValues(40, 110);

  RangeValues _ageValues;
  RangeValues _heightValues;
  RangeValues _weightValues;

  int selectedGenderItem = 0;
  int genderId;

  bool disableCountry = false;
  bool disableCity = false;

  @override
  void initState() {
    defaultFilters(false);
    super.initState();
  }

  defaultFilters(bool reset) async {
    user = await userDao.getCurrentUser(App.me.user);

    switch (reset) {
      case true:
        filterDefaultGender(reset);
        disableCity = false;

        filters.age = [
          _startAgeValuesFemale.start.round(),
          _startAgeValuesFemale.end.round()
        ];
        filters.height = [
          _startHeightValuesFemale.start.round(),
          _startHeightValuesFemale.end.round()
        ];
        filters.weight = [
          _startWeightValuesFemale.start.round(),
          _startWeightValuesFemale.end.round()
        ];
        // filters.city =
        //     user.profile.city.id == '194' || user.profile.city.id == '255' || user.profile.city.id == '401'
        //         ? user.profile.city
        //         : null;
        // _locationValue = null;
        _locationValue = user.profile.city;
        _countryValue = user.profile.country;
        break;

      case false:
        disableCity = filters.country == null || filters.country.id != null
            ? false
            : true;
        filterDefaultGender(reset);

        _countryValue =
            filters.country == null ? user.profile.country : filters.country;
        _locationValue = filters.city == null ? null : filters.city;
        // _locationValue = filters.city == null
        //     ? user.profile.city.id == '194' || user.profile.city.id == '255'
        //         ? user.profile.city
        //         : null
        //     : filters.city;

        if (user.profile.gender == 1) {
          _ageValues = filters.age == null
              ? _startAgeValuesFemale
              : RangeValues(filters.age[0] * 1.0, filters.age[1] * 1.0);
          _heightValues = filters.height == null
              ? _startHeightValuesFemale
              : RangeValues(filters.height[0] * 1.0, filters.height[1] * 1.0);
          _weightValues = filters.weight == null
              ? _startWeightValuesFemale
              : RangeValues(filters.weight[0] * 1.0, filters.weight[1] * 1.0);
        } else {
          _ageValues = filters.age == null
              ? _startAgeValuesMale
              : RangeValues(filters.age[0] * 1.0, filters.age[1] * 1.0);
          _heightValues = filters.height == null
              ? _startHeightValuesMale
              : RangeValues(filters.height[0] * 1.0, filters.height[1] * 1.0);
          _weightValues = filters.weight == null
              ? _startWeightValuesMale
              : RangeValues(filters.weight[0] * 1.0, filters.weight[1] * 1.0);
        }

        break;
      default:
        return;
    }

    setState(() {});
  }

  filterDefaultGender(bool reset) {
    //________ RESET TRUE
    if (reset) {
      switch (user.preferences.gender) {
        case 0:
          filters.gender = user.profile.gender == 1 ? 2 : 1;
          selectedGenderItem = user.profile.gender == 1 ? 1 : 0;
          genderId = user.profile?.gender == 1 ? 2 : 1;
          _ageValues = _startAgeValuesFemale;
          _heightValues = _startHeightValuesFemale;
          _weightValues = _startWeightValuesFemale;
          break;
        case 1:
          filters.gender = 1;
          selectedGenderItem = 0;
          genderId = 1;
          _ageValues = _startAgeValuesMale;
          _heightValues = _startHeightValuesMale;
          _weightValues = _startWeightValuesMale;
          break;
        case 2:
          filters.gender = 2;
          selectedGenderItem = 1;
          genderId = 2;
          _ageValues = _startAgeValuesFemale;
          _heightValues = _startHeightValuesFemale;
          _weightValues = _startWeightValuesFemale;
          break;
        case 3:
          filters.gender = null;
          selectedGenderItem = 2;
          genderId = null;
          _ageValues = _startAgeValuesMale;
          _heightValues = _startHeightValuesMale;
          _weightValues = _startWeightValuesMale;
          break;
        default:
      }
    }

    //_________RESET FALSE
    if (!reset) {
      if (filters.gender != null) {
        selectedGenderItem = filters.gender - 1;
        genderId = filters.gender;
      }

      if (filters.gender == null) {
        switch (user.preferences.gender) {
          case 0:
            selectedGenderItem = user.profile.gender == 1 ? 1 : 0;
            genderId = user.profile.gender == 1 ? 2 : 1;
            break;
          case 1:
            selectedGenderItem = 0;
            genderId = 1;
            break;
          case 2:
            selectedGenderItem = 1;
            genderId = 2;
            break;
          case 3:
            selectedGenderItem = 2;
            genderId = null;
            break;
          default:
            return;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    setState(() {});
    return Scaffold(
      key: _scaffold,
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
            actionTitleCallBack: () {
              defaultFilters(true);
            },
            backArrowIcon: Icon(
              DenimIcon.universal_close,
              size: 24,
            ),
            mainTitle: AppTranslations.of(context).text('filters'),
            actionTitle: AppTranslations.of(context).text('reset'),
          ),
          Expanded(
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              children: <Widget>[
                SizedBox(height: 16),
                Visibility(
                  visible: App.me.user.preferences?.gender == null ||
                          App.me.user.preferences?.gender == 0 ||
                          App.me.user.preferences?.gender == 3
                      ? true
                      : false,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        AppTranslations.of(context).text('gender'),
                        style: DenimFonts.titleBody(context),
                      ),
                      SizedBox(height: 8),
                      Container(
                        height: 32,
                        child: DefaultRadioButtonGroup(
                          items: buildRadioButtonList(),
                          selectedItem: selectedGenderItem,
                          onChange: selectGenderItem,
                        ),
                      ),
                      SizedBox(height: 28),
                    ],
                  ),
                ),
                DefaultRangeSlider(
                  title: AppTranslations.of(context).text('age'),
                  slider: RangeSlider(
                    min: 18,
                    max: 65,
                    values: _ageValues,
                    activeColor: Colors.black,
                    onChanged: (RangeValues values) {
                      if (!mounted) return;
                      setState(
                        () {
                          _ageValues = values;
                        },
                      );
                    },
                  ),
                ),
                SizedBox(height: 28),
                FakeFormSelect(
                  disable: disableCountry,
                  label:
                      AppTranslations.of(context).text('profile_country_title'),
                  value: _countryValue == null ? '' : _countryValue.title,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (
                        context,
                      ) {
                        return CountryRegistrationStep(
                          pageType: PageType.FILTER,
                        );
                      }),
                    ).then((country) {
                      if (country == null) return;
                      _countryValue = country;
                      if (_countryValue.id == null) {
                        disableCity = true;
                        _locationValue = UserLocation(
                          id: null,
                          title: AppTranslations.of(context)
                              .text('filters_city_disable'),
                        );
                      } else {
                        disableCity = false;
                        _locationValue = null;
                      }
                      setState(() {});
                    });
                  },
                ),
                SizedBox(height: 28),
                FakeFormSelect(
                  disable: disableCity,
                  label: AppTranslations.of(context).text('search_city_title'),
                  value: _locationValue == null ? '' : _locationValue.title,
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (
                        context,
                      ) {
                        return LocationRegistrationStep(
                          pageType: PageType.FILTER,
                          country: _countryValue,
                        );
                      }),
                    ).then((location) {
                      if (location == null) return;
                      _locationValue = location;
                      setState(() {});
                    });
                  },
                ),
                SizedBox(height: 28),
                DefaultRangeSlider(
                  title:
                      AppTranslations.of(context).text('profile_height_title'),
                  slider: RangeSlider(
                    min: 150,
                    max: 220,
                    values: _heightValues,
                    activeColor: Colors.black,
                    onChanged: (RangeValues values) {
                      setState(
                        () {
                          _heightValues = values;
                        },
                      );
                    },
                  ),
                ),
                SizedBox(height: 8),
                DefaultRangeSlider(
                  title:
                      AppTranslations.of(context).text('profile_weight_title'),
                  slider: RangeSlider(
                    min: 40,
                    max: 150,
                    values: _weightValues,
                    activeColor: Colors.black,
                    onChanged: (RangeValues values) {
                      setState(
                        () {
                          _weightValues = values;
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    onPressed: () {
                      filters.offset = 0;
                      filters.gender = genderId == 3 ? null : genderId;
                      filters.age = [
                        _ageValues.start.round(),
                        _ageValues.end.round(),
                      ];
                      filters.height = [
                        _heightValues.start.round(),
                        _heightValues.end.round(),
                      ];
                      filters.weight = [
                        _weightValues.start.round(),
                        _weightValues.end.round(),
                      ];
                      filters.city =
                          _locationValue == null ? null : _locationValue;
                      filters.country =
                          _countryValue == null ? null : _countryValue;

                      firebaseAnalytics.sendAnalyticsEvent('filter');
                      yandexAnalytics.sendYandexAnalyticsEventWithParams(
                          'filter', filters.toJson());
                      facebookAnalytics.sendFacebookAnalyticsEvent(
                          facebookAnalytics.eventNameSearched);
                      Navigator.pop(context, filters);
                    },
                    title: AppTranslations.of(context).text('btn_apply_title'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void selectGenderItem(index) {
    setState(() {
      selectedGenderItem = index;
      genderId = index + 1;
    });
  }

  List<RadioButton> buildRadioButtonList() {
    List<RadioButton> _radioBtnList;

    _radioBtnList = [
      RadioButton(
        title:
            AppTranslations.of(context).text('geder_registration_step_btn_1'),
      ),
      RadioButton(
        title:
            AppTranslations.of(context).text('geder_registration_step_btn_2'),
      ),
    ];

    if (App.me.user.preferences != null &&
        App.me.user.preferences.gender == 3) {
      _radioBtnList.add(
        RadioButton(
          title:
              AppTranslations.of(context).text('geder_registration_step_btn_3'),
        ),
      );
    }
    return _radioBtnList;
  }
}
