import 'package:loft/denim_icons.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/model/websocket/chat.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/chat.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/widgets/gallery/gallery_overlay_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChatMessageItem extends StatefulWidget {
  final ChatMessageItemWidget message;

  ChatMessageItem({
    this.message,
  });

  @override
  _ChatMessageItemState createState() => _ChatMessageItemState();
}

class _ChatMessageItemState extends State<ChatMessageItem> {
  @override
  Widget build(BuildContext context) {
    return buildMessageItem();
  }

  buildMessageItem() {
    switch (widget.message.type) {
      case 0:
        return buildMessageTextItem();
        break;
      case 1:
        return buildMessagePhotoItem();
      default:
        return;
    }
  }

  void openViewGallery() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          search: true,
          showCounter: false,
          galleryItems: [
            UserPhoto(
                original: ImageFile(path: widget.message.imageUrl),
                preview: widget.message.imageUrl)
          ],
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: 0,
        ),
      ),
    ).then((val) {
      return;
    });
  }

  Widget buildMessageTextItem() {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(12, 1, 12, 1),
          child: Row(
            mainAxisAlignment: widget.message.incoming
                ? MainAxisAlignment.start
                : MainAxisAlignment.end,
            children: <Widget>[
              Visibility(
                visible: widget.message.incoming ? false : true,
                child: SizedBox(width: 74),
              ),
              Expanded(
                child: Align(
                  alignment: widget.message.incoming
                      ? Alignment.centerLeft
                      : Alignment.centerRight,
                  child: Stack(
                    alignment: AlignmentDirectional.centerStart,
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(minWidth: 80),
                        padding: EdgeInsets.fromLTRB(12, 12, 12, 13),
                        decoration: BoxDecoration(
                          color: widget.message.incoming
                              ? DenimColors.colorBlackCell
                              : DenimColors.colorPrimary,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Text(
                          widget.message.content.toString(),
                          style: DenimFonts.titleBody(
                            context,
                            widget.message.incoming
                                ? Colors.black
                                : Colors.white,
                          ),
                        ),
                      ),
                      Positioned(
                        left: -18,
                        child: Visibility(
                          visible:
                              widget.message.read && !widget.message.incoming,
                          child: Container(
                            height: 8,
                            width: 8,
                            decoration: BoxDecoration(
                              color: DenimColors.colorPrimaryShade,
                              borderRadius: BorderRadius.circular(100),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 1,
                        right: 10,
                        child: Visibility(
                          visible: true,
                          child: Text(
                            ChatUtils.getCleanMessageTs(widget.message.ts),
                            style: TextStyle(
                              fontFamily: 'RobotoRegular',
                              fontSize: 10,
                              color: widget.message.incoming
                                  ? DenimColors.colorTranslucentSecondary
                                  : Colors.white60,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Visibility(
                visible: widget.message.incoming ? true : false,
                child: SizedBox(width: 74),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget buildMessagePhotoItem() {
    return InkWell(
      onTap: () => openViewGallery(),
      child: Container(
        padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
        child: Row(
          mainAxisAlignment: widget.message.incoming
              ? MainAxisAlignment.start
              : MainAxisAlignment.end,
          children: <Widget>[
            Visibility(
              visible: widget.message.incoming ? false : true,
              child: SizedBox(width: 74),
            ),
            Expanded(
              child: Align(
                alignment: widget.message.incoming
                    ? Alignment.centerLeft
                    : Alignment.centerRight,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: <Widget>[
                      AspectRatio(
                        aspectRatio: 1 / 1,
                        child: ImageUtils.imageWidget(UserPhoto(
                            original: ImageFile(path: widget.message.imageUrl),
                            preview: widget.message.imageUrl)),
                      ),
                      Visibility(
                        visible: widget.message.loading,
                        child: AspectRatio(
                          aspectRatio: 1 / 1,
                          child: Container(
                            color: Colors.white70,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: widget.message.loading,
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: DenimColors.colorTranslucentPrimary,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: widget.message.loading,
                        child: SpinKitDualRing(
                          color: Colors.white,
                          size: 27.0,
                          lineWidth: 2,
                        ),
                      ),
                      Visibility(
                        visible: false,
                        child: IconButton(
                          icon: Icon(
                            DenimIcon.universal_close,
                            color: Colors.white,
                          ),
                          onPressed: () => widget.message.removeLoadingPhoto(),
                        ),
                      ),
                      Positioned(
                        right: 6,
                        bottom: 6,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                          decoration: BoxDecoration(
                            color: DenimColors.colorTranslucentSecondary,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Text(
                            ChatUtils.getCleanMessageTs(widget.message.ts),
                            style:
                                DenimFonts.titleCaption(context, Colors.white),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Visibility(
              visible: widget.message.incoming ? true : false,
              child: SizedBox(width: 74),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatMessagesBox extends StatefulWidget {
  @override
  _ChatMessagesBoxState createState() => _ChatMessagesBoxState();
}

class _ChatMessagesBoxState extends State<ChatMessagesBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[],
      ),
    );
  }
}
