import 'dart:io';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/db/user_dao.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/model/websocket/search_filter.dart';
import 'package:loft/pages/registration_steps/birthday_registration.dart';
import 'package:loft/pages/registration_steps/country_registration.dart';
import 'package:loft/pages/registration_steps/height_registration.dart';
import 'package:loft/pages/registration_steps/location_registration.dart';
import 'package:loft/pages/registration_steps/search_gender.dart';
import 'package:loft/pages/registration_steps/weight_registration.dart';
import 'package:loft/pages/tabs_page.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/utils/validators.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/fake_form_select.dart';
import 'package:loft/widgets/image.dart';
import 'package:loft/widgets/input.dart';
import 'package:loft/widgets/popups/posting_photo_rules.dart';
import 'package:loft/widgets/popups/user_description_rules.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:loft/widgets/textarea.dart';
import 'package:flutter/material.dart';
import 'package:loft/widgets/gallery/gallery_overlay_view.dart';
import 'package:flutter/rendering.dart';
import 'package:transparent_image/transparent_image.dart';
import '../../../locator.dart';

class ProfileEditPage extends StatefulWidget {
  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  TextEditingController _profileDescriptionController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  List<UserPhoto> galleryItems = [];
  List<Widget> gridGallery = [];
  String _birthDayValue = '';
  String _cityName = '';
  UserLocation _cityValue;
  String _countryName = '';
  UserLocation _countryValue;
  String _weightValue = '';
  String _heightValue = '';
  bool _hideHeightSettings;
  bool _hideWeightSettings;
  String _genderPrefName = '';
  Preferences _preferencesValue;
  ProfileData user = App.me.user;
  String avatarId;
  int width;
  double gridHeight;
  String token;
  SearchFilters searchFilters;

  @override
  void initState() {
    getToken();
    fetchUser();
    super.initState();
  }

  getToken() async {
    token = await authDao.getToken();
  }

  fetchUser() async {
    ProfileData currentUser = await userDao.getCurrentUser(user);
    _hideWeightSettings = currentUser.settings.hideWeight;
    _hideHeightSettings = currentUser.settings.hideHeight;
    avatarId = currentUser.profile.avatar?.id;

    setState(() {
      _countryValue = user.profile.country;
      _cityValue = user.profile.city;
      _nameController = TextEditingController(text: currentUser.profile.name);
      _profileDescriptionController =
          TextEditingController(text: currentUser.profile.about);
      _birthDayValue =
          ProfileUtils.getCleanBirthday(currentUser.profile.birthday);
      _cityName = currentUser.profile.city.title;
      _countryName = currentUser.profile.country.title;
      _weightValue = currentUser.profile.weight.toString();
      _heightValue = currentUser.profile.height.toString();
      _genderPrefName = genderPref(currentUser.preferences?.gender);
      _preferencesValue = currentUser.preferences;
      user.profile.avatar = currentUser.profile.avatar;
    });
  }

  String genderPref(int genderIndex) {
    print('gender pref___: $genderIndex');
    switch (genderIndex) {
      case 1:
        return AppTranslations.of(context).text('gender_pref_$genderIndex');
        break;
      case 2:
        return AppTranslations.of(context).text('gender_pref_$genderIndex');
        break;
      case 3:
        return AppTranslations.of(context).text('gender_pref_$genderIndex');
        break;
      default:
        return '';
    }
  }

  initGridGalleryHeight([items]) {
    var length = items.length + 1;
    var remainder = length % 3;
    var itemsCount = 3;
    var itemHeight = (width) / 3;
    double height;
    if (remainder == 0) {
      height = length / itemsCount * itemHeight;
    } else if (remainder > 0 && remainder < 0.5) {
      height = (length / itemsCount).round() * itemHeight;
    } else if (remainder > 1) {
      height = (length / itemsCount).round() * itemHeight;
    } else {
      height = ((length / itemsCount) + 1).round() * itemHeight;
    }

    gridHeight = height;
    return gridHeight;
  }

  @override
  Widget build(BuildContext context) {
    width = MediaQuery.of(context).size.width.round();
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle: false,
            mainTitle: AppTranslations.of(context).text('profile_edit_title'),
            backArrowTap: () => Navigator.pop(context),
          ),
          SizedBox(height: 8),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                FutureBuilder(
                    future: ProfileUtils.getUserImages(),
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.active:
                        case ConnectionState.waiting:
                          return CircularLoadingIndicator();
                          break;
                        case ConnectionState.none:
                          return Center(
                            child: Text(AppTranslations.of(context)
                                .text('blocking_no_user')),
                          );
                          break;
                        case ConnectionState.done:
                          if (snapshot.data == null) {
                            return Center(
                              child: Text(
                                AppTranslations.of(context)
                                    .text('blocking_error'),
                                style: DenimFonts.titleDefault(context),
                              ),
                            );
                          } else {
                            updateGalleryItemsList(snapshot.data);
                            return SizedBox(
                              height: initGridGalleryHeight(snapshot.data),
                              child: GridView.count(
                                shrinkWrap: false,
                                physics: ScrollPhysics(),
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                crossAxisSpacing: 10,
                                mainAxisSpacing: 10,
                                crossAxisCount: 3,
                                children: buidGrid(snapshot.data, context),
                              ),
                            );
                          }
                          break;
                        default:
                          return Text(AppTranslations.of(context)
                              .text('blocking_error'));
                      }
                    }),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => _showPostingPhotoRulesDialog(),
                        child: Text(
                          AppTranslations.of(context)
                              .text('add_photo_rules_page_title'),
                          style: DenimFonts.titleFootnote(
                            context,
                            DenimColors.colorBlackTertiary,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 28,
                      ),
                      DenimInput(
                        labelText: AppTranslations.of(context)
                            .text('profile_name_title'),
                        controller: _nameController,
                        onChanged: (val) {
                          _nameController = TextEditingController(text: val);
                        },
                      ),
                      SizedBox(height: 28),
                      FakeFormSelect(
                        label: AppTranslations.of(context)
                            .text('profile_birthday_title'),
                        value: _birthDayValue,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (
                              context,
                            ) {
                              return BirthDayRegistrationStep(
                                pageType: PageType.EDIT_PROFILE,
                              );
                            }),
                          ).then((data) {
                            if (data == null) return;
                            setState(() {
                              _birthDayValue =
                                  ProfileUtils.getCleanBirthday(data);
                            });
                          });
                        },
                      ),
                      SizedBox(height: 28),
                      FakeFormSelect(
                        label: AppTranslations.of(context)
                            .text('profile_country_title'),
                        value: _countryName == null ? '' : _countryName,
                        onTap: () => selectCountry(),
                      ),
                      SizedBox(height: 28),
                      FakeFormSelect(
                        label: AppTranslations.of(context)
                            .text('profile_location_title'),
                        value: _cityName == null ? '' : _cityName,
                        onTap: () => selectCity(),
                      ),
                      SizedBox(height: 36),
                      Text(
                        AppTranslations.of(context)
                            .text('profile_edit_gender_pref_title'),
                        style: DenimFonts.titleSection(context),
                      ),
                      SizedBox(height: 15),
                      FakeFormSelect(
                        label: AppTranslations.of(context).text('look_for'),
                        value: _genderPrefName,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (
                              context,
                            ) {
                              return SearchGenderRegistrationStep(
                                pageType: PageType.EDIT_PROFILE,
                              );
                            }),
                          ).then((preferences) {
                            if (preferences == null) return;
                            setState(() {
                              _genderPrefName = genderPref(preferences?.gender);
                              _preferencesValue = preferences;
                            });
                          });
                        },
                      ),
                      SizedBox(height: 36),
                      Text(
                        AppTranslations.of(context)
                            .text('profile_edit_personal_information_title'),
                        style: DenimFonts.titleSection(context),
                      ),
                      SizedBox(height: 19),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: FakeFormSelect(
                              label: AppTranslations.of(context)
                                  .text('profile_height_title'),
                              value: _heightValue +
                                  ' ${AppTranslations.of(context).text('height_registration_step_unit_title')}',
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (
                                    context,
                                  ) {
                                    return HeightRegistrationStep(
                                      pageType: PageType.EDIT_PROFILE,
                                    );
                                  }),
                                ).then((data) {
                                  if (data == null) return;
                                  _hideHeightSettings = data['settings'];
                                  setState(() {
                                    _heightValue = data['height'].toString();
                                  });
                                });
                              },
                            ),
                          ),
                          SizedBox(width: 32),
                          Expanded(
                            child: FakeFormSelect(
                              label: AppTranslations.of(context)
                                  .text('profile_weight_title'),
                              value: _weightValue +
                                  ' ${AppTranslations.of(context).text('weight_registration_step_unit_title')}',
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (
                                    context,
                                  ) {
                                    return WeightRegistrationStep(
                                      pageType: PageType.EDIT_PROFILE,
                                    );
                                  }),
                                ).then((data) {
                                  if (data == null) return;
                                  _hideWeightSettings = data['settings'];
                                  setState(() {
                                    _weightValue = data['weight'].toString();
                                  });
                                });
                              },
                            ),
                          )
                        ],
                      ),
                      // SizedBox(height: 20),
                      // FakeFormSelect(
                      //   label: 'Семейное положение',
                      //   value: _genderPrefName,
                      //   onTap: () => print('Семейное положение')
                      // ),
                      SizedBox(height: 36),
                      Text(
                        AppTranslations.of(context)
                            .text('profile_description_title'),
                        style: DenimFonts.titleSection(context),
                      ),
                      SizedBox(height: 15),
                      Textarea(
                        controller: _profileDescriptionController,
                        hintText: AppTranslations.of(context).text(
                            'user_description_registration_step_input_hint'),
                      ),
                      GestureDetector(
                        onTap: () => _showUserDescriptionRulesDialog(),
                        child: Text(
                          AppTranslations.of(context)
                              .text('about_me_rules_page_title'),
                          style: DenimFonts.titleFootnote(
                            context,
                            DenimColors.colorBlackTertiary,
                          ),
                        ),
                      ),
                      SizedBox(height: 40),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: AppTranslations.of(context).text('btn_done_title'),
                    onPressed: () => saveUserData(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  addLoadImageButtonToGrid(scaffoldContext) {
    gridGallery.add(
      Container(
        child: RawMaterialButton(
          splashColor: DenimColors.colorPrimary,
          highlightColor: DenimColors.colorPrimaryTint,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(
              width: 1,
              color: DenimColors.colorPrimary,
            ),
          ),
          onPressed: () => Snackbar.showLoadPhotoFromSnackbar(
              scaffoldContext, addPhotoToProfile),
          child: Icon(
            DenimIcon.section_profileedit_addphoto,
            size: 56,
            color: DenimColors.colorPrimary,
          ),
        ),
      ),
    );
  }

  addPhotoToProfile([String loadFrom]) async {
    ImageFile image = loadFrom == 'camera'
        ? await ImageUtils.getPhotoFromCamera()
        : await ImageUtils.getPhotoFromGallery();
    if (image == null) {
      return;
    } else {
      await ProfileUtils.addImageToProfile(image, () {
        if (!mounted) return;
        setState(() {});
      });
    }
  }

  selectCity() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (
        context,
      ) {
        return LocationRegistrationStep(
          country: _countryValue,
          pageType: PageType.EDIT_PROFILE,
        );
      }),
    ).then((city) {
      if (city == null) return;
      _cityValue = city;
      setState(() {
        _cityName = _cityValue.title;
      });
    });
  }

  selectCountry() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (
        context,
      ) {
        return CountryRegistrationStep(
          pageType: PageType.EDIT_PROFILE,
        );
      }),
    ).then((country) {
      if (country == null) return;
      _countryValue = country;
      setState(() {
        _cityValue = null;
        _cityName = null;
        _countryName = _countryValue.title;
      });
    });
  }

  updateGalleryItemsList(List<UserPhoto> items) {
    galleryItems = items;
    initGridGalleryHeight(galleryItems);
    return galleryItems;
  }

  List<Widget> buidGrid(List<UserPhoto> items, context) {
    gridGallery = [];
    addLoadImageButtonToGrid(context);
    items.map((item) {
      var index = items.indexOf(item);
      gridGallery.add(buildGridGalleryItem(index, item, context));
    }).toList();
    return gridGallery;
  }

  void openViewGallery(BuildContext context, final int index) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => GalleryPhotoViewWrapper(
          galleryItems: galleryItems,
          backgroundDecoration: const BoxDecoration(
            color: Colors.black,
          ),
          initialIndex: index,
          editMod: true,
        ),
      ),
    );
  }

  _showPostingPhotoRulesDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return PostingPhotoRules();
      },
    );
  }

  saveUserData() async {
    bool validateName =
        ValidatorUtils.validateIsEmptyField(_nameController.text);
    bool validateCity = ValidatorUtils.validateIsEmptyField(_cityValue?.id);
    bool validateCountry =
        ValidatorUtils.validateIsEmptyField(_countryValue?.id);
    bool validateWeight = ValidatorUtils.validateIsEmptyField(_weightValue);
    bool validateHeight = ValidatorUtils.validateIsEmptyField(_heightValue);
    bool validateDescription =
        ValidatorUtils.validateIsEmptyField(_profileDescriptionController.text);

    if (validateName &&
        validateCity &&
        validateWeight &&
        validateHeight &&
        validateDescription &&
        validateCountry) {
      user.profile.name = _nameController.text;
      user.profile.city = _cityValue;
      user.profile.citizenship = _countryValue;
      user.profile.height = int.parse(_heightValue);
      user.profile.weight = int.parse(_weightValue);
      user.profile.about = _profileDescriptionController.text;
      user.settings.hideWeight = _hideWeightSettings;
      user.settings.hideHeight = _hideHeightSettings;
      // preferences
      user.preferences = _preferencesValue;
      // gender prefs временный фикс
      SearchFilters().gender = SearchFilters.searchUserByGenderPreferences(
        _preferencesValue?.gender,
      );
      // update User
      userApi.updateAccount(
        ProfileData(
          profile: user.profile,
          settings: user.settings,
          preferences: user.preferences,
        ),
        updateProfileCallback,
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context)
                  .text('popup_edit_profile_empty_filed_error'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
            cancelBtnCallBack: () => Navigator.pop(context),
          );
        },
      );
    }
  }

  updateProfileCallback(Map data) async {
    if (data['ctrl']['code'] == 200) {
      await userDao.updateUser(user);
      await settingsDao.setSettings(user.settings.toJson());
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context)
                  .text('popup_edit_profile_data_updated'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context)
                .text('popup_edit_profile_back_to_profile'),
            cancelBtnCallBack: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TabsPage(initialIndex: 3),
              ),
            ),
          );
        },
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context).text('blocking_error_connection'),
              style: DenimFonts.titleBody(context),
            ),
            cancelBtnTitle: AppTranslations.of(context).text('clear'),
            cancelBtnCallBack: () => Navigator.pop(context),
          );
        },
      );
      return;
    }
  }

  _showUserDescriptionRulesDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return UserDescriptionRules();
      },
    );
  }

  deleteGalleryItem(List<UserPhoto> galleryItems, ProfileData user,
      UserDao _userDao, UserPhoto item, bldContext) {
    if (galleryItems.length <= 1) {
      Snackbar.showSnackbar(
          bldContext,
          Text(AppTranslations.of(context).text('blocking_photo_delete') +
              '. Сначала загрузите новые фотографии'),
          2);
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return DefaultAlertDialog(
            title: Text(
              AppTranslations.of(context).text('delete_photo'),
              style: DenimFonts.titleBody(context),
            ),
            actionButton: GestureDetector(
              onTap: () {
                ProfileUtils.deleteGalleryItem(
                  galleryItems,
                  user,
                  _userDao,
                  item,
                  () {
                    setState(() {});
                    Navigator.pop(context);
                  },
                  () {
                    Snackbar.showSnackbar(bldContext);
                    Navigator.pop(context);
                  },
                );
              },
              child: Text(
                AppTranslations.of(context).text('delete'),
                style: DenimFonts.titleLink(
                  context,
                  DenimColors.colorPrimary,
                ),
              ),
            ),
            cancelBtnTitle:
                AppTranslations.of(context).text('btn_сancel_title'),
          );
        },
      );
    }
  }

  Widget buildGridGalleryItem(index, UserPhoto galleryItem, context) {
    return Container(
      child: GestureDetector(
        onTap: () => openViewGallery(context, index),
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(10),
              ),
              constraints: BoxConstraints.expand(),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: imageWidget(galleryItem),
              ),
            ),
            Visibility(
              visible: galleryItem.moderated == 0 ? true : false,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: DenimColors.colorTranslucentSecondary,
                ),
                constraints: BoxConstraints.expand(),
                child: Center(
                  child: Icon(
                    DenimIcon.section_profileedit_photomoderation,
                    color: Colors.white,
                    size: 48,
                  ),
                ),
              ),
            ),
            Visibility(
              visible: galleryItem.moderated == 2 ? true : false,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: DenimColors.colorTranslucentSecondary,
                ),
                constraints: BoxConstraints.expand(),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/images/icons/section-moderation-reject.png',
                        width: 45,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 6,
              right: 6,
              child: SizedBox(
                width: 24,
                height: 24,
                child: RawMaterialButton(
                  fillColor: DenimColors.colorTranslucentPrimary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  onPressed: () => deleteGalleryItem(
                      galleryItems, user, userDao, galleryItem, context),
                  child: Icon(
                    DenimIcon.section_profileedit_deletephoto,
                    color: Colors.white,
                    size: 12,
                  ),
                ),
              ),
            ),
            Visibility(
              visible: galleryItem.id == avatarId ? true : false,
              child: Positioned(
                top: 6,
                left: 6,
                child: Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    color: DenimColors.colorTranslucentPrimary,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Icon(
                    DenimIcon.section_profileview_photomain,
                    color: Colors.white,
                    size: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  imageWidget(UserPhoto item) {
    if (item.original.path != null &&
        item.original.path.contains(appConfig.downloadImageEndpoint)) {
      ProfileUtils.downloadAndUpdateUserPhoto(item);
      return DImage(
        imageUrl:
            '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${item.preview != null ? item.preview.split("/").last : item.original.path.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
        fit: BoxFit.cover,
        httpHeaders: {'Authorization': 'token $token'},
        errorWidget: (context, url, error) => Icon(
          Icons.error,
          size: 60,
          color: Colors.white,
        ),
      );
    }

    return FadeInImage(
      // FIXME:
      placeholder: MemoryImage(kTransparentImage),
      // FIXME:
      image: FileImage(
        File(item.original.path),
      ),
      fit: BoxFit.cover,
    );
  }
}
