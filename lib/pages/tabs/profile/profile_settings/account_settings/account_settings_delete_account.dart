import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/services/navigation_service.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/label_checkbox.dart';
import 'package:loft/widgets/textarea.dart';
import 'package:flutter/material.dart';
import 'package:loft/route_paths.dart' as routes;

class AccountSettingsDeleteAccountPage extends StatefulWidget {
  @override
  _AccountSettingsDeleteAccountPageState createState() =>
      _AccountSettingsDeleteAccountPageState();
}

class _AccountSettingsDeleteAccountPageState
    extends State<AccountSettingsDeleteAccountPage> {
  bool _allowAnswer = true;
  NavigationService _navigationService = locator<NavigationService>();
  TextEditingController editingController = TextEditingController();
  bool disableBtn = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            mainTitle: AppTranslations.of(context).text('delete_account'),
            visibleActionTitle: false,
            backArrowTap: () => Navigator.pop(context),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  Textarea(
                    controller: editingController,
                    minLines: 4,
                    autofocus: true,
                    maxLines: 6,
                    hintText:
                        AppTranslations.of(context).text('reason_removal'),
                    onChanged: (val) {
                      if (val.length > 0) {
                        disableBtn = false;
                      } else {
                        disableBtn = true;
                      }

                      setState(() {});
                    },
                  ),
                  LabeledCheckbox(
                    label: AppTranslations.of(context)
                        .text('account_account_delete_subtitle'),
                    padding: const EdgeInsets.symmetric(horizontal: 0),
                    value: _allowAnswer,
                    onChanged: (bool newValue) {
                      setState(() {
                        _allowAnswer = newValue;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    disable: disableBtn,
                    title: AppTranslations.of(context).text('delete_account'),
                    onPressed: () => userApi.deleteAccount(
                      editingController.text,
                      deleteAccountCallBack,
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  deleteAccountCallBack(Map data) async {
    if (data['ctrl']['code'] == 200) {
      authApi.logout(logoutOnDeleteCallback);
    } else {
      errorDeletePopup();
    }
  }

  logoutOnDeleteCallback(Map data) async {
    if (data['ctrl']['code'] == 200) {
      successDeleted();
      await Future.delayed(Duration(seconds: 4));
      ProfileData user = App.me.user;
      var userId = await authDao.getCurrentUserId();
      user.id = userId;
      await userDao.deleteUser(user);
      await authDao.deleteCurrentUserId();
      await authDao.deleteToken();
      _navigationService.navigateTo(routes.HomeRoute);
    } else {
      errorDeletePopup();
    }
  }

  errorDeletePopup() {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          title: Text(
            AppTranslations.of(context).text('login_error_2'),
          ),
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
          cancelBtnCallBack: () => Navigator.pop(context),
        );
      },
    );
  }

  successDeleted() {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          body: Container(
            padding: EdgeInsets.only(bottom: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  DenimIcon.universal_done,
                  size: 60,
                ),
                SizedBox(height: 20),
                Text(
                  AppTranslations.of(context).text('profile_deleted'),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
          hiddenButtons: true,
        );
      },
    );
  }
}
