import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_account.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_information.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_lang.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_notifications.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/popups/new_support.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../../locator.dart';

class ProfileSettingsPage extends StatefulWidget {
  @override
  _ProfileSettingsPageState createState() => _ProfileSettingsPageState();
}

class _ProfileSettingsPageState extends State<ProfileSettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            DefaultHeader(
              backArrowTap: () => Navigator.pop(context),
              mainTitle: AppTranslations.of(context)
                  .text('account_settings_header_title'),
              visibleActionTitle: false,
            ),
            buildProfileSettingsItem(
              AppTranslations.of(context)
                  .text('account_settings_notification_title'),
              Icon(
                DenimIcon.section_settings_notifiacations,
                color: Colors.black,
              ),
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProfileSettingsNotificationPage()),
                );
              },
            ),
            buildProfileSettingsItem(
              AppTranslations.of(context)
                  .text('account_settings_account_title'),
              Icon(
                DenimIcon.section_settings_account,
                color: Colors.black,
              ),
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfileSettingsAccountPage(),
                  ),
                );
              },
            ),
            buildProfileSettingsItem(
              AppTranslations.of(context)
                  .text('account_settings_information_title'),
              Icon(
                DenimIcon.section_settings_information,
                color: Colors.black,
              ),
              () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ProfileSettingsInformationPage(),
                  ),
                );
              },
            ),
            buildProfileSettingsItem(
              AppTranslations.of(context)
                  .text('account_settings_сontact_title'),
              Icon(
                DenimIcon.section_settings_feedback,
                color: Colors.black,
              ),
              () => showDialog(
                context: context,
                builder: (context) {
                  return SupportDialogs();
                },
              ),
            ),
            buildProfileSettingsItem(
              AppTranslations.of(context)
                  .text('account_settings_language_title'),
              Icon(
                DenimIcon.section_login_password_show,
                color: Colors.black,
              ),
              () => showDialog(
                context: context,
                builder: (context) {
                  return ProfileSettingsLangPage();
                },
              ),
            ),
            SizedBox(height: 40),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: BorderListItem(
                title: Text(
                  AppTranslations.of(context).text('account_logout'),
                  style: DenimFonts.titleBtnDefault(context),
                ),
                onTap: () => authApi.logout(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildProfileSettingsItem(
    String title,
    Icon icon,
    GestureTapCallback onTap,
  ) {
    return ListTile(
      onTap: onTap,
      leading: icon,
      title: Text(
        title,
        style: DenimFonts.titleBody(context),
      ),
    );
  }
}
