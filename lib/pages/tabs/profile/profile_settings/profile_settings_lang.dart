import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';

import '../../../../locator.dart';

class ProfileSettingsLangPage extends StatefulWidget {
  @override
  _ProfileSettingsLangPageState createState() =>
      _ProfileSettingsLangPageState();
}

class _ProfileSettingsLangPageState extends State<ProfileSettingsLangPage> {
  static final List<String> languagesList = App.me.supportedLanguages;
  static final List<String> languageCodesList = App.me.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
            mainTitle: AppTranslations.of(context)
                .text("account_settings_language_title"),
            visibleActionTitle: false,
          ),
          Expanded(
            child: _buildLanguagesList(),
          )
        ],
      ),
    );
  }

  _buildLanguagesList() {
    return ListView.builder(
      itemCount: languagesList.length,
      itemBuilder: (context, index) {
        return buildProfileSettingsItem(languagesList[index]);
      },
    );
  }

  Widget buildProfileSettingsItem(
    String language,
  ) {
    return ListTile(
      onTap: () {
        langDao.setLang(Locale(languagesMap[language]).toString());
        App.me.onLocaleChanged(Locale(languagesMap[language]));
      },
      title: Text(
        language,
        style: DenimFonts.titleBody(context),
      ),
    );
  }
}
