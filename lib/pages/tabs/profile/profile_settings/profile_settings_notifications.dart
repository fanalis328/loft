import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../../locator.dart';

class ProfileSettingsNotificationPage extends StatefulWidget {
  @override
  _ProfileSettingsNotificationPageState createState() =>
      _ProfileSettingsNotificationPageState();
}

class _ProfileSettingsNotificationPageState
    extends State<ProfileSettingsNotificationPage> {
  bool messagesNotification = true;
  bool viewsNotification = true;
  ProfileSettings _settings = App.me.settings;

  @override
  void initState() {
    loadSettings();
    super.initState();
  }

  loadSettings() async {
    var settings = await settingsDao.getSettings();
    setState(() {
      messagesNotification = settings['pushMsg'];
      viewsNotification = settings['pushView'];
    });
  }

  updateSettings() async {
    _settings.pushMsg = messagesNotification;
    _settings.pushView = viewsNotification;
    await settingsDao.setSettings(_settings.toJson());
    ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
    userApi.updateAccount(ProfileData(profile: currentUser.profile, settings: _settings));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
            mainTitle: AppTranslations.of(context)
                .text('account_settings_notification_title'),
            visibleActionTitle: false,
          ),
          SwitchListTile(
            value: messagesNotification,
            title: Text(
              AppTranslations.of(context)
                  .text('account_notifications_messages_title'),
              style: DenimFonts.titleBody(context),
            ),
            onChanged: (value) {
              setState(() {
                messagesNotification = value;
                updateSettings();
              });
            },
            activeTrackColor: DenimColors.colorBlackSecondary,
            activeColor: Colors.black,
          ),
          SwitchListTile(
            value: viewsNotification,
            title: Text(
              AppTranslations.of(context)
                  .text('account_notifications_views_title'),
              style: DenimFonts.titleBody(context),
            ),
            onChanged: (value) {
              setState(() {
                viewsNotification = value;
                updateSettings();
              });
            },
            activeTrackColor: DenimColors.colorBlackSecondary,
            activeColor: Colors.black,
          ),
        ],
      ),
    );
  }
}
