import 'package:loft/locale/app_translations.dart';
import 'package:loft/pages/tabs/profile/profile_settings/account_settings/account_settings_delete_account.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../../locator.dart';

class ProfileSettingsAccountPage extends StatefulWidget {
  @override
  _ProfileSettingsAccountPageState createState() =>
      _ProfileSettingsAccountPageState();
}

class _ProfileSettingsAccountPageState
    extends State<ProfileSettingsAccountPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => Navigator.pop(context),
            mainTitle: AppTranslations.of(context)
                .text('account_settings_account_title'),
            visibleActionTitle: false,
          ),
          buildProfileSettingsAccountItem(
            AppTranslations.of(context).text('account_account_delete_title'),
            () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AccountSettingsDeleteAccountPage(),
                ),
              );
            },
          ),
          SizedBox(height: 40),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: BorderListItem(
              title: Text(
                AppTranslations.of(context).text('account_logout'),
                style: DenimFonts.titleBtnDefault(context),
              ),
              onTap: () => authApi.logout(),
            ),
          )
        ],
      ),
    );
  }

  Widget buildProfileSettingsAccountItem(String title, GestureTapCallback onTap,
      [String subTitle = '']) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
        child: Row(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: DenimFonts.titleBody(context),
                ),
                Visibility(
                  visible: subTitle.length > 0 ? true : false,
                  child: Text(
                    subTitle,
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
