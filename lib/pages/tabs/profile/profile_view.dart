import 'package:loft/denim_icons.dart';
import 'package:loft/pages/tabs/profile/base_profile_layout.dart';
import 'package:loft/pages/tabs/profile/profile_edit.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ProfileViewPage extends StatefulWidget {
  @override
  _ProfileViewPageState createState() => _ProfileViewPageState();
}

class _ProfileViewPageState extends State<ProfileViewPage> {
  int moderatedUserStatus;

  @override
  Widget build(BuildContext context) {
    return BaseProfileViewLayout(
      floatBtnIcon: Icon(
        DenimIcon.universal_close,
        size: 24,
        color: Colors.white,
      ),
      profileActionButtonList: [
        ProfileActionButton(
          icon: Icon(
            DenimIcon.section_profileview_action_photoadd,
            color: DenimColors.colorPrimary,
            size: 40,
          ),
          onTap: () async {
            ImageFile image = await ImageUtils.getPhotoFromGallery();
            if (image == null) return;
            await ProfileUtils.addImageToProfile(image, () {
              showDialog(
                context: context,
                builder: (context) {
                  return DefaultAlertDialog(
                    title: Text(
                      'Фото успешно добавлено',
                      style: DenimFonts.titleBody(context),
                    ),
                    cancelBtnTitle: 'Понятно',
                  );
                },
              );
            });
          },
        ),
        ProfileActionButton(
          icon: Icon(
            DenimIcon.section_profileview_action_edit,
            color: DenimColors.colorSecondaryDefault,
            size: 40,
          ),
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => ProfileEditPage())),
        ),
      ],
    );
  }
}
