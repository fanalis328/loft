import 'dart:io';
import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/voice.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/denim_black/loft_premium.dart';
import 'package:loft/pages/tabs/profile/profile_edit.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings.dart';
import 'package:loft/pages/tabs/profile/profile_view.dart';
import 'package:loft/services/image_editor/image_editor_interface.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/image.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:loft/locator.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  ImageFile loadingImage;
  UserPhoto avatar;
  ProfileData user = App.me.user;
  String token;
  bool instagram = false;
  VoiceStatus voiceStatus;

  @override
  void initState() {
    getToken();
    getVoiceStatus();
    super.initState();
  }

  getToken() async {
    token = await authDao.getToken();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: FutureBuilder(
          future: userDao.getCurrentUser(user),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.active:
              case ConnectionState.waiting:
                return CircularLoadingIndicator();
                break;
              case ConnectionState.none:
                return Center(
                  child: Text('Пользователь не найден'),
                );
                break;
              case ConnectionState.done:
                if (snapshot.data == null) {
                  return SafeArea(
                    top: true,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 100),
                      child: Center(
                        child: Text(
                          'Ошибка! \n не удалось загрузить профиль',
                          style: DenimFonts.titleDefault(context),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  );
                } else {
                  showAvatarPhoto(snapshot.data.profile.avatar);
                  return Column(
                    children: <Widget>[
                      SafeArea(
                        top: true,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(2, 2, 2, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  DenimIcon.section_account_settings,
                                  color: Colors.black,
                                  size: 30,
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          ProfileSettingsPage(),
                                    ),
                                  );
                                },
                              ),
                              IconButton(
                                icon: Icon(
                                  DenimIcon.section_account_profileedit,
                                  color: Colors.black,
                                  size: 30,
                                ),
                                onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ProfileEditPage(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          children: <Widget>[
                            Stack(
                              overflow: Overflow.visible,
                              alignment: AlignmentDirectional.bottomEnd,
                              children: <Widget>[
                                Container(
                                  width: 104,
                                  height: 104,
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(1),
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              ProfileViewPage(),
                                        ),
                                      );
                                    },
                                    child: avatarWidget(),
                                  ),
                                ),
                                Positioned(
                                  right: -8,
                                  bottom: -8,
                                  child: Visibility(
                                    visible: user.profile.avatar == null &&
                                                user.profile.gender == 2 ||
                                            user.profile.gender == 2 &&
                                                user.moderated != 1 ||
                                            user.profile.gender == 2 &&
                                                user?.profile?.avatar
                                                        ?.moderated !=
                                                    1
                                        ? true
                                        : false,
                                    child: Image.asset(
                                      'assets/images/icons/section-moderation-pending.png',
                                      width: 45,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 10),
                            Text(
                              '${snapshot.data.profile.name}, ${ProfileUtils.getAge(snapshot.data.profile.birthday)}',
                              style: DenimFonts.titleXLarge(context),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 16),
                            Container(
                              width: 215,
                              child: BorderListItem(
                                title: Text(
                                  AppTranslations.of(context)
                                      .text('account_profile_view_title'),
                                  style: DenimFonts.titleBtnSmall(context),
                                ),
                                height: 36,
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ProfileViewPage(),
                                    ),
                                  );
                                },
                              ),
                            ),
                            SizedBox(height: 40),
                          ],
                        ),
                      ),
                      Container(
                        child: user.profile.gender == 2
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          padding: EdgeInsets.all(20),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/voice.jpg'),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                AppTranslations.of(context).text('profile_female_poject_title'),
                                                style: TextStyle(
                                                  fontFamily:
                                                      'MontserratSemiBold',
                                                  fontWeight: FontWeight.w900,
                                                  fontSize: 20,
                                                  height: 1.4,
                                                  color: Colors.white,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                              SizedBox(height: 35),
                                              Container(
                                                child: DenimButton(
                                                  disable: voiceStatus !=
                                                              null &&
                                                          voiceStatus.owner ==
                                                              user.id &&
                                                          voiceStatus
                                                                  .voiceStatus ==
                                                              true
                                                      ? true
                                                      : false,
                                                  icon: voiceStatus != null &&
                                                          voiceStatus.owner ==
                                                              user.id &&
                                                          voiceStatus
                                                                  .voiceStatus ==
                                                              true
                                                      ? Icon(
                                                          DenimIcon
                                                              .universal_checkmark,
                                                          color: Colors.white,
                                                          size: 30)
                                                      : null,
                                                  title: voiceStatus != null &&
                                                          voiceStatus.owner ==
                                                              user.id &&
                                                          voiceStatus
                                                                  .voiceStatus ==
                                                              true
                                                      ? AppTranslations.of(context).text('profile_female_poject_btn_2')
                                                      : AppTranslations.of(context).text('profile_female_poject_btn_1'),
                                                  onPressed: () =>
                                                      addUserToSV(),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            : Visibility(
                                visible:
                                    user.profile.gender == 1 ? true : false,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/account_line.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    FadeInImage(
                                      placeholder:
                                          MemoryImage(kTransparentImage),
                                      image: AssetImage(
                                          'assets/images/icons/section-subscription-symbol.jpg'),
                                      fit: BoxFit.cover,
                                      width: 64,
                                    ),
                                    SizedBox(height: 12),
                                    Visibility(
                                      visible:
                                          snapshot.data.subscription != null
                                              ? false
                                              : true,
                                      child: Text(
                                        AppTranslations.of(context).text(
                                            'account_premium_description'),
                                        style: DenimFonts.titleBody(context),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    // TODO: hide in LOFT
                                    // Visibility(
                                    //   visible:
                                    //       snapshot.data.subscription != null,
                                    //   child: Text(
                                    //     'Denim Black',
                                    //     style: DenimFonts.titleFootnote(context,
                                    //         DenimColors.colorBlackSecondary),
                                    //   ),
                                    // ),
                                    Visibility(
                                      visible:
                                          snapshot.data.subscription != null,
                                      child: Text(
                                        !purchaseUtils.getPurchaseStatus(
                                                snapshot.data.subscription)
                                            ? AppTranslations.of(context).text('activated')
                                            : AppTranslations.of(context).text('denim_black_profile_status_title'),
                                        style: DenimFonts.titleDefault(context),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    SizedBox(height: 20),
                                    Visibility(
                                      visible: true,
                                      child: RawMaterialButton(
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => LoftPremium(),
                                            ),
                                          );
                                        },
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 16),
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        child: Text(
                                          snapshot.data.subscription != null
                                              ? AppTranslations.of(context).text('extend')
                                              : AppTranslations.of(context).text(
                                                  'account_premium_btn_title'),
                                          style: DenimFonts.titleBtnSmall(
                                            context,
                                            Colors.white,
                                          ),
                                        ),
                                        fillColor: DenimColors.colorPrimary,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                      ),
                    ],
                  );
                }
                break;
              default:
                return CircularLoadingIndicator();
            }
          },
        ),
      ),
    );
  }

  Widget buildServiceProfileCard(
    String imageUrl,
    String title,
    String subtitle,
    GestureTapCallback onTap,
  ) {
    return Expanded(
      child: Ink(
        decoration: BoxDecoration(
          color: DenimColors.colorBlackCell,
          borderRadius: BorderRadius.circular(10),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: onTap,
          child: Container(
            padding: EdgeInsets.all(15),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                  width: 30,
                  child: Image.asset(imageUrl),
                ),
                SizedBox(height: 10),
                Text(
                  title,
                  style: DenimFonts.titleSmall(context),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 2),
                Text(
                  subtitle,
                  style: DenimFonts.titleCaption(
                    context,
                    DenimColors.colorBlackSecondary,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  updateAvatar([String loadFrom]) async {
    ImageFile image = loadFrom == 'camera'
        ? await ImageUtils.getPhotoFromCamera()
        : await ImageUtils.getPhotoFromGallery();
    if (image == null) return;
    ImageFile cropedImage = await locator.get<IImageEditor>().cropImage(image);

    Scaffold.of(context).hideCurrentSnackBar();
    if (cropedImage == null) {
      return;
    } else {
      String imageId = await userApi.uploadImage(cropedImage);

      if (imageId != null) {
        await ProfileUtils.updateAvatar(imageId, () {
          setState(() {
            userDao.updateUser(user);
            avatar = user.profile.avatar;
          });
        });
      }
    }
  }

  getVoiceStatus() async {
    voiceStatus = await voiceDao.getVoiceStatus();
    return voiceStatus;
  }

  addUserToSV() {
    if (voiceStatus != null &&
        voiceStatus.owner == user.id &&
        voiceStatus.voiceStatus == true) return;
    userApi.addUserToSV((Map data) async {
      if (data != null && data['ctrl']['code'] == 200) {
        showDialog(
            context: context,
            builder: (context) {
              return DefaultAlertDialog(
                centerButtons: true,
                cancelBtnTitle: AppTranslations.of(context).text('btn_сontinue_title'),
                body: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/operation-complete.jpg',
                        width: 120,
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      AppTranslations.of(context).text('profile_female_poject_btn_2'),
                      style: DenimFonts.titleDefault(context),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 12),
                    Text(
                      AppTranslations.of(context).text('profile_female_poject_popup_1'),
                      style: DenimFonts.titleBody(context),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 32),
                  ],
                ),
              );
            });
        await voiceDao
            .setVoiceStatus(VoiceStatus(owner: user.id, voiceStatus: true));
        setState(() {
          voiceStatus.voiceStatus = true;
        });
      } else {
        Snackbar.showSnackbar(context);
      }
    });
  }

  showAvatarPhoto(UserPhoto userAvatar) {
    if (userAvatar == null && userAvatar.original.path == null) {
      avatar = null;
      return;
    }

    if (userAvatar.original.path.contains(appConfig.downloadImageEndpoint)) {
      avatar = userAvatar;
      ProfileUtils.downloadAndUpdateUserPhoto(userAvatar);
    } else {
      avatar = userAvatar;
    }
  }

  Widget avatarWidget() {
    Future.delayed(Duration(milliseconds: 200));
    if (avatar == null || user?.profile?.avatar?.moderated == 2) {
      return Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
        ),
        child: Center(
            child: Image.asset(
          user.profile.gender == 1
              ? 'assets/images/icons/universal-userphoto-rounded-male.png'
              : 'assets/images/icons/universal-userphoto-rounded-female.png',
          fit: BoxFit.cover,
        )),
      );
    } else if (avatar.original.path.contains(appConfig.downloadImageEndpoint)) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: DImage(
          imageUrl:
              '${appConfig.baseUrl}${appConfig.downloadImageEndpoint}?file=${avatar.preview.split("/").last}&auth=token&secret=${Uri.encodeQueryComponent(App.me.token)}',
          httpHeaders: {'Authorization': 'token $token'},
          errorWidget: (context, url, error) => Icon(
            Icons.error,
            size: 60,
            color: Colors.white,
          ),
          fit: BoxFit.cover,
        ),
      );
    } else {
      return ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: FadeInImage(
          placeholder: MemoryImage(kTransparentImage),
          image: FileImage(File(avatar.original.path)),
          fadeInDuration: Duration(milliseconds: 200),
          fit: BoxFit.cover,
          width: 64,
        ),
      );
    }
  }
}
