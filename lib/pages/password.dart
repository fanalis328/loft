import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/input.dart';
import 'package:flutter/material.dart';

class PasswordPage extends StatefulWidget {
  final String title;
  final TextEditingController controller;
  final String passwordFieldErrorText;
  final bool showSubtitle;
  final bool passwordFieldError;
  final GestureTapCallback backArrowAction;
  final GestureTapCallback submitButtonAction;
  final String submitButtonTitle;

  PasswordPage({
    this.title,
    this.controller,
    this.passwordFieldErrorText = 'Пароль должен быть не менее 6 символов',
    this.showSubtitle = true,
    this.passwordFieldError = false,
    this.backArrowAction,
    this.submitButtonAction,
    this.submitButtonTitle = 'Продолжить',
  });

  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  bool toggleObscurePassword = true;
  // FocusNode focusNode = FocusNode();
  
  //  void afterBuild(_) {
  //   FocusScope.of(context).requestFocus(focusNode);
  // }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback(afterBuild);
    
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: widget.backArrowAction,
          ),
          SizedBox(height: 10),
          Expanded(
            child: Container(
              constraints: BoxConstraints.expand(),
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.title,
                    style: DenimFonts.titleCallout(context),
                    textAlign: TextAlign.left,
                  ),
                  Visibility(
                    visible: widget.showSubtitle,
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 10),
                        Text(
                          AppTranslations.of(context)
                              .text('error_6_characters'),
                          style: DenimFonts.titleFootnote(
                              context, DenimColors.colorBlackTertiary),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  DenimInput(
                    // focusNode: focusNode,
                    autofocus: true,
                    controller: widget.controller,
                    errorText: widget.passwordFieldError ? widget.passwordFieldErrorText : null,
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          toggleObscurePassword = !toggleObscurePassword;
                        });
                      },
                      icon: toggleObscurePassword
                          ? Icon(
                              DenimIcon.section_login_password_hide,
                              color: Colors.black,
                            )
                          : Icon(
                              DenimIcon.section_login_password_show,
                              color: Colors.black,
                            ),
                    ),
                    obscureText: toggleObscurePassword,
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: widget.submitButtonTitle,
                    onPressed: widget.submitButtonAction,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
