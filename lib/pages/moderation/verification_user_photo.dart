import 'dart:io';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';
import 'package:loft/locator.dart';

class VerificationUserPhoto extends StatefulWidget {
  final dynamic data;
  final String title;
  final String description;
  final String btnTitle;
  final bool showHeader;
  final GestureTapCallback onTap;
  final GestureTapCallback backArrowTap;
  final bool moderated;

  VerificationUserPhoto({
    this.data,
    this.title = '',
    this.description = '',
    this.btnTitle = '',
    this.showHeader = false,
    this.onTap,
    this.backArrowTap,
    this.moderated = true,
  });

  @override
  _VerificationUserPhotoState createState() => _VerificationUserPhotoState();
}

class _VerificationUserPhotoState extends State<VerificationUserPhoto> {
  String token;
  ProfileData user = App.me.user;
  var imageWidget;

  @override
  void initState() {
    delayed();
    super.initState();
  }

  delayed() async {
    await Future.delayed(Duration(seconds: 1));
    if (!mounted) return;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Visibility(
            visible: widget.data == null
                ? widget.showHeader
                : widget.data['showHeader'],
            child: DefaultHeader(
              backArrowIcon: Icon(DenimIcon.universal_close),
              backArrowTap: () => widget.data == null
                  ? widget.backArrowTap
                  : Navigator.pop(context),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: widget.data != null && !widget.showHeader
                    ? MainAxisAlignment.start
                    : MainAxisAlignment.center,
                children: <Widget>[
                  Visibility(
                    visible: widget.data != null && !widget.showHeader,
                    child: SizedBox(height: 30),
                  ),
                  Container(
                    child: Stack(
                      overflow: Overflow.visible,
                      alignment: Alignment.center,
                      children: <Widget>[
                        Container(
                          width: 104,
                          height: 104,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: user.profile != null && widget.data == null
                                ? ImageUtils.imageWidget(user.profile.avatar)
                                : FutureBuilder(
                                    future: verificationPhotoDao
                                        .getVerificationImage(),
                                    builder: (context, snapshot) {
                                      if (snapshot.data == null ||
                                          snapshot.data.owner !=
                                              App.me.user.id) {
                                        return Image.asset(
                                          'assets/images/verification/female-verification.jpg',
                                          fit: BoxFit.cover,
                                        );
                                      } else {
                                        return Image(
                                          image: FileImage(
                                              File(snapshot.data.imagePath)),
                                          fit: BoxFit.cover,
                                        );
                                      }
                                    },
                                  ),
                          ),
                        ),
                        Positioned(
                          right: -10,
                          bottom: -10,
                          child: Visibility(
                            visible:
                                widget.data != null && !widget.data['moderated']
                                    ? true
                                    : false,
                            child: Image.asset(
                              'assets/images/icons/section-moderation-reject.png',
                              width: 48,
                            ),
                          ),
                        ),
                        Positioned(
                          right: -10,
                          bottom: -10,
                          child: Visibility(
                            visible: widget.moderated && widget.data == null
                                ? true
                                : false,
                            child: Image.asset(
                              'assets/images/icons/section-moderation-pending.png',
                              width: 48,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    widget.data == null ? widget.title : widget.data['title'],
                    style: DenimFonts.titleDefault(context),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 12),
                  Text(
                    widget.data == null
                        ? widget.description
                        : widget.data['description'],
                    style: DenimFonts.titleBody(context),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 32),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: widget.data == null
                        ? widget.btnTitle
                        : widget.data['btnTitle'],
                    onPressed: widget.data == null
                        ? widget.onTap
                        : widget.data['onTap'],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
