import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:flutter/material.dart';

class CheckpointRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final String title;
  final String titleSecondary;
  final String description;
  final String buttonTitle;
  final GestureTapCallback callback;

  CheckpointRegistrationStep({
    this.bloc,
    this.title = '',
    this.titleSecondary = '',
    this.description = '',
    this.buttonTitle = '',
    this.callback,
  });

  @override
  _CheckpointRegistrationStepState createState() =>
      _CheckpointRegistrationStepState(
        bloc: bloc,
        title: title,
        titleSecondary: titleSecondary,
        description: description,
        buttonTitle: buttonTitle,
        callback: callback,
      );
}

class _CheckpointRegistrationStepState
    extends State<CheckpointRegistrationStep> {
  final RegistrationBloc bloc;
  final String title;
  final String titleSecondary;
  final String description;
  final String buttonTitle;
  final GestureTapCallback callback;

  _CheckpointRegistrationStepState({
    this.bloc,
    this.title,
    this.titleSecondary,
    this.description,
    this.buttonTitle,
    this.callback,
  });

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/operation-complete.jpg',
                  width: 120,
                ),
                SizedBox(height: 15),
                Text(
                  title,
                  style: DenimFonts.titleXLarge(context),
                  textAlign: TextAlign.center,
                ),
                secondaryTitle(),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    children: <Widget>[
                      Text(
                        description,
                        style: DenimFonts.titleBody(context),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 40),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: buttonTitle,
                    onPressed: callback,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget secondaryTitle() {
    return titleSecondary.length > 0
        ? Text(
            titleSecondary,
            style: DenimFonts.titleXLarge(context),
            textAlign: TextAlign.center,
          )
        : Container();
  }
}
