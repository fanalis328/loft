import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/prev_step_registration.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../locator.dart';

class BirthDayRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  BirthDayRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
  });

  @override
  _BirthDayRegistrationStepState createState() =>
      _BirthDayRegistrationStepState(bloc: bloc);
}

class _BirthDayRegistrationStepState extends State<BirthDayRegistrationStep> {
  final RegistrationBloc bloc;

  _BirthDayRegistrationStepState({this.bloc});

  DateTime _birthDay;
  int day;
  int month;
  int year;
  ProfileData user = App.me.user;
  int initDay;
  int initMonth;
  int initYear;
  @override
  void initState() {
    setState(() {
      initDay = DateTime.now().day - 1;
      initMonth = DateTime.now().month - 1;
      initYear = DateTime.now().year;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? RegistrationPrevStep.prevStep(
                      bloc,
                      GetNameStep(),
                    )
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Text(
                    AppTranslations.of(context)
                        .text('birthday_registration_step_title'),
                    style: DenimFonts.titleCallout(context),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 8),
                  Text(
                    AppTranslations.of(context)
                        .text('birthday_registration_step_subtitle'),
                    style: DenimFonts.titleFootnote(
                      context,
                      DenimColors.colorBlackTertiary,
                    ),
                  ),
                  SizedBox(height: 40),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          AppTranslations.of(context).text('day'),
                          style: DenimFonts.titleSection(context),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          AppTranslations.of(context).text('month'),
                          style: DenimFonts.titleSection(context),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                        child: Text(
                          AppTranslations.of(context).text('year'),
                          style: DenimFonts.titleSection(context),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 100,
                          child: CupertinoPicker.builder(
                            itemExtent: 30,
                            childCount: 31,
                            scrollController: FixedExtentScrollController(
                                initialItem: initDay),
                            backgroundColor: Colors.white,
                            onSelectedItemChanged: (data) {
                              setState(() {
                                day = int.parse(handleDayAndMonth(data));
                              });
                            },
                            itemBuilder: (context, data) {
                              return Text(
                                handleDayAndMonth(data),
                                style: DenimFonts.titleCallout(context),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          height: 100,
                          width: 100,
                          child: CupertinoPicker.builder(
                            itemExtent: 30,
                            childCount: 12,
                            scrollController: FixedExtentScrollController(
                                initialItem: initMonth),
                            backgroundColor: Colors.white,
                            onSelectedItemChanged: (data) {
                              setState(() {
                                month = int.parse(handleDayAndMonth(data));
                              });
                            },
                            itemBuilder: (context, data) {
                              return Text(
                                handleDayAndMonth(data),
                                style: DenimFonts.titleCallout(context),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Container(
                          height: 100,
                          width: 100,
                          child: CupertinoPicker.builder(
                            itemExtent: 30,
                            childCount: 101,
                            scrollController: FixedExtentScrollController(
                                initialItem:
                                    initYear - (DateTime.now().year - 100)),
                            backgroundColor: Colors.white,
                            onSelectedItemChanged: (data) {
                              setState(() {
                                year = int.parse(handleYear(data));
                              });
                            },
                            itemBuilder: (context, data) {
                              return Text(
                                handleYear(data),
                                style: DenimFonts.titleCallout(context),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    title: widget.pageType == PageType.REGISTRATION
                        ? AppTranslations.of(context).text('btn_сontinue_title')
                        : AppTranslations.of(context).text('btn_save_title'),
                    onPressed: () => submitBirthDay(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String handleDayAndMonth(int data) => (data + 1).toString();
  String handleYear(int data) => (data + DateTime.now().year - 118).toString();

  void submitBirthDay() {
    day == null ? day = int.parse(handleDayAndMonth(initDay)) : day.toString();
    month == null
        ? month = int.parse(handleDayAndMonth(initMonth))
        : month.toString();
    year == null ? year = initYear - 18 : year.toString();

    String birthDaySrt =
        day.toString() + '/' + month.toString() + '/' + year.toString();
    _birthDay = DateFormat("dd/MM/yyyy").parse(birthDaySrt);

    user.profile.birthday =
        (_birthDay.toUtc().millisecondsSinceEpoch / 1000).round();

    if (widget.pageType == PageType.REGISTRATION) {
      int userAge = int.parse(ProfileUtils.getAge(user.profile.birthday));
      if (userAge < 18) {
        if (!mounted) return;
        Snackbar.showSnackbar(context, Text('Вам еще не исполнилось 18 лет'));
        return;
      }
      userApi.updateAccount(ProfileData(profile: user.profile, settings: App.me.settings), (Map data) {
        if (data['ctrl']['code'] == 200) {
          userDao.updateUser(user);
          bloc.dispatch(GetCountryStep());
          if (user.profile.gender == 1) {
            firebaseAnalytics.sendAnalyticsEvent('reg_step_6_birth_M');
          } else {
            firebaseAnalytics.sendAnalyticsEvent('reg_step_6_birth_W');
          }
        } else {
          Snackbar.showSnackbar(context);
        }
      });
    } else {
      Navigator.pop(context, user.profile.birthday);
    }
  }
}
