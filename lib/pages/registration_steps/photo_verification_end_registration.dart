import 'dart:io';
import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/verification.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/moderation/verification_user_photo.dart';
import 'package:loft/pages/tabs_page.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/material.dart';
import 'package:loft/services/image_picker/index.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/theme/theme_colors.dart';
import '../../locator.dart';

class PhotoVerificationEndStep extends StatefulWidget {
  final ImageFile imageFile;
  final RegistrationBloc bloc;
  final bool reVerification;

  PhotoVerificationEndStep(
      {this.imageFile, this.bloc, this.reVerification = false});

  @override
  _PhotoVerificationEndStepState createState() =>
      _PhotoVerificationEndStepState(imageFile: imageFile, bloc: bloc);
}

class _PhotoVerificationEndStepState extends State<PhotoVerificationEndStep> {
  final ImageFile imageFile;
  final RegistrationBloc bloc;
  _PhotoVerificationEndStepState({this.imageFile, this.bloc});
  ProfileData user = App.me.user;
  bool loading = false;

  @override
  void initState() {
    if (imageFile == null) {
      Snackbar.showSnackbar(context);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(backArrowTap: () {
            if (widget.reVerification) {
              Navigator.pop(context);
            } else {
              bloc.dispatch(GetPhotoVerificationStep());
            }
          }),
          Expanded(
            child: loading
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircularLoadingIndicator(),
                    ],
                  )
                : SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 35),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 10),
                          Text(
                            AppTranslations.of(context).text(
                                'end_verification_registration_step_title'),
                            style: DenimFonts.titleCallout(context),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 15),
                          Text(
                            AppTranslations.of(context).text(
                                'end_verification_registration_step_description'),
                            style: DenimFonts.titleBody(context),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 40),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Flexible(
                                child: FadeInImage(
                                  placeholder: MemoryImage(kTransparentImage),
                                  image: AssetImage(
                                      'assets/images/verification/female-verification.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(width: 15),
                              Flexible(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                    color: Colors.black,
                                    child: FadeInImage(
                                      placeholder:
                                          MemoryImage(kTransparentImage),
                                      image: FileImage(
                                        File(imageFile.path),
                                      ),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30),
                          Container(
                            width: 130,
                            child: BorderListItem(
                              height: 36,
                              onTap: () => widget.reVerification
                                  ? Navigator.pop(context)
                                  : bloc.dispatch(GetPhotoVerificationStep()),
                              title: Text(
                                AppTranslations.of(context).text(
                                    'end_verification_registration_step_btn_title'),
                                style: DenimFonts.titleBtnSmall(context),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    disable: loading,
                    title:
                        AppTranslations.of(context).text('btn_сontinue_title'),
                    onPressed: () => validateUserPhoto(),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  validateUserPhoto() async {
    loading = true;
    if (!mounted) return;
    setState(() {});
    userApi.validateUser(widget.imageFile, (Map data) async {
      if (data['ctrl']['code'] == 200) {
        ImageFile verificationPhoto =
            await ImageUtils.saveImage(widget.imageFile);
        await verificationPhotoDao.setVerificationImage(
          VerificationPhoto(
            owner: user.id,
            imagePath: verificationPhoto.path,
          ),
        );
        await Future.delayed(Duration(milliseconds: 200));
        loading = false;
        if (!mounted) return;
        setState(() {});
        if (widget.reVerification) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => VerificationUserPhoto(
                title: AppTranslations.of(context).text('profile_registration_on_moderation_title'),
                description: AppTranslations.of(context).text('profile_registration_on_moderation_description'),
                btnTitle: AppTranslations.of(context).text('to_begin'),
                onTap: () {
                  ProfileUtils.updateUserProfile();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => TabsPage(initialIndex: 0),
                    ),
                  );
                },
              ),
            ),
          );
        } else {
          bloc.dispatch(GetCheckpointStep());
          firebaseAnalytics.sendAnalyticsEvent('reg_step_9.5_photoconf_comp_W');
          yandexAnalytics.sendYandexAnalyticsEvent(
              yandexAnalytics.eventRegStep9PhotoconfW);
        }
      } else {
        loadingPhotoError();
      }
    });
  }

  loadingPhotoError() {
    if (!mounted) return;
    setState(() {
      loading = false;
    });
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
          title: Text(AppTranslations.of(context).text('blocking_error')),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              AppTranslations.of(context).text('photo_loading_error'),
              style: DenimFonts.titleBody(
                context,
                DenimColors.colorBlackSecondary,
              ),
            ),
          ),
        );
      },
    );
  }
}
