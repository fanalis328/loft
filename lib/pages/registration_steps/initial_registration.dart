import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/data/app.dart';
import 'package:loft/data/repository.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/country_phone_codes.dart';
import 'package:loft/pages/registration_steps/sms_code_registration.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_information.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/utils/validators.dart';
import 'package:loft/utils/connections.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/fake_form_select.dart';
import 'package:loft/widgets/input.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../locator.dart';

class InitRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final PageType pageType;

  InitRegistrationStep({
    this.bloc,
    this.pageType = PageType.REGISTRATION,
  });
  @override
  _InitRegistrationStepState createState() =>
      _InitRegistrationStepState(bloc: bloc);
}

class _InitRegistrationStepState extends State<InitRegistrationStep> {
  final RegistrationBloc bloc;
  DenimInput input;
  String phone;

  _InitRegistrationStepState({this.bloc});

  TextEditingController _phoneController = TextEditingController();
  String _phoneCode = '+7';
  ProfileData user = App.me.user;
  bool errorPhone = false;
  String errorPhoneText = '';
  bool disableNextStepBtn = true;
  int minLengthForDisableBtn = 10;
  int maxLengthForDisableBtn = 10;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? Navigator.pop(context)
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            flex: 1,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Text(
                      AppTranslations.of(context)
                          .text('initial_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                      textAlign: TextAlign.left,
                    ),
                    Text(
                      AppTranslations.of(context)
                          .text('initial_registration_step_subtitle'),
                      style: DenimFonts.titleCallout(context),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 10),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 80,
                          child: FakeFormSelect(
                            value: _phoneCode,
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => CountryPhoneCodes(),
                                ),
                              ).then((code) {
                                if (code == null || code.length == 0) {
                                  _phoneCode = '+7';
                                  if (!mounted) return;
                                  setState(() {
                                    minLengthForDisableBtn = 10;
                                    maxLengthForDisableBtn = 10;
                                  });
                                } else {
                                  if (!mounted) return;
                                  switch (code) {
                                    case '+7':
                                      phoneNumberGuard(code, 10, 10);
                                      break;
                                    case '+373':
                                      phoneNumberGuard(code, 8, 8);
                                      break;
                                    default:
                                      phoneNumberGuard(code, 9, 12);
                                  }
                                }
                              });
                            },
                          ),
                        ),
                        SizedBox(width: 20),
                        Expanded(
                          child: input = DenimInput(
                            autofocus: true,
                            errorText: errorPhone ? errorPhoneText : null,
                            controller: _phoneController,
                            keyboardType: TextInputType.phone,
                            onChanged: (val) {
                              if (_phoneController.text.length <
                                      minLengthForDisableBtn ||
                                  _phoneController.text.length >
                                      maxLengthForDisableBtn) {
                                setState(() {
                                  disableNextStepBtn = true;
                                });
                              } else {
                                setState(() {
                                  disableNextStepBtn = false;
                                });
                              }
                            },
                            hintText: '(000) 000-0000',
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 25),
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'initial_registration_step_description_part_1'),
                          style: DenimFonts.titleFootnote(
                              context, DenimColors.colorBlackTertiary),
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'initial_registration_step_description_part_2'),
                          style: DenimFonts.titleFootnote(context),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewInFlutter(
                                    appBarTitle: AppTranslations.of(context)
                                        .text(
                                            'account_information_terms_title'),
                                    link: 'https://loft-app.com/agreements',
                                  ),
                                ),
                              );
                            },
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'initial_registration_step_description_part_3'),
                          style: DenimFonts.titleFootnote(
                              context, DenimColors.colorBlackTertiary),
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'initial_registration_step_description_part_4'),
                          style: DenimFonts.titleFootnote(context),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewInFlutter(
                                    appBarTitle: AppTranslations.of(context).text(
                                        'account_information_privacy_policy_title'),
                                    link:
                                        'https://loft-app.com/privacy-policy',
                                  ),
                                ),
                              );
                            },
                        ),
                      ]),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    disable: disableNextStepBtn,
                    onPressed: () {
                      widget.pageType == PageType.REGISTRATION
                          ? _submitPhoneInputForRegistration()
                          : _submitPhoneInputForEdit();
                    },
                    title:
                        AppTranslations.of(context).text('btn_сontinue_title'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  phoneNumberGuard(String code, int min, int max) {
    setState(() {
      _phoneCode = code;
      minLengthForDisableBtn = min;
      maxLengthForDisableBtn = max;
      _phoneController.clear();
    });
  }

  _submitPhoneInputForRegistration() async {
    if (validatePhone('registration')) {
      phone = ProfileUtils.getCleanPhone(phoneNumber());
      bool internetConnection =
          await ConnectionUtils.checkInternetConnectivity(context);
      if (!internetConnection) return;
      authApi.validate(phone, validateCallback);
    } else {
      setState(() {
        errorPhone = true;
        errorPhoneText =
            AppTranslations.of(context).text('initial_registration_step_error');
      });
    }
  }

  validateCallback(Map validateData) {
    if (validateData == null) return;

    switch (validateData['ctrl']['code']) {
      case 200:
        String code = validateData['data']['code'] == null
            ? null
            : validateData['data']['code'];
        bloc.dispatch(GetSmsCodeStep(
          code: code,
          status: validateData['data']['status'].toString(),
          phone: phone,
        ));
        firebaseAnalytics.sendAnalyticsEvent('reg_step_1');
        break;
      case 429:
        if (!mounted) return;
        setState(() {
          errorPhone = true;
          errorPhoneText = AppTranslations.of(context).text('login_error_1');
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorPhone = true;
          errorPhoneText = AppTranslations.of(context).text('login_error_2');
        });
        break;
      default:
        return;
    }
  }

  _submitPhoneInputForEdit() async {
    if (validatePhone('edit')) {
      var sms = await Repository.get().getSmsCodeFromFakeApi(phoneNumber());
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SmsCodeRegistrationStep(
            smsCode: sms,
            pageType: PageType.EDIT_PROFILE,
          ),
        ),
      );
    } else {
      setState(() {
        errorPhone = true;
      });
    }
  }

  bool validatePhone(String validateType) {
    var phoneValidation;

    switch (validateType) {
      case 'registration':
        phoneValidation = ValidatorUtils.validatePhoneNumberForRegistration(
          phoneNumber(),
        );
        break;
      case 'edit':
        phoneValidation = ValidatorUtils.validatePhoneNumberForLogin(
          phoneNumber(),
          '+70000000000',
        );
        break;
      default:
        phoneValidation = ValidatorUtils.validatePhoneNumberForRegistration(
          phoneNumber(),
        );
    }
    return phoneValidation;
  }

  String phoneNumber() {
    String phoneNumber = _phoneCode + _phoneController.text;
    return phoneNumber;
  }
}
