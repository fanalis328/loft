import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_event.dart';
import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/voice.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/moderation/verification_user_photo.dart';
import 'package:loft/pages/tabs_page.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/locator.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/utils/validators.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/popups/user_description_rules.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:loft/widgets/textarea.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class UserProfileDescriptionRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;

  UserProfileDescriptionRegistrationStep({this.bloc});
  @override
  _UserProfileDescriptionRegistrationStepState createState() =>
      _UserProfileDescriptionRegistrationStepState(bloc: bloc);
}

class _UserProfileDescriptionRegistrationStepState
    extends State<UserProfileDescriptionRegistrationStep> {
  final RegistrationBloc bloc;

  _UserProfileDescriptionRegistrationStepState({this.bloc});

  ProfileData user = App.me.user;
  TextEditingController _profileDescriptionController = TextEditingController();
  bool errorProfileDescription = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        DefaultHeader(
          backArrowTap: () => bloc.dispatch(
            GetWeightStep(),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Text(
                    AppTranslations.of(context)
                        .text('user_description_registration_step_title'),
                    style: DenimFonts.titleCallout(context),
                  ),
                  SizedBox(height: 8),
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: AppTranslations.of(context).text(
                            'user_description_registration_step_subtitle'),
                        style: DenimFonts.titleFootnote(
                            context, DenimColors.colorBlackSecondary),
                      ),
                      TextSpan(
                        text: AppTranslations.of(context).text('rules'),
                        style: DenimFonts.titleFootnote(context),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => _showUserDescriptionRulesDialog(),
                      )
                    ]),
                  ),
                  SizedBox(height: 16),
                  Textarea(
                    autofocus: true,
                    maxLines: 6,
                    onChanged: (val) {
                      if(val.length > 0) {
                        setState(() {
                          errorProfileDescription = false;
                        });
                      }
                    },
                    errorText: errorProfileDescription
                        ? AppTranslations.of(context)
                            .text('user_description_registration_step_error')
                        : null,
                    controller: _profileDescriptionController,
                    hintText: AppTranslations.of(context)
                        .text('user_description_registration_step_input_hint'),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DenimButton(
                  title: AppTranslations.of(context).text('btn_сontinue_title'),
                  onPressed: () => submitDescription(),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  void submitDescription() {
    if (ValidatorUtils.validateIsEmptyField(
      _profileDescriptionController.text,
    )) {
      user.profile.about = _profileDescriptionController.text;
      userApi.updateAccount(ProfileData(profile: user.profile, settings: user.settings), (Map data) {
        if (data['ctrl']['code'] == 200 && user.id != null) {
          App.me.sendLegacy(
            {'id': socket.requestId(), 'method': 'registerAccount'},
            (Map data) {
              if (data['ctrl']['code'] == 200) {
                userDao.updateUser(user);
                if (user.profile.gender == 1) {
                  bloc.dispatch(GetFinalRegistrationStep());
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => VerificationUserPhoto(
                        title: AppTranslations.of(context).text('profile_registration_on_moderation_title'),
                        description: AppTranslations.of(context).text('profile_registration_on_moderation_description'),
                        btnTitle: AppTranslations.of(context).text('to_begin'),
                        onTap: () async {
                          await voiceDao
                              .setVoiceStatus(VoiceStatus(voiceStatus: false));
                          ProfileUtils.updateUserProfile();
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => TabsPage(initialIndex: 0),
                              ));
                          firebaseAnalytics
                              .sendAnalyticsEvent('complete_registration_W');
                          firebaseAnalytics
                              .sendAnalyticsEvent('reg_step_13_wheight_W');
                          yandexAnalytics.sendYandexAnalyticsEvent(
                              yandexAnalytics.eventRegSuccessW);
                          facebookAnalytics
                              .sendFacebookAnalyticsEventWithParams(
                                  facebookAnalytics
                                      .standardEventCompletedRegistration,
                                  0.00,
                                  'USD');
                        },
                      ),
                    ),
                  );
                }
              } else {
                Snackbar.showSnackbar(context);
              }
            },
          );
        } else {
          Snackbar.showSnackbar(context);
        }
      });
    } else {
      setState(() {
        errorProfileDescription = true;
      });
    }
  }

  _showUserDescriptionRulesDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return UserDescriptionRules();
      },
    );
  }
}
