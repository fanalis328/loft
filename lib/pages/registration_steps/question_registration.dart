import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';

class QuestionTemplate extends StatefulWidget {
  final String questionTitle;
  final List<String> questions;
  final dynamic nextCallback;
  final dynamic prevCallback;

  QuestionTemplate({
    this.questionTitle,
    this.questions,
    this.nextCallback,
    this.prevCallback,
  });

  @override
  _QuestionTemplateState createState() => _QuestionTemplateState();
}

class _QuestionTemplateState extends State<QuestionTemplate> {
  String answer;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        DefaultHeader(
          backArrowTap: () => widget.prevCallback(),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10),
                Text(
                  widget.questionTitle,
                  style: DenimFonts.titleCallout(context),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: 30),
                getTextWidgets(),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget getTextWidgets() {
    return Column(
      children: widget.questions.map(
            (itemTitle) => BorderListItem(
              title: Text(
                itemTitle,
                textAlign: TextAlign.center,
                style: DenimFonts.titleBody(context),
              ),
              onTap: () => widget.nextCallback(widget.questions.indexOf(itemTitle)),
            ),
          ).toList(),
    );
  }
}
