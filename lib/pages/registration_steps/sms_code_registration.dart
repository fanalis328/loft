import 'package:loft/data/app.dart';
import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/moderation/universal_blocking_profile.dart';
import 'package:loft/pages/tabs/profile/profile.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/locator.dart';
import 'package:loft/utils/auth.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/counter.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/input.dart';
import 'package:flutter/material.dart';
import 'package:loft/model/verification.dart';
import 'package:loft/utils/verification.dart';
import 'package:loft/utils/chat.dart';
import 'package:loft/utils/connections.dart';
import 'package:loft/pages/tabs_page.dart';

class SmsCodeRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final String smsCode;
  final String phone;
  final String status;
  final PageType pageType;

  SmsCodeRegistrationStep({
    this.bloc,
    this.smsCode,
    this.phone,
    this.status,
    this.pageType = PageType.REGISTRATION,
  });

  @override
  _SmsCodeRegistrationStepState createState() => _SmsCodeRegistrationStepState();
}

class _SmsCodeRegistrationStepState extends State<SmsCodeRegistrationStep> with TickerProviderStateMixin {
  // FocusNode focusNode;
  static AnimationController _controller;
  static const int kStartValue = 60;
  bool resendSmsCode = true;

  ProfileData user = App.me.user;
  TextEditingController _smsCodeController;
  bool errorSmsCode = false;
  String errorSmsCodeText;
  String sms;
  int reconectCount = 0;

  @override
  void initState() {
    super.initState();
    // focusNode = FocusNode();
    // WidgetsBinding.instance.addPostFrameCallback((_){
    //   FocusScope.of(context).requestFocus(focusNode);
    // });
    _controller = AnimationController(
      vsync: this,
      duration: Duration(minutes: 1),
    );
    _controller.forward(from: 0.0);
    _controller.addStatusListener(statusListener);
    setState(() {
      sms = widget.smsCode;
      resendSmsCode = false;
    });
  }

  @override
  void dispose() {
    _controller.removeStatusListener(statusListener);
    _controller.dispose();
    // focusNode.dispose();
    super.dispose();
  }

  statusListener(val) {
    setState(() {
      resendSmsCode = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () => widget.pageType == PageType.REGISTRATION
                ? widget.bloc.dispatch(GetInitialStep())
                : Navigator.pop(context),
          ),
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Text(
                    '${AppTranslations.of(context).text('sms_registration_step_title')} ${sms == null ? '' : ':' + sms}',
                    style: DenimFonts.titleCallout(context),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 8),
                  Text(
                    '${AppTranslations.of(context).text('sms_registration_step_subtitle')} ${widget.phone}',
                    style: DenimFonts.titleFootnote(context, DenimColors.colorBlackTertiary),
                  ),
                  SizedBox(height: 15),
                  DenimInput(
                    autofocus: true,
                    // focusNode: focusNode,
                    controller: _smsCodeController,
                    keyboardType: TextInputType.number,
                    hintText: AppTranslations.of(context)
                        .text('sms_registration_step_input_hint'),
                    errorText: errorSmsCode ? errorSmsCodeText : null,
                    onChanged: (val) {
                      if (val.length == 4) submitSmsCode(val);
                    },
                  ),
                  SizedBox(height: 25),
                  SizedBox(
                    width: 195,
                    child: RawMaterialButton(
                      elevation: 0,
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      fillColor: !resendSmsCode
                          ? DenimColors.colorBlackLine
                          : Colors.white,
                      child: resendSmsCode
                          ? Text(
                              AppTranslations.of(context).text(
                                  'sms_registration_step_repeat_btn_title'),
                            )
                          : Countdown(
                              style: DenimFonts.titleBtnXSmall(context),
                              animation: StepTween(
                                begin: kStartValue,
                                end: 0,
                              ).animate(_controller),
                            ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(
                          width: 1,
                          color: DenimColors.colorBlackLine,
                        ),
                      ),
                      onPressed: () {
                        if (!resendSmsCode) return;
                        widget.pageType == PageType.REGISTRATION
                            ? authApi.validate(widget.phone, validateCallback)
                            : Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  validateCallback(Map validateData) {
    if (validateData == null) return;

    switch (validateData['ctrl']['code']) {
      case 200:
        String code = validateData['data']['code'] == null
            ? null
            : validateData['data']['code'];
        _controller.forward(from: 0.0);
        setState(() {
          resendSmsCode = false;
          sms = code;
          _smsCodeController = TextEditingController(text: '');
        });
        break;
      case 429:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText =
              'Превышен лимит попыток.\nПопробуйте запросить \nкод позднее.';
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText =
              'Что-то пошло не так.\nПопробуйте позже или \nобратитесь в поддержку.';
        });
        break;
      default:
        return;
    }
  }

  submitSmsCode([String inputSms]) async {
    switch (widget.pageType) {
      case PageType.REGISTRATION:
        bool internetConnection =
            await ConnectionUtils.checkInternetConnectivity(context);
        if (!internetConnection) return;
        authApi.login('${widget.phone}:$inputSms', 'phone', loginCallback);
        break;
      case PageType.EDIT_PROFILE:
        return showDialog(
          context: context,
          builder: (context) {
            return DefaultAlertDialog(
              title: Text('Ваш номер телефона изменен'),
              cancelBtnCallBack: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfilePage())),
              cancelBtnTitle: 'Перейти',
              body: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Теперь вы можете перейти в профиль',
                      textAlign: TextAlign.left,
                      style: DenimFonts.titleBody(
                        context,
                        Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
        break;
      default:
        return;
    }
  }

  loginCallback([Map data]) async {
    if (data == null) return;
    switch (data['ctrl']['code']) {
      case 200:
        await AuthUtils.loginPhoneCallback(data);
        await userApi.getAccount(checkUserRegistrated);
        firebaseAnalytics.sendAnalyticsEvent('reg_step_2');
        firebaseAnalytics.sendAnalyticsEvent('login');
        firebaseAnalytics.logLogin();
        break;
      case 400:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText = 'Код доступа \nвведен неверно';
        });
        break;
      case 500:
        if (!mounted) return;
        setState(() {
          errorSmsCode = true;
          errorSmsCodeText =
              'Что-то пошло не так.\nПопробуйте позже или \nобратитесь в поддержку.';
        });
        break;
      default:
        return;
    }
  }

  checkUserRegistrated([Map data]) async {
    user.id = await authDao.getCurrentUserId();
    if (data == null) return;

    if (data != null &&
        data['ctrl']['code'] == 200 &&
        data['data']['registered'] != null) {
      ProfileUtils.updateUserInstanceFromServer(data,
          (ProfileData userData) async {
        await ChatUtils.updateDialogs();
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => TabsPage()));
        VerificationUtils.checkVerificationCases(data, context);
      });
      return;
    } else if (data != null &&
        data['ctrl']['code'] == 200 &&
        data['data']['blocked']) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UniversalBlockingProfile(
            title: AppTranslations.of(context).text('login_blocked_user'),
          ),
        ),
      );
      return;
    } else if (data['ctrl']['code'] == 200 &&
        data['data']['registered'] == null &&
        widget.status == '1') {
      ProfileUtils.updateUserInstanceFromServer(data,
          (ProfileData profileData) {
        setState(() {
          user = profileData;
        });
        widget.bloc.dispatch(GetGenderStep());
      });
      return;
    } else if (data['ctrl']['code'] == 200) {
      setRegistrationStep(data);
      return;
    }
  }

  setRegistrationStep(Map data) async {
    VerificationPhoto verificationPhoto =
        await verificationPhotoDao.getVerificationImage();
    ProfileUtils.updateUserInstanceFromServer(data, (ProfileData profileData) {
      setState(() {
        user = null;
        user = profileData;
      });

      var profile = data['data']['profile'];
      var birthday = profile['birthday'];
      var city = profile['city'];
      var citizenship = profile['citizenship'];
      var height = profile['height'];
      var avatar = profile['avatar'];
      var prefGender = data['data']['preferences'];
      var country = profile['country'];

      if (profile['gender'] == 0) {
        widget.bloc.dispatch(GetGenderStep());
      } else if (prefGender == null || prefGender['gender'] == 0) {
        widget.bloc.dispatch(GetSearchGenderStep());
      } else if (profile['name'].toString().length == 0) {
        widget.bloc.dispatch(GetNameStep());
      } else if (birthday == 0) {
        widget.bloc.dispatch(GetBirthDayStep());
      } else if (country == null) {
        widget.bloc.dispatch(GetCountryStep());
      } else if (city == null) {
        widget.bloc.dispatch(GetCityStep());
      } else if (citizenship == null) {
        widget.bloc.dispatch(GetCitizenshipStep());
      } else if (avatar == null) {
        widget.bloc.dispatch(GetPhotoStep());
      } else if (data['data']['moderated'] != 1 &&
              profile['gender'] == 2 &&
              avatar != null &&
              verificationPhoto == null ||
          verificationPhoto.owner != user.id) {
        widget.bloc.dispatch(GetPhotoVerificationStep());
      } else if (height == 0) {
        widget.bloc.dispatch(GetHeightStep());
      } else if (profile['weight'] == 0) {
        widget.bloc.dispatch(GetWeightStep());
      } else if (profile['about'].toString() == '') {
        widget.bloc.dispatch(GetUserProfileDescriptionStep());
      } else {
        widget.bloc.dispatch(GetFinalRegistrationStep());
      }
    });
  }

  logoutCallback(data) {
    if (data['ctrl']['code'] == 200) {
      AuthUtils.cleanLogoutCallback(data);
    } else {
      setState(() {
        errorSmsCode = true;
        errorSmsCodeText = AppTranslations.of(context).text('blocking_error_connection_2');
      });
    }
  }
}
