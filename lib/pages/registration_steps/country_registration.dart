import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/enum.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/prev_step_registration.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/input.dart';
import 'package:flutter/material.dart';

class CountryBloc {}

class CountryRegistrationStep extends StatefulWidget {
  final PageType pageType;
  final RegistrationBloc bloc;

  CountryRegistrationStep({
    this.pageType = PageType.REGISTRATION,
    this.bloc,
  });

  @override
  _CountryRegistrationStepState createState() =>
      _CountryRegistrationStepState();
}

class _CountryRegistrationStepState extends State<CountryRegistrationStep> {
  TextEditingController _countryController = TextEditingController();
  bool errorCitizen = false;
  ProfileData user = App.me.user;
  List _countries = [];
  List _countriesForDisplay = [];
  UserLocation selectedCountry;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    getCountries();
    super.initState();
  }

  getCountries() async {
    App.me.sendLegacy({'id': socket.requestId(), 'method': 'getAllCountries'},
        (Map data) async {
      if (data != null &&
          data['ctrl']['code'] == 200 &&
          data['data']['countries'].length > 0) {
        List filteredList = data['data']['countries'];
        print('COUNTRIES LEN _____${filteredList.length}');
        getCountriescallback(filteredList);
        await countriesListDao.setCountriesList(filteredList);
      } else {
        return;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            visibleActionTitle:
                widget.pageType == PageType.REGISTRATION ? true : false,
            backArrowTap: () {
              widget.pageType == PageType.REGISTRATION
                  ? RegistrationPrevStep.prevStep(
                      widget.bloc,
                      GetBirthDayStep(),
                    )
                  : Navigator.pop(context);
            },
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Visibility(
                    visible: true,
                    child: Text(
                      AppTranslations.of(context).text('country_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                    ),
                  ),
                  SizedBox(height: 10),
                  DenimInput(
                    autofocus: true,
                    controller: _countryController,
                    hintText: AppTranslations.of(context)
                        .text('location_registration_step_input_hint'),
                    prefixIcon: Icon(
                      DenimIcon.universal_searchbar_search,
                      color: DenimColors.colorBlackTertiary,
                      size: 20,
                    ),
                    onChanged: (val) {
                      searchCountry(val);
                    },
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    flex: 1,
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: _countriesForDisplay.length,
                      itemBuilder: (BuildContext context, int index) {
                        return BorderListItem(
                          title: Text(
                            _countriesForDisplay[index]['title'],
                            textAlign: TextAlign.center,
                            style: DenimFonts.titleBody(context),
                            maxLines: 1,
                          ),
                          onTap: () {
                            submitCountry(_countriesForDisplay[index]);
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  getCountriescallback(countries) {
    if(widget.pageType == PageType.FILTER) {
      _countries.add({'id': null, 'title': AppTranslations.of(context).text('all_countries')});
    }
    _countries.addAll(countries);
    _countriesForDisplay = _countries;
    setState(() {});
  }

  searchCountry([String text = '']) {
    setState(() {
      _countriesForDisplay = _countries.where((item) {
        var countryName = item['title'].toLowerCase();
        return countryName.contains(text.toLowerCase());
      }).toList();
    });
  }

  void submitCountry(Map country) {
    selectedCountry = UserLocation.fromJson(country);
    user.profile.country = selectedCountry;
    switch (widget.pageType) {
      case PageType.REGISTRATION:
        userApi.updateAccount(
            ProfileData(profile: user.profile, settings: App.me.settings),
            (Map data) {
          if (data['ctrl']['code'] == 200) {
            countriesDao.setCountries([country]);
            userDao.updateUser(user);
            widget.bloc.dispatch(GetCityStep(country: selectedCountry));
          }
        });
        break;
      case PageType.EDIT_PROFILE:
        Navigator.pop(
          context,
          selectedCountry,
        );
        break;
      case PageType.FILTER:
        Navigator.pop(context, selectedCountry);
        break;
      default:
        widget.bloc.dispatch(GetCityStep(country: selectedCountry));
    }
  }
}
