import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_event.dart';
import 'package:loft/pages/password.dart';
import 'package:loft/utils/validators.dart';
import 'package:flutter/material.dart';

class PasswordRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;

  PasswordRegistrationStep({this.bloc});

  @override
  _PasswordRegistrationStepState createState() =>
      _PasswordRegistrationStepState();
}

class _PasswordRegistrationStepState extends State<PasswordRegistrationStep> {
  TextEditingController _passwordController = TextEditingController();
  bool passwordFieldError = false;

  @override
  Widget build(BuildContext context) {
    return PasswordPage(
      title: 'Придумайте пароль',
      controller: _passwordController,
      passwordFieldError: passwordFieldError,
      backArrowAction: () {
        widget.bloc.dispatch(GetSmsCodeStep());
      },
      submitButtonAction: () => validatePassword(),
    );
  }

  validatePassword() {
    if(ValidatorUtils.minLength(_passwordController.text, 6) == true) {
    } else {
      setState(() {
       passwordFieldError = true; 
      });
      return;
    }
  }
}
