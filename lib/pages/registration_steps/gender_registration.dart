import 'package:loft/data/app.dart';
import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/prev_step_registration.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';

class GenderRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  GenderRegistrationStep({this.bloc});
  @override
  _GenderRegistrationStepState createState() =>
      _GenderRegistrationStepState(bloc: bloc);
}

class _GenderRegistrationStepState extends State<GenderRegistrationStep> {
  final RegistrationBloc bloc;

  _GenderRegistrationStepState({this.bloc});
  int _genderType;
  ProfileData user = App.me.user;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowTap: () {
              authApi.logout((Map data) {
                RegistrationPrevStep.prevStep(
                  bloc,
                  GetInitialStep(),
                );
              });
            },
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      AppTranslations.of(context)
                          .text('geder_registration_step_title'),
                      style: DenimFonts.titleCallout(context),
                    ),
                  ),
                  SizedBox(height: 34),
                  BorderListItem(
                    title: Text(
                      AppTranslations.of(context)
                          .text('geder_registration_step_btn_1'),
                      textAlign: TextAlign.center,
                      style: DenimFonts.titleBody(context),
                    ),
                    onTap: () => submitGender(1),
                  ),
                  SizedBox(height: 4),
                  BorderListItem(
                    title: Text(
                      AppTranslations.of(context)
                          .text('geder_registration_step_btn_2'),
                      textAlign: TextAlign.center,
                      style: DenimFonts.titleBody(context),
                    ),
                    onTap: () => submitGender(2),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void submitGender(int gender) async {
    _genderType = gender;
    user.profile.gender = _genderType;
    userApi.updateAccount(
      ProfileData(profile: user.profile, settings: App.me.settings),
      updateGenderStepCallback,
    );
  }

  updateGenderStepCallback(Map data) {
    if (data['ctrl']['code'] == 200) {
      userDao.updateUser(user);
      bloc.dispatch(GetSearchGenderStep());
      if (user.profile.gender == 1) {
        firebaseAnalytics.sendAnalyticsEvent('reg_step_4_M');
      } else {
        firebaseAnalytics.sendAnalyticsEvent('reg_step_4_W');
      }
    } else {
      Snackbar.showSnackbar(context);
    }
  }
}
