import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';

class RegistrationPrevStep {
  static prevStep(RegistrationBloc bloc,RegistrationEvent event) {
    bloc.dispatch(event);
  }
}