import 'dart:async';

import 'package:loft/data/app.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_event.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/prev_step_registration.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/validators.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/input.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/material.dart';

import '../../locator.dart';

class NameRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;

  NameRegistrationStep({this.bloc});

  @override
  _NameRegistrationStepState createState() => _NameRegistrationStepState();
}

class _NameRegistrationStepState extends State<NameRegistrationStep> {
  ProfileData user = App.me.user;
  ProfileSettings settings = App.me.settings;
  TextEditingController _nameController = TextEditingController();
  bool errorName = false;
  // FocusNode focusNode = FocusNode();

  // void afterBuild(_) {
  //   FocusScope.of(context).requestFocus(focusNode);
  // }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback(afterBuild);
    return Column(
      children: <Widget>[
        DefaultHeader(
          backArrowTap: () => RegistrationPrevStep.prevStep(
            widget.bloc,
            GetSearchGenderStep(),
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    AppTranslations.of(context).text('name_registration_step_title'),
                    style: DenimFonts.titleCallout(context),
                  ),
                ),
                SizedBox(height: 10),
                DenimInput(
                  autofocus: true,
                  // focusNode: focusNode,
                  controller: _nameController,
                  hintText: AppTranslations.of(context)
                      .text('name_registration_step_input_hint'),
                  errorText: errorName
                      ? AppTranslations.of(context)
                          .text('name_registration_step_error')
                      : null,
                ),
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(8),
          child: Row(
            children: <Widget>[
              Expanded(
                child: DenimButton(
                  title: AppTranslations.of(context).text('btn_сontinue_title'),
                  onPressed: () => submitName(),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  void submitName() {
    var validateName = ValidatorUtils.validateIsEmptyField(_nameController.text);
    if (validateName) {
      user.profile.name = _nameController.text;
      userApi.updateAccount(ProfileData(profile: user.profile, settings: settings), (Map data) {
        if (data['ctrl']['code'] == 200) {
          userDao.updateUser(user);
          widget.bloc.dispatch(GetBirthDayStep());
          if (user.profile.gender == 1) {
            firebaseAnalytics.sendAnalyticsEvent('reg_step_5_name_M');
          } else {
            firebaseAnalytics.sendAnalyticsEvent('reg_step_5_name_W');
          }
        } else {
          Snackbar.showSnackbar(context);
        }
      });
    } else {
      if (!mounted) return;
      setState(() {
        errorName = true;
      });
    }
  }
}
