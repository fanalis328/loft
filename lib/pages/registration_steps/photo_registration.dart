import 'package:loft/config/config.dart';
import 'package:loft/data/app.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_event.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/tabs_page.dart';
import 'package:loft/services/image_editor/image_editor_interface.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/image.dart';
import 'package:loft/utils/profile.dart';
import 'package:loft/widgets/border_list_item.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/widgets/popups/posting_photo_rules.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../../locator.dart';

class PhotoRegistrationStep extends StatefulWidget {
  final RegistrationBloc bloc;
  final bool reVerification;
  PhotoRegistrationStep({this.bloc, this.reVerification = false});

  @override
  _PhotoRegistrationStepState createState() => _PhotoRegistrationStepState();
}

class _PhotoRegistrationStepState extends State<PhotoRegistrationStep> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PhotoRegistrationStepBody(
        bloc: widget.bloc,
        reVerification: widget.reVerification,
      ),
    );
  }
}

class PhotoRegistrationStepBody extends StatefulWidget {
  final RegistrationBloc bloc;
  final bool reVerification;
  PhotoRegistrationStepBody({this.bloc, this.reVerification = false});

  @override
  _PhotoRegistrationStepBodyState createState() =>
      _PhotoRegistrationStepBodyState();
}

class _PhotoRegistrationStepBodyState extends State<PhotoRegistrationStepBody> {
  ImageFile avatar;
  ProfileData user = App.me.user;
  UserPhoto verificationAvatar;
  bool loading = false;
  String token;
  var imageWidget;

  @override
  void initState() {
    if (widget.reVerification) {
      if (!mounted) return;
      setState(() {
        verificationAvatar = user.profile.avatar;
        user.profile.avatar = null;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        user.profile.avatar = verificationAvatar;
        Navigator.pop(context);
        return;
      },
      child: Column(
        children: <Widget>[
          DefaultHeader(backArrowTap: () async {
            if (widget.reVerification) {
              user.profile.avatar = verificationAvatar;
              Navigator.pop(context);
            } else {
              widget.bloc.dispatch(GetCityStep());
            }
          }),
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(0),
              shrinkWrap: false,
              children: <Widget>[
                Visibility(
                  visible: widget.reVerification,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(height: 20),
                        Visibility(
                          visible: widget.reVerification &&
                                      user.profile.avatar != null ||
                                  loading
                              ? false
                              : true,
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Stack(
                                  overflow: Overflow.visible,
                                  alignment: Alignment.center,
                                  children: <Widget>[
                                    Container(
                                      width: 104,
                                      height: 104,
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        child: verificationAvatar != null
                                            ? ImageUtils.imageWidget(
                                                verificationAvatar)
                                            : Image.asset(
                                                user.profile?.gender == 1
                                                    ? 'assets/images/icons/universal-userphoto-rounded-male.png'
                                                    : 'assets/images/icons/universal-userphoto-rounded-female.png',
                                                fit: BoxFit.cover,
                                              ),
                                      ),
                                    ),
                                    Positioned(
                                      right: -10,
                                      bottom: -10,
                                      child: Visibility(
                                        visible: true,
                                        child: Image.asset(
                                          'assets/images/icons/section-moderation-reject.png',
                                          width: 48,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(height: 16),
                              Text(
                                AppTranslations.of(context).text('main_photo_moderation_error'),
                                style: DenimFonts.titleCallout(context),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(height: 16),
                              Text(
                                AppTranslations.of(context).text('main_photo_moderation_error_2'),
                                textAlign: TextAlign.center,
                                style: DenimFonts.titleBody(context),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: !widget.reVerification,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(36, 8, 36, 0),
                    child: Text(
                      AppTranslations.of(context)
                          .text('photo_registration_step_title'),
                      style: DenimFonts.titleLarge(context),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                SizedBox(height: 24),
                loading ? loadingState() : buildContentStates(),
              ],
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: DenimButton(
                    color: loading
                        ? DenimColors.colorBlackLine
                        : DenimColors.colorPrimary,
                    title: user.profile.avatar == null
                        ? AppTranslations.of(context)
                            .text('photo_registration_step_btn_title')
                        : AppTranslations.of(context)
                            .text('btn_сontinue_title'),
                    onPressed: () {
                      handleAvatarStep();
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  handleAvatarStep() async {
    if (user.profile.avatar == null && !loading) {
      if (!mounted) return;
      await Future.delayed(Duration(milliseconds: 50));
      Snackbar.showLoadPhotoFromSnackbar(context, loadAvatar);
    } else if (user.profile.avatar != null &&
        !loading &&
        !user.profile.avatar.original.path
            .contains(appConfig.downloadImageEndpoint)) {
      if (widget.reVerification) {
        await uploadAndHandleAvatar(user.profile.avatar.original, () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => TabsPage(
                initialIndex: 0,
              ),
            ),
          );
        });
      } else {
        await uploadAndHandleAvatar(
            user.profile.avatar.original, goToPhotoVerificationStep);
      }
    } else {
      goToPhotoVerificationStep();
    }
  }

  Widget loadingState() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1 / 1,
          child: CircularLoadingIndicator(),
        )
      ],
    );
  }

  Widget buidPhotoRulesRow(String title, String src1, String src2) {
    return Container(
      margin: EdgeInsets.only(bottom: 30),
      child: Column(
        children: <Widget>[
          Text(
            title,
            style: DenimFonts.titleFootnote(context),
          ),
          SizedBox(height: 12),
          Row(
            children: <Widget>[
              Expanded(flex: 1, child: Image.asset(src1)),
              SizedBox(width: 10),
              Expanded(flex: 1, child: Image.asset(src2)),
            ],
          )
        ],
      ),
    );
  }

  Widget buildContentStates() {
    if (user.profile.avatar == null) {
      return Container(
          padding: EdgeInsets.symmetric(horizontal: 54),
          child: Column(
            children: <Widget>[
              user.profile?.gender == 1
                  ? Wrap(
                      children: <Widget>[
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_1'),
                          'assets/images/registration/male/rule-1-1.jpg',
                          'assets/images/registration/male/rule-1-2.jpg',
                        ),
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_2'),
                          'assets/images/registration/male/rule-2-1.jpg',
                          'assets/images/registration/male/rule-2-2.jpg',
                        ),
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_3'),
                          'assets/images/registration/male/rule-3-1.jpg',
                          'assets/images/registration/male/rule-3-2.jpg',
                        ),
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_5'),
                          'assets/images/registration/male/rule-5-1.jpg',
                          'assets/images/registration/male/rule-5-2.jpg',
                        ),
                        buildPhotoRulesDescription(1)
                      ],
                    )
                  : Wrap(
                      children: <Widget>[
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_1'),
                          'assets/images/registration/female/rule-1-1.jpg',
                          'assets/images/registration/male/rule-1-2.jpg',
                        ),
                        buidPhotoRulesRow(
                          AppTranslations.of(context)
                              .text('photo_registration_step_subtitle_2'),
                          'assets/images/registration/female/rule-2-1.jpg',
                          'assets/images/registration/female/rule-2-2.jpg',
                        ),
                        buildPhotoRulesDescription(2)
                      ],
                    ),
            ],
          ));
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 16),
          Container(
            width: 160,
            height: 160,
            child: ClipOval(
                child: ImageUtils.imageWidget(App.me.user.profile?.avatar)),
          ),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              BorderListItem(
                margin: 0,
                padding: 16,
                height: 36,
                title: Text(
                  AppTranslations.of(context)
                      .text('photo_registration_step_btn_title_2'),
                  style: DenimFonts.titleBtnSmallSecondary(context),
                ),
                onTap: () =>
                    Snackbar.showLoadPhotoFromSnackbar(context, loadAvatar),
              ),
              SizedBox(width: 8),
              Visibility(
                visible: !widget.reVerification,
                child: Container(
                  width: 36,
                  child: BorderListItem(
                    margin: 0,
                    height: 36,
                    title: Icon(
                      DenimIcon.photoview_deletephoto,
                      size: 24,
                    ),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return DefaultAlertDialog(
                            title: Text(
                              AppTranslations.of(context).text('delete_photo'),
                              style: DenimFonts.titleBody(context),
                            ),
                            actionButton: GestureDetector(
                              onTap: () => deleteAvatar(),
                              child: Text(
                                AppTranslations.of(context).text('delete'),
                                style: DenimFonts.titleLink(
                                  context,
                                  DenimColors.colorPrimary,
                                ),
                              ),
                            ),
                            cancelBtnTitle: AppTranslations.of(context)
                                .text('btn_сancel_title'),
                          );
                        },
                      );
                    },
                  ),
                ),
              )
            ],
          ),
          SizedBox(height: 40),
          Text(
            '${AppTranslations.of(context).text('photo_registration_step_description_3')} ${AppTranslations.of(context).text('photo_registration_step_description_4')}',
            style: DenimFonts.titleFootnote(
              context,
              DenimColors.colorBlackTertiary,
            ),
          ),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(children: [
              TextSpan(
                text: AppTranslations.of(context)
                    .text('photo_registration_step_description_5'),
                style: DenimFonts.titleFootnote(
                  context,
                  Colors.black,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () => _showPostingPhotoRulesDialog(),
              ),
              TextSpan(
                text: AppTranslations.of(context)
                    .text('photo_registration_step_description_6'),
                style: DenimFonts.titleFootnote(
                  context,
                  DenimColors.colorBlackTertiary,
                ),
              ),
            ]),
          ),
        ],
      );
    }
  }

  Widget buildPhotoRulesDescription(int gender) {
    return Column(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: DenimColors.colorBlackCell,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: EdgeInsets.all(12),
          child: Text(
            AppTranslations.of(context)
                .text('photo_registration_step_description_1'),
            textAlign: TextAlign.center,
            style: DenimFonts.titleBody(context),
          ),
        ),
        SizedBox(height: 24),
        Text(
          AppTranslations.of(context)
              .text('photo_registration_step_description_2'),
          style: DenimFonts.titleFootnote(
            context,
            DenimColors.colorBlackTertiary,
          ),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: 10),
        Text(
          AppTranslations.of(context)
              .text('photo_registration_step_description_3'),
          style: DenimFonts.titleFootnote(
            context,
            DenimColors.colorBlackTertiary,
          ),
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
              text: AppTranslations.of(context)
                  .text('photo_registration_step_description_4'),
              style: DenimFonts.titleFootnote(
                context,
                DenimColors.colorBlackTertiary,
              ),
            ),
            TextSpan(
              text: AppTranslations.of(context)
                  .text('photo_registration_step_description_5'),
              style: DenimFonts.titleFootnote(
                context,
                Colors.black,
              ),
              recognizer: TapGestureRecognizer()
                ..onTap = () => _showPostingPhotoRulesDialog(),
            ),
          ]),
        ),
        Text(
          AppTranslations.of(context)
              .text('photo_registration_step_description_6'),
          style: DenimFonts.titleFootnote(
            context,
            DenimColors.colorBlackTertiary,
          ),
        ),
        SizedBox(height: 30),
      ],
    );
  }

  _showPostingPhotoRulesDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return PostingPhotoRules();
      },
    );
  }

  deleteAvatar() async {
    ProfileData currentUser = await userDao.getCurrentUser(user);
    if (widget.reVerification) {
      ProfileUtils.deleteGalleryItem(
          currentUser.profile.photos, user, userDao, user.profile.avatar, () {
        deletedAvatarState(currentUser, null);
      });
    } else {
      return deletedAvatarState(currentUser, null);
    }
  }

  deletedAvatarState(ProfileData currentUser, UserPhoto avatarPhoto) {
    if (user.profile.avatar != null || currentUser.profile.avatar != null) {
      setState(() {
        user.profile.avatar = avatarPhoto;
      });
      Navigator.pop(context);
    }
  }

  loadAvatar([String loadFrom]) async {
    ImageFile image = loadFrom == 'camera'
        ? await ImageUtils.getPhotoFromCamera()
        : await ImageUtils.getPhotoFromGallery();

    if (image == null) return;
    ImageFile cropedImage = await locator.get<IImageEditor>().cropImage(image);
    if (cropedImage == null) return;
    // File compressImage = await ImageUtils.compressImage(cropedImage);
    // if (compressImage == null) return;
    print('img: ${image.name} path: ${image.path}');

    user.profile.avatar = UserPhoto(original: cropedImage);
    if (!mounted) return;
    setState(() {});
  }

  uploadAndHandleAvatar(ImageFile compressImage,
      [dynamic successCallback]) async {
    loading = true;
    if (!mounted) return;
    setState(() {});

    String imageId =
        await userApi.uploadImage(compressImage, loadingPhotoError);

    if (imageId != null) {
      await ProfileUtils.updateAvatar(imageId, () async {
        ProfileData currentUser = await userDao.getCurrentUser(App.me.user);
        if (currentUser.profile.photos != null &&
            currentUser.profile.photos.length > 1 &&
            !widget.reVerification) {
          ProfileUtils.registrationPhotosValidation();
        }

        if (successCallback != null) successCallback();
      }, loadingPhotoError);
    }

    if (user.profile.gender == 1) {
      firebaseAnalytics.sendAnalyticsEvent('reg_step_8_add_pht_сomp_M');
    } else {
      firebaseAnalytics.sendAnalyticsEvent('reg_step_8_add_pht_сomp_W');
    }
  }

  goToPhotoVerificationStep() {
    user.profile?.gender == 2
        ? widget.bloc.dispatch(GetPhotoVerificationStep())
        : widget.bloc.dispatch(GetCheckpointStep());
  }

  loadingPhotoError() {
    if (!mounted) return;
    setState(() {
      loading = false;
    });
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
          title: Text(AppTranslations.of(context).text('blocking_error')),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Text(
              AppTranslations.of(context).text('photo_loading_error'),
              style: DenimFonts.titleBody(
                context,
                DenimColors.colorBlackSecondary,
              ),
            ),
          ),
        );
      },
    );
  }
}
