import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/pages/report_user/report_user_description.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:flutter/material.dart';

class ReportUser extends StatefulWidget {
  final String userName;
  final String userId;

  ReportUser({
    this.userName,
    this.userId,
  });

  @override
  _ReportUserState createState() => _ReportUserState();
}

class _ReportUserState extends State<ReportUser> {
   Widget buildReportTypeTile(
    String title,
    GestureTapCallback onTap,
  ) {
    return ListTile(
      contentPadding: EdgeInsets.symmetric(horizontal: 3),
      onTap: onTap,
      title: Text(
        title,
        style: DenimFonts.titleBody(context),
      ),
    );
  }

  navigateToReportDescriptionScreen(String reportReason) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ReportUserDescription(reportReason: reportReason, userId: widget.userId,)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          DefaultHeader(
            backArrowIcon: Icon(
              DenimIcon.universal_close,
              color: Colors.black,
            ),
            visibleActionTitle: false,
            backArrowTap: () => Navigator.pop(context),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 10),
                          Text(
                            AppTranslations.of(context).text('report_reason_title'),
                            style: DenimFonts.titleCallout(context),
                            textAlign: TextAlign.start,
                          ),
                          SizedBox(height: 8),
                          Text(
                            '${widget.userName} ${AppTranslations.of(context).text('report_subtitle')}',
                            style: DenimFonts.titleFootnote(context, DenimColors.colorBlackTertiary),
                            textAlign: TextAlign.start,
                          ),
                          SizedBox(height: 24),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_1'), () => navigateToReportDescriptionScreen('inappropriatephoto')),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_2'), () => navigateToReportDescriptionScreen('inappropriateprofile')),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_3'), () => navigateToReportDescriptionScreen('offensivebehaviour')),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_4'), () => navigateToReportDescriptionScreen('spam')),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_5'), () => navigateToReportDescriptionScreen('fraud')),
                          buildReportTypeTile(AppTranslations.of(context).text('report_reason_6'), () => navigateToReportDescriptionScreen('other')),
                          SizedBox(height: 16),
                          Text(
                            // AppTranslations.of(context).text(' report_description_1'),
                            AppTranslations.of(context).text('loft_report_description_1'),
                            style: DenimFonts.titleFootnote(context, DenimColors.colorBlackTertiary),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
