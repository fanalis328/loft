import 'dart:async';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/denim_black/denim_black_trial.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_information.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:loft/model/purchase.dart';
import 'package:loft/locator.dart';

const String m3 = '3m';
const String w13dFree = '1w_3d_free';
const String m1 = '1m';
const String d3NoSub = '3d_nosub';

class DenimBlack extends StatefulWidget {
  @override
  _DenimBlackState createState() => _DenimBlackState();
}

class _DenimBlackState extends State<DenimBlack>
    with SingleTickerProviderStateMixin {
  bool _available = true;
  List<ProductDetails> products = [];
  List<PurchaseDetails> purchases = [];
  StreamSubscription _subscription;

  bool show = false;
  bool isVisible = true;
  String selectedItemId;
  ProductDetails selectedProduct;
  List<PurchaseItem> planSubscribtionList = [];
  List<Widget> productItemsList = [];
  PurchaseDetails trial;
  TrialPurchaseStatus trialPurchaseStatus;

  @override
  void initState() {
    _initialize();
    Timer(Duration(milliseconds: 2500), () {
      if (!mounted) return;
      setState(() {
        show = true;
      });
    });
    Timer(Duration(seconds: 3), () {
      if (!mounted) return;
      setState(() {
        isVisible = false;
      });
    });
    facebookAnalytics
        .sendFacebookAnalyticsEvent(facebookAnalytics.evenInitiatedCheckout);
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  _initialize() async {
    // TODO: implement for web
    if (kIsWeb) return;

    purchaseApi.getProducts();
    UpdatePurchaseData purchaseData;
    _available = await purchaseUtils.iap.isAvailable();
    if (_available) {
      products = await purchaseUtils.getProducts([m1, w13dFree, d3NoSub]);
      purchases = await purchaseUtils.getPastPurchases();
      _verifyPurchase();
    }
    _subscription =
        purchaseUtils.iap.purchaseUpdatedStream.listen((data) async {
      PurchaseDetails currentPurchase = data
          ?.firstWhere((purchase) => purchase.productID == selectedProduct.id);

      if (data != null && currentPurchase != null) {
        purchaseData = UpdatePurchaseData(
          owner: App.me.user.id,
          token: currentPurchase.billingClientPurchase.purchaseToken,
          product: currentPurchase.productID,
        );
        // set or update purchase to DB
        await purchaseUtils
            .checkAndUpdateCurrentDenimBlackPurchase(purchaseData);
        // update purchse on server
        purchaseApi.updatePurchase(
          purchaseData,
          (purchaseResponse) {
            purchaseUtils.afterProductPurchase(purchaseResponse,
                (ProfileData userData) async {
              if (currentPurchase.productID == w13dFree) {
                await purchaseDao.setTrialPurchaseStatus(
                    TrialPurchaseStatus(owner: App.me.user.id, status: true));
                trialPurchaseStatus =
                    await purchaseDao.getTrialPurchaseStatus();
              }
              yandexAnalytics.sendYandexAnalyticsEvent(
                yandexAnalytics.eventPurchaseSuccess,
              );
              firebaseAnalytics.sendAnalyticsEvent('purchase_success');
              //  show success popup;
              purchaseUtils.successPurchseDialog(context);
            });
          },
        );
      }
    });
  }

  _verifyPurchase() async {
    trial = purchaseUtils.hasPurchased(w13dFree, purchases);
    trialPurchaseStatus = await purchaseDao.getTrialPurchaseStatus();
    selectItem(w13dFree);
    selectedItemId = w13dFree;

    if (trialPurchaseStatus?.status == true) {
      productsList();
      return planSubscribtionList;
    }
  }

  consumePurchase() {
    PurchaseDetails purch = purchaseUtils.hasPurchased(d3NoSub, purchases);
    purchaseUtils.iap.consumePurchase(purch);
  }

  navigateToTrial() async {
    Navigator.pop(context);
    if (trialPurchaseStatus == null) {
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => DenimBlackTrial()));
    }
  }

  @override
  Widget build(BuildContext context) {
    planSubscribtionList = [
      PurchaseItem(
        id: 0,
        position: 0,
        productId: d3NoSub,
        tooltip: AppTranslations.of(context).text('denim_black_item_1_tooltip'),
        units: 3,
        title: AppTranslations.of(context).text('denim_black_item_1_title'),
      ),
      PurchaseItem(
        id: 1,
        position: 1,
        productId: w13dFree,
        tooltip: AppTranslations.of(context).text('denim_black_item_2_tooltip'),
        units: trialPurchaseStatus?.status == true ? 1 : 3,
        title: trialPurchaseStatus?.status == true
            ? AppTranslations.of(context).text('week')
            : AppTranslations.of(context).text('denim_black_item_2_title'),
        subtitle: trialPurchaseStatus?.status == true
            ? ''
            : AppTranslations.of(context).text('denim_black_item_2_subtitle_1'),
        subtitle2:
            AppTranslations.of(context).text('denim_black_item_2_subtitle_3'),
      ),
      PurchaseItem(
        id: 2,
        position: 2,
        productId: m1,
        tooltip: AppTranslations.of(context).text('denim_black_item_3_tooltip'),
        units: 1,
        title: AppTranslations.of(context).text('denim_black_item_3_title'),
      ),
    ];

    return Scaffold(
      backgroundColor: Colors.black,
      body: WillPopScope(
        onWillPop: () => navigateToTrial(),
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: SafeArea(
                    top: true,
                    child: ListView(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            AspectRatio(
                              aspectRatio: 2.1 / 1,
                              child: Container(
                                child: Image.asset(
                                  'assets/images/subscription-bg.jpg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                AspectRatio(
                                  aspectRatio: 3.3 / 1,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 56),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Image(
                                      image: AssetImage(
                                        'assets/images/section-subscription-logo-denimblack-white.png',
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 16),
                                Text(
                                  AppTranslations.of(context)
                                      .text('denim_black_main_description_1'),
                                  style: DenimFonts.titleBook(
                                      context, Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  AppTranslations.of(context)
                                      .text('denim_black_main_description_2'),
                                  style: DenimFonts.titleBook(
                                      context, Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  AppTranslations.of(context)
                                      .text('denim_black_main_description_3'),
                                  style: DenimFonts.titleBook(
                                      context, Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(height: 10),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    GestureDetector(
                                      child: Text(
                                        AppTranslations.of(context).text(
                                            'denim_black_more_info_title'),
                                        style: DenimFonts.titleFootnote(
                                            context, Colors.white60),
                                      ),
                                      onTap: () => aboutDenimBlackDialog(),
                                    ),
                                    SizedBox(width: 6),
                                    GestureDetector(
                                      child: Icon(
                                        DenimIcon.universal_help_small,
                                        color: Colors.white60,
                                        size: 16,
                                      ),
                                      onTap: () => aboutDenimBlackDialog(),
                                    )
                                  ],
                                ),
                                SizedBox(height: 64),
                                Container(
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  child: productsList(),
                                ),
                                Visibility(
                                  visible: !isVisible,
                                  child: AnimatedOpacity(
                                    opacity: isVisible ? 0 : 1,
                                    duration: Duration(milliseconds: 2100),
                                    child: Container(
                                      padding: EdgeInsets.all(16),
                                      child: subscriptionDisclaimers(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Positioned(
                              top: 40,
                              child: AnimatedOpacity(
                                opacity: isVisible ? 0 : 1,
                                duration: Duration(milliseconds: 2100),
                                child: IconButton(
                                  icon: Icon(DenimIcon.universal_close,
                                      color: Colors.white),
                                  onPressed: () => navigateToTrial(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: !isVisible,
                  child: AnimatedOpacity(
                    opacity: isVisible ? 0 : 1,
                    duration: Duration(milliseconds: 2100),
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: DenimButton(
                              title: AppTranslations.of(context)
                                  .text('btn_сontinue_title'),
                              onPressed: () {
                                purchaseUtils.buyProduct(selectedProduct);
                                yandexAnalytics.sendYandexAnalyticsEvent(
                                    yandexAnalytics.eventPurchase);
                                firebaseAnalytics
                                    .sendAnalyticsEvent('purchase');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              left: 0,
              top: 0,
              bottom: 0,
              right: 0,
              child: Container(
                child: Center(
                  child: AnimatedOpacity(
                    opacity: isVisible ? 1 : 0,
                    duration: Duration(seconds: 1),
                    child: IgnorePointer(
                      child: Container(
                        color: Colors.black,
                        constraints: BoxConstraints.expand(),
                        child: Center(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            child: AnimatedOpacity(
                              opacity: show ? 0 : 1,
                              duration: Duration(milliseconds: 300),
                              child: Image(
                                image: AssetImage(
                                  'assets/images/denim-black-mosh.gif',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  selectItem(String productId) {
    setState(() {
      selectedItemId = productId;
      selectedProduct =
          products.firstWhere((product) => product.id == productId);
    });
  }

  productsList() {
    productItemsList.clear();
    products.sort((a, b) => b.id.compareTo(a.id));
    for (var product in products) {
      for (var item in planSubscribtionList) {
        if (product.id == item.productId) {
          item.price = product.price;
          productItemsList.add(
            buildServiceProfileCard(
                item, selectedItemId == item.productId ? true : false),
          );
        }
      }
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: productItemsList,
    );
  }

  Column appInformationLinks() {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebViewInFlutter(
                  appBarTitle: AppTranslations.of(context)
                      .text('account_information_terms_title'),
                  link: 'https://loft-app.com/agreements',
                ),
              ),
            );
          },
          child: Text(
            AppTranslations.of(context).text('account_information_terms_title'),
            style: DenimFonts.titleFootnote(
                context, DenimColors.colorPrimary.withOpacity(0.7)),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebViewInFlutter(
                  appBarTitle: AppTranslations.of(context)
                      .text('account_information_privacy_policy_title'),
                  link: 'https://loft-app.com/privacy-policy',
                ),
              ),
            );
          },
          child: Text(
            AppTranslations.of(context)
                .text('account_information_privacy_policy_title'),
            style: DenimFonts.titleFootnote(
                context, DenimColors.colorPrimary.withOpacity(0.7)),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }

  subscriptionDisclaimers() {
    switch (selectedItemId) {
      case '3d_nosub':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('denim_black_item_1_disclaimer'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              AppTranslations.of(context)
                  .text('denim_black_item_1_disclaimer_1'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      case '1w_3d_free':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              trialPurchaseStatus?.status == true
                  ? AppTranslations.of(context)
                      .text('denim_black_item_2_disclaimer_1')
                  : AppTranslations.of(context)
                      .text('denim_black_item_2_disclaimer_trial_1'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              trialPurchaseStatus?.status == true
                  ? AppTranslations.of(context)
                      .text('denim_black_item_2_disclaimer_2')
                  : AppTranslations.of(context)
                      .text('denim_black_item_2_disclaimer_trial_2'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      case '1m':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('denim_black_item_3_disclaimer'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              AppTranslations.of(context)
                  .text('denim_black_item_3_disclaimer_1'),
              style: DenimFonts.titleFootnote(context, Colors.white60),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      default:
        return;
    }
  }

  tooltipColor(int id) {
    switch (id) {
      case 0:
        return DenimColors.colorPrimary;
        break;
      case 1:
        return DenimColors.colorSecondaryDefault;
        break;
      case 2:
        return DenimColors.colorSpecialPositiveDark;
        break;
      default:
        return DenimColors.colorSecondaryDefault;
    }
  }

  aboutDenimBlackDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10),
              Image.asset(
                'assets/images/icons/section-subscription-symbol.jpg',
                width: 64,
              ),
              SizedBox(height: 16),
              Text(
                AppTranslations.of(context)
                    .text('denim_black_popup_description_1'),
                style: DenimFonts.titleFootnote(context),
              ),
              SizedBox(height: 20),
              Text(
                AppTranslations.of(context)
                    .text('denim_black_popup_description_2'),
                style: DenimFonts.titleFootnote(context),
              ),
              SizedBox(height: 20),
            ],
          ),
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
        );
      },
    );
  }

  Widget buildServiceProfileCard(PurchaseItem item, bool isSelected) {
    return Expanded(
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(10),
        onTap: () => selectItem(item.productId),
        child: Stack(
          alignment: Alignment.center,
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    item.units.toString(),
                    style: TextStyle(
                      fontFamily: 'MontserratSemiBold',
                      fontWeight: FontWeight.w900,
                      fontSize: 44,
                      color: isSelected ? Colors.white : Colors.white60,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    item.title,
                    style: TextStyle(
                      fontFamily: 'MontserratSemiBold',
                      fontWeight: FontWeight.w900,
                      fontSize: 18,
                      color: isSelected ? Colors.white : Colors.white60,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Visibility(
                    visible: item.subtitle == '' ? false : true,
                    child: Text(
                      item.subtitle.toString(),
                      style: TextStyle(
                        fontFamily: 'MontserratSemiBold',
                        fontWeight: FontWeight.w900,
                        fontSize: 18,
                        color: isSelected ? Colors.white : Colors.white60,
                      ),
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(height: 7),
                  Visibility(
                    visible: item.productId == w13dFree &&
                            trialPurchaseStatus?.status == null
                        ? true
                        : false,
                    child: Text(
                      AppTranslations.of(context)
                          .text('denim_black_item_2_subtitle_2'),
                      style: DenimFonts.titleFootnote(
                          context, isSelected ? Colors.white : Colors.white60),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(
                    item.price +
                        '${item.subtitle2 != '' && trialPurchaseStatus?.status == null ? ' / ' + item.subtitle2 : ''}',
                    style: DenimFonts.titleFootnote(
                        context, isSelected ? Colors.white : Colors.white60),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 16),
                  Visibility(
                    visible: true,
                    child: isSelected
                        ? Icon(
                            DenimIcon.section_subscription_selection_selected,
                            color: Colors.white,
                            size: 16,
                          )
                        : Icon(
                            DenimIcon.section_subscription_selection_unselected,
                            color: Colors.white60,
                            size: 16,
                          ),
                  ),
                ],
              ),
            ),
            Positioned(
              top: -35,
              child: AnimatedOpacity(
                opacity: isSelected ? 1 : 0,
                duration: Duration(milliseconds: 400),
                child: Column(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: tooltipColor(item.id),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(6),
                      child: Text(
                        item.tooltip.toUpperCase(),
                        style: TextStyle(
                          fontFamily: 'MontserratSemiBold',
                          fontWeight: FontWeight.w900,
                          fontSize: 11,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Image.asset(
                      'assets/images/icons/triangle-${item.id}.png',
                      width: 25,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
