import 'dart:async';
import 'package:loft/data/app.dart';
import 'package:loft/denim_icons.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/purchase.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_information.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/utils/purchase.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:loft/locale/app_translations.dart';

class DenimBlackTrial extends StatefulWidget {
  @override
  _DenimBlackTrialState createState() => _DenimBlackTrialState();
}

class _DenimBlackTrialState extends State<DenimBlackTrial> {
  bool _avaible = true;
  List<ProductDetails> products = [];
  List<PurchaseDetails> purchases = [];
  StreamSubscription _subscription;
  String price;

  @override
  void initState() {
    _initialize();
    facebookAnalytics
        .sendFacebookAnalyticsEvent(facebookAnalytics.evenInitiatedCheckout);
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  _initialize() async {
    UpdatePurchaseData purchaseData;
    _avaible = await purchaseUtils.iap.isAvailable();
    if (_avaible) {
      products = await purchaseUtils.getProducts([w13dFree]);
      purchases = await purchaseUtils.getPastPurchases();
      setState(() {
        price = products?.first?.price;
      });
    }
    _subscription =
        purchaseUtils.iap.purchaseUpdatedStream.listen((data) async {
      PurchaseDetails currentPurchase = data?.first;
      if (data != null && currentPurchase != null) {
        purchaseData = UpdatePurchaseData(
          owner: App.me.user.id,
          token: currentPurchase.billingClientPurchase.purchaseToken,
          product: currentPurchase.productID,
        );
        // set purchase to DB
        await purchaseUtils.checkAndUpdateCurrentDenimBlackPurchase(purchaseData);
        // update purchse on server
        purchaseApi.updatePurchase(
          purchaseData,
          (purchaseResponse) {
            purchaseUtils.afterProductPurchase(purchaseResponse,
                (ProfileData userData) {
              yandexAnalytics.sendYandexAnalyticsEvent(
                yandexAnalytics.eventPurchaseSuccess,
              );
              firebaseAnalytics.sendAnalyticsEvent('purchase_success');
              //  show success popup;
              purchaseUtils.successPurchseDialog(context);
            });
          },
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SafeArea(
            top: true,
            child: Container(
              height: 56,
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(DenimIcon.universal_close, color: Colors.black),
                    onPressed: () => Navigator.pop(context),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 16),
                  RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: AppTranslations.of(context).text('denim_black_trial_title_1') + " ",
                        style: TextStyle(
                          fontFamily: 'MontserratAlternatesBlack',
                          fontSize: 30,
                          color: DenimColors.colorBlackTertiary,
                        ),
                      ),
                      TextSpan(
                        text: AppTranslations.of(context).text('denim_black_trial_title_2'),
                        style: TextStyle(
                          fontFamily: 'MontserratAlternatesBlack',
                          fontSize: 30,
                          color: Colors.black,
                        ),
                      ),
                    ]),
                  ),
                  SizedBox(height: 32),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/icons/section-premium-trial-advantage-1.jpg',
                        width: 56,
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                               AppTranslations.of(context).text('denim_black_trial_item_1_title'),
                              style:
                                  DenimFonts.titleBtnSecondaryDefault(context),
                            ),
                            Text(
                              AppTranslations.of(context).text('denim_black_trial_item_1_subtitle'),
                              style: DenimFonts.titleFootnote(context),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 32),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/icons/section-premium-trial-advantage-2.jpg',
                        width: 56,
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              AppTranslations.of(context).text('denim_black_trial_item_2_title'),
                              style: DenimFonts.titleBtnSecondaryDefault(context),
                            ),
                            Text(
                              AppTranslations.of(context).text('denim_black_trial_item_2_subtitle'),
                              style: DenimFonts.titleFootnote(context),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 32),
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/icons/section-premium-trial-advantage-3.jpg',
                        width: 56,
                      ),
                      SizedBox(width: 12),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              AppTranslations.of(context).text('denim_black_trial_item_3_title'),
                              style: DenimFonts.titleBtnSecondaryDefault(context),
                            ),
                            Text(
                              AppTranslations.of(context).text('loft_premium_trial_item_3_subtitle'),
                              style: DenimFonts.titleFootnote(context),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 32),
                  Text(
                    AppTranslations.of(context)
                        .text('loft_premium_item_2_disclaimer_trial_1'),
                    style: DenimFonts.titleCaption(
                        context, DenimColors.colorBlackSecondary),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 20),
                  Text(
                    AppTranslations.of(context)
                        .text('loft_premium_item_2_disclaimer_trial_2'),
                    style: DenimFonts.titleCaption(
                        context, DenimColors.colorBlackSecondary),
                    textAlign: TextAlign.left,
                  ),
                  SizedBox(height: 20),
                  RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: '${AppTranslations.of(context).text('see_details')} ',
                          style: DenimFonts.titleCaption(context, DenimColors.colorBlackSecondary)
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'account_information_terms_title'),
                          style: DenimFonts.titleCaption(
                        context, DenimColors.colorPrimary),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewInFlutter(
                                    appBarTitle: AppTranslations.of(context)
                                        .text(
                                            'account_information_terms_title'),
                                    link: 'https://loft-app.com/agreements',
                                  ),
                                ),
                              );
                            },
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'initial_registration_step_description_part_3'),
                          style: DenimFonts.titleCaption(
                        context, DenimColors.colorBlackSecondary),
                        ),
                        TextSpan(
                          text: AppTranslations.of(context).text(
                              'account_information_privacy_policy_title'),
                          style: DenimFonts.titleCaption(
                        context, DenimColors.colorPrimary),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => WebViewInFlutter(
                                    appBarTitle: AppTranslations.of(context).text(
                                        'account_information_privacy_policy_title'),
                                    link:
                                        'https://loft-app.com/privacy-policy',
                                  ),
                                ),
                              );
                            },
                        ),
                      ]),
                    ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: SizedBox(
                    height: 50,
                    child: FlatButton(
                      onPressed: () => purchaseUtils.buyProduct(products.first),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            AppTranslations.of(context).text('denim_black_trial_btn'),
                            style: DenimFonts.titleBtnSmallSecondary(context, Colors.white),
                          ),
                          Text(
                            '${AppTranslations.of(context).text('denim_black_item_2_subtitle_2')} $price / ${AppTranslations.of(context).text('week')}',
                            style: DenimFonts.titleBtnSmallSecondary(
                                context, Colors.white),
                          ),
                        ],
                      ),
                      color: DenimColors.colorPrimary,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      disabledColor: DenimColors.colorBlackTertiary,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
