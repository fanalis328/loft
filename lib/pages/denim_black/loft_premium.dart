import 'dart:async';
import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/denim_black/denim_black_trial.dart';
import 'package:loft/pages/tabs/profile/profile_settings/profile_settings_information.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/button.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:loft/model/purchase.dart';
import 'package:loft/locator.dart';
import 'package:loft/widgets/default_header.dart';
import 'package:loft/pages/denim_black/loft_premium_slider.dart';

const String m3 = '3m';
const String w13dFree = '1w_3d_free';
const String m1 = '1m';
const String d3NoSub = '3d_nosub';

class LoftPremium extends StatefulWidget {
  @override
  _LoftPremiumState createState() => _LoftPremiumState();
}

class _LoftPremiumState extends State<LoftPremium> {
  bool _available = true;
  List<ProductDetails> products = [];
  List<PurchaseDetails> purchases = [];
  StreamSubscription _subscription;

  String selectedItemId;
  ProductDetails selectedProduct;
  List<PurchaseItem> planSubscribtionList = [];
  List<Widget> productItemsList = [];
  PurchaseDetails trial;
  TrialPurchaseStatus trialPurchaseStatus;

  @override
  void initState() {
    _initialize();
    facebookAnalytics
        .sendFacebookAnalyticsEvent(facebookAnalytics.evenInitiatedCheckout);
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  _initialize() async {
    // TODO: implement for web
    if (kIsWeb) return;

    purchaseApi.getProducts();
    UpdatePurchaseData purchaseData;
    _available = await purchaseUtils.iap.isAvailable();
    if (_available) {
      products = await purchaseUtils.getProducts([m1, w13dFree, d3NoSub]);
      purchases = await purchaseUtils.getPastPurchases();
      _verifyPurchase();
    }
    _subscription =
        purchaseUtils.iap.purchaseUpdatedStream.listen((data) async {
      PurchaseDetails currentPurchase = data
          ?.firstWhere((purchase) => purchase.productID == selectedProduct.id);

      if (data != null && currentPurchase != null) {
        purchaseData = UpdatePurchaseData(
          owner: App.me.user.id,
          token: currentPurchase.billingClientPurchase.purchaseToken,
          product: currentPurchase.productID,
        );
        // set or update purchase to DB
        await purchaseUtils
            .checkAndUpdateCurrentDenimBlackPurchase(purchaseData);
        // update purchse on server
        purchaseApi.updatePurchase(
          purchaseData,
          (purchaseResponse) {
            purchaseUtils.afterProductPurchase(purchaseResponse,
                (ProfileData userData) async {
              if (currentPurchase.productID == w13dFree) {
                await purchaseDao.setTrialPurchaseStatus(
                    TrialPurchaseStatus(owner: App.me.user.id, status: true));
                trialPurchaseStatus =
                    await purchaseDao.getTrialPurchaseStatus();
              }
              yandexAnalytics.sendYandexAnalyticsEvent(
                yandexAnalytics.eventPurchaseSuccess,
              );
              firebaseAnalytics.sendAnalyticsEvent('purchase_success');
              //  show success popup;
              purchaseUtils.successPurchseDialog(context);
            });
          },
        );
      }
    });
  }

  _verifyPurchase() async {
    trial = purchaseUtils.hasPurchased(w13dFree, purchases);
    trialPurchaseStatus = await purchaseDao.getTrialPurchaseStatus();
    selectItem(w13dFree);
    selectedItemId = w13dFree;

    if (trialPurchaseStatus?.status == true) {
      productsList();
      return planSubscribtionList;
    }
  }

  consumePurchase() {
    PurchaseDetails purch = purchaseUtils.hasPurchased(d3NoSub, purchases);
    purchaseUtils.iap.consumePurchase(purch);
  }

  navigateToTrial() async {
    Navigator.pop(context);
    if (trialPurchaseStatus == null) {
      return Navigator.push(
          context, MaterialPageRoute(builder: (context) => DenimBlackTrial()));
    }
  }

  @override
  Widget build(BuildContext context) {
    planSubscribtionList = [
      PurchaseItem(
        id: 0,
        position: 0,
        productId: d3NoSub,
        tooltip: AppTranslations.of(context).text('denim_black_item_1_tooltip'),
        units: 3,
        title: AppTranslations.of(context).text('denim_black_item_1_title'),
      ),
      PurchaseItem(
        id: 1,
        position: 1,
        productId: w13dFree,
        tooltip: AppTranslations.of(context).text('denim_black_item_2_tooltip'),
        units: trialPurchaseStatus?.status == true ? 1 : 3,
        title: trialPurchaseStatus?.status == true
            ? AppTranslations.of(context).text('week')
            : AppTranslations.of(context).text('denim_black_item_2_title'),
        subtitle: trialPurchaseStatus?.status == true
            ? ''
            : AppTranslations.of(context).text('denim_black_item_2_subtitle_1'),
        subtitle2:
            AppTranslations.of(context).text('denim_black_item_2_subtitle_3'),
      ),
      PurchaseItem(
        id: 2,
        position: 2,
        productId: m1,
        tooltip: AppTranslations.of(context).text('denim_black_item_3_tooltip'),
        units: 1,
        title: AppTranslations.of(context).text('denim_black_item_3_title'),
      ),
    ];

    return Scaffold(
      body: WillPopScope(
        onWillPop: () => navigateToTrial(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            DefaultHeader(
              mainTitle: AppTranslations.of(context).text('loft_premium_header_title'),
              visibleActionTitle: false,
              backArrowTap: () => navigateToTrial(),
            ),
            Expanded(
              child: ListView(
                children: <Widget>[
                  LoftPremiumSlider(),
                  SizedBox(height: 64),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    child: productsList(),
                  ),
                  // Container(
                  //   padding: EdgeInsets.symmetric(horizontal: 5),
                  //   child: Row(
                  //     children: planSubscribtionList
                  //         .map((item) => buildServiceProfileCard(item, false))
                  //         .toList(),
                  //   ),
                  // ),
                  Container(
                    padding: EdgeInsets.all(16),
                    child: subscriptionDisclaimers(),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: DenimButton(
                      title: AppTranslations.of(context)
                          .text('btn_сontinue_title'),
                      onPressed: () {
                        purchaseUtils.buyProduct(selectedProduct);
                        yandexAnalytics.sendYandexAnalyticsEvent(
                            yandexAnalytics.eventPurchase);
                        firebaseAnalytics.sendAnalyticsEvent('purchase');
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  selectItem(String productId) {
    setState(() {
      selectedItemId = productId;
      selectedProduct =
          products.firstWhere((product) => product.id == productId);
    });
  }

  productsList() {
    productItemsList.clear();
    products.sort((a, b) => b.id.compareTo(a.id));
    for (var product in products) {
      for (var item in planSubscribtionList) {
        if (product.id == item.productId) {
          item.price = product.price;
          productItemsList.add(
            buildServiceProfileCard(
                item, selectedItemId == item.productId ? true : false),
          );
        }
      }
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: productItemsList,
    );
  }

  Column appInformationLinks() {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebViewInFlutter(
                  appBarTitle: AppTranslations.of(context)
                      .text('account_information_terms_title'),
                  link: 'https://loft-app.com/agreements',
                ),
              ),
            );
          },
          child: Text(
            AppTranslations.of(context).text('account_information_terms_title'),
            style: DenimFonts.titleFootnote(
                context, DenimColors.colorPrimary.withOpacity(0.7)),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(height: 10),
        GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WebViewInFlutter(
                  appBarTitle: AppTranslations.of(context)
                      .text('account_information_privacy_policy_title'),
                  link: 'https://loft-app.com/privacy-policy',
                ),
              ),
            );
          },
          child: Text(
            AppTranslations.of(context)
                .text('account_information_privacy_policy_title'),
            style: DenimFonts.titleFootnote(
                context, DenimColors.colorPrimary.withOpacity(0.7)),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }

  subscriptionDisclaimers() {
    switch (selectedItemId) {
      case '3d_nosub':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('loft_premium_item_1_disclaimer'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              AppTranslations.of(context)
                  .text('loft_premium_item_1_disclaimer_1'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      case '1w_3d_free':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              trialPurchaseStatus?.status == true
                  ? AppTranslations.of(context)
                      .text('loft_premium_item_2_disclaimer_1')
                  : AppTranslations.of(context)
                      .text('loft_premium_item_2_disclaimer_trial_1'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              trialPurchaseStatus?.status == true
                  ? AppTranslations.of(context)
                      .text('loft_premium_item_2_disclaimer_2')
                  : AppTranslations.of(context)
                      .text('loft_premium_item_2_disclaimer_trial_2'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      case '1m':
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              AppTranslations.of(context).text('loft_premium_item_2_disclaimer_1'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            Text(
              AppTranslations.of(context)
                  .text('loft_premium_item_2_disclaimer_2'),
              style: DenimFonts.titleFootnote(
                  context, DenimColors.colorBlackSecondary),
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10),
            appInformationLinks(),
          ],
        );
        break;
      default:
        return;
    }
  }

  tooltipColor(int id) {
    switch (id) {
      case 0:
        return DenimColors.colorPrimary;
        break;
      case 1:
        return DenimColors.colorSecondaryDefault;
        break;
      case 2:
        return DenimColors.colorSpecialPositiveDark;
        break;
      default:
        return DenimColors.colorSecondaryDefault;
    }
  }

  aboutDenimBlackDialog() {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaultAlertDialog(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10),
              Image.asset(
                'assets/images/icons/section-subscription-symbol.jpg',
                width: 64,
              ),
              SizedBox(height: 16),
              Text(
                AppTranslations.of(context)
                    .text('denim_black_popup_description_1'),
                style: DenimFonts.titleFootnote(context),
              ),
              SizedBox(height: 20),
              Text(
                AppTranslations.of(context)
                    .text('denim_black_popup_description_2'),
                style: DenimFonts.titleFootnote(context),
              ),
              SizedBox(height: 20),
            ],
          ),
          cancelBtnTitle: AppTranslations.of(context).text('clear'),
        );
      },
    );
  }

  Widget buildServiceProfileCard(PurchaseItem item, bool isSelected) {
    return Expanded(
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.white.withOpacity(0.1),
        borderRadius: BorderRadius.circular(10),
        onTap: () => selectItem(item.productId),
        child: Container(
          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
          decoration: BoxDecoration(
            border: Border.all(color: isSelected ? DenimColors.colorPrimary : Colors.white, width: 4),
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Stack(
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      item.units.toString(),
                      style: TextStyle(
                        fontFamily: 'MontserratSemiBold',
                        fontWeight: FontWeight.w900,
                        fontSize: 44,
                        color: isSelected
                            ? Colors.black
                            : DenimColors.colorBlackSecondary,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      item.title,
                      style: TextStyle(
                        fontFamily: 'MontserratSemiBold',
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                        color: isSelected
                            ? Colors.black
                            : DenimColors.colorBlackSecondary,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Visibility(
                      visible: item.subtitle == '' ? false : true,
                      child: Text(
                        item.subtitle.toString(),
                        style: TextStyle(
                          fontFamily: 'MontserratSemiBold',
                          fontWeight: FontWeight.w900,
                          fontSize: 16,
                          color: isSelected
                              ? Colors.black
                              : DenimColors.colorBlackSecondary,
                        ),
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    SizedBox(height: 7),
                    Visibility(
                      visible: item.productId == w13dFree &&
                              trialPurchaseStatus?.status == null
                          ? true
                          : false,
                      child: Text(
                        AppTranslations.of(context)
                            .text('denim_black_item_2_subtitle_2'),
                        style: DenimFonts.titleBody(
                            context,
                            isSelected
                                ? Colors.black
                                : DenimColors.colorBlackSecondary),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Text(
                      item.price +
                          '${item.subtitle2 != '' && trialPurchaseStatus?.status == null ? ' / ' + item.subtitle2 : ''}',
                      style: DenimFonts.titleBody(
                          context,
                          isSelected
                              ? Colors.black
                              : DenimColors.colorBlackSecondary),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Positioned(
                top: -27,
                child: AnimatedOpacity(
                  opacity: isSelected ? 1 : 0,
                  duration: Duration(milliseconds: 0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 20,
                        decoration: BoxDecoration(
                          // color: tooltipColor(item.id),
                          color: DenimColors.colorPrimary,
                          borderRadius: BorderRadius.circular(40),
                        ),
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: Center(
                          child: Text(
                            item.tooltip.toUpperCase(),
                            style: TextStyle(
                              fontFamily: 'MontserratSemiBold',
                              fontWeight: FontWeight.w900,
                              fontSize: 10,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
