import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations.dart';
import 'package:loft/model/websocket/account.dart';
import 'package:loft/pages/registration_steps/birthday_registration.dart';
import 'package:loft/pages/registration_steps/checkpoint_registration.dart';
import 'package:loft/pages/registration_steps/citizenship_registration.dart';
import 'package:loft/pages/registration_steps/country_registration.dart';
import 'package:loft/pages/registration_steps/gender_registration.dart';
import 'package:loft/pages/registration_steps/height_registration.dart';
import 'package:loft/pages/registration_steps/initial_registration.dart';
import 'package:loft/pages/registration_steps/location_registration.dart';
import 'package:loft/pages/registration_steps/name_registration.dart';
import 'package:loft/pages/registration_steps/password_registration.dart';
import 'package:loft/pages/registration_steps/photo_registration.dart';
import 'package:loft/pages/registration_steps/photo_verification_end_registration.dart';
import 'package:loft/pages/registration_steps/photo_verification_registration.dart';
import 'package:loft/pages/registration_steps/question_1.dart';
import 'package:loft/blocs/registration_bloc/bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_bloc.dart';
import 'package:loft/blocs/registration_bloc/registration_state.dart';
import 'package:loft/pages/registration_steps/repeat_password_registration.dart';
import 'package:loft/pages/registration_steps/sms_code_registration.dart';
import 'package:loft/pages/registration_steps/user_profile_description_registration.dart';
import 'package:loft/pages/registration_steps/weight_registration.dart';
import 'package:loft/pages/tabs_page.dart';
import 'package:loft/theme/theme_colors.dart';
import 'package:loft/theme/theme_typography.dart';
import 'package:loft/widgets/circular_progress.dart';
import 'package:loft/widgets/default_dialog.dart';
import 'package:loft/widgets/snackbar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:loft/pages/registration_steps/search_gender.dart';
import '../locator.dart';

class RegistrationPage extends StatefulWidget {
  final bool getGenderStep;

  RegistrationPage({
    this.getGenderStep = false,
  });

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final registrationBloc = RegistrationBloc();

  @override
  void initState() {
    if (widget.getGenderStep) {
      registrationBloc.dispatch(GetGenderStep());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () => showDialog<bool>(
          context: context,
          builder: (c) => DefaultAlertDialog(
            title: Text(AppTranslations.of(context)
                .text('blocking_logout_on_registration')),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                AppTranslations.of(context)
                    .text('blocking_logout_on_registration_subtitle'),
                style: DenimFonts.titleBody(
                  context,
                  DenimColors.colorBlackSecondary,
                ),
              ),
            ),
            cancelBtnCallBack: () {
              Navigator.pop(c, true);
              authApi.logout();
            },
            cancelBtnTitle: AppTranslations.of(context).text('account_logout'),
            actionButton: GestureDetector(
              child: Text(
                AppTranslations.of(context).text('btn_сontinue_title'),
                style: DenimFonts.titleLink(
                  context,
                  DenimColors.colorPrimary,
                ),
              ),
              onTap: () => Navigator.pop(c, false),
            ),
          ),
        ),
        child: BlocProvider(
          bloc: registrationBloc,
          child: BlocBuilder(
            bloc: registrationBloc,
            builder: buidRegistrationPage,
          ),
        ),
      ),
    );
  }

  Widget buidRegistrationPage(BuildContext context, RegistrationState state) {
    if (state is LoadingRegistrationStepState)
      return CircularLoadingIndicator();
    if (state is InitialRegistrationState)
      return InitRegistrationStep(bloc: registrationBloc);
    if (state is SmsCodeRegistrationState)
      return SmsCodeRegistrationStep(
        bloc: registrationBloc,
        smsCode: state.smsCode,
        phone: state.phone,
        status: state.status,
      );
    // if (state is PasswordRegistrationState)
    //   return PasswordRegistrationStep(bloc: registrationBloc);
    // if (state is PasswordRepeatRegistrationState)
    //   return RepeatPasswordRegistrationStep(
    //     bloc: registrationBloc,
    //     password: state.password,
    //   );
    if (state is GenderRegistrationState)
      return GenderRegistrationStep(bloc: registrationBloc);
    if (state is SearchGenderRegistrationState)
      return SearchGenderRegistrationStep(bloc: registrationBloc);
    if (state is NameRegistrationState)
      return NameRegistrationStep(bloc: registrationBloc);
    if (state is BirthDayRegistrationState)
      return BirthDayRegistrationStep(bloc: registrationBloc);
    if (state is CountryRegistrationState)
      return CountryRegistrationStep(bloc: registrationBloc);
    if (state is CityRegistrationState)
      return LocationRegistrationStep(
        bloc: registrationBloc,
        country: state.country,
      );
    // if (state is CitizenshipRegistrationState)
    //   return CitizenshipRegistrationStep(bloc: registrationBloc);
    if (state is PhotoRegistrationState)
      return PhotoRegistrationStep(bloc: registrationBloc);
    if (state is PhotoVerificationRegistrationState)
      return PhotoVerificationStep(bloc: registrationBloc);
    if (state is PhotoVerificationEndRegistrationState)
      return PhotoVerificationEndStep(
        imageFile: state.imageFile,
        bloc: registrationBloc,
      );
    if (state is CheckpointRegistrationState)
      return CheckpointRegistrationStep(
          bloc: registrationBloc,
          title: AppTranslations.of(context)
              .text('checkpoint_registration_step_title'),
          description: AppTranslations.of(context)
              .text('checkpoint_registration_step_description'),
          titleSecondary: AppTranslations.of(context)
              .text('checkpoint_registration_step_subtitle'),
          buttonTitle: AppTranslations.of(context).text('btn_сontinue_title'),
          callback: () {
            registrationBloc.dispatch(GetHeightStep());
            switch (App?.me?.user?.profile?.gender) {
              case 1:
                yandexAnalytics.sendYandexAnalyticsEvent(
                    yandexAnalytics.eventRegStep10proSuccessM);
                firebaseAnalytics
                    .sendAnalyticsEvent('reg_step_10_pro_success_M');
                facebookAnalytics.sendFacebookAnalyticsEvent(
                    facebookAnalytics.eventCompletedRegistrationM);
                break;
              case 2:
                yandexAnalytics.sendYandexAnalyticsEvent(
                    yandexAnalytics.eventRegStep10proSuccessW);
                firebaseAnalytics
                    .sendAnalyticsEvent('reg_step_10_pro_success_W');
                facebookAnalytics.sendFacebookAnalyticsEvent(
                    facebookAnalytics.eventSubmitApplicationW);
                break;
              default:
                return;
            }
          });
    if (state is HeightRegistrationState)
      return HeightRegistrationStep(bloc: registrationBloc);
    if (state is WeightRegistrationState)
      return WeightRegistrationStep(bloc: registrationBloc);
    // Question1RegistrationState
    if (state is Question1RegistrationState)
      return Question1(bloc: registrationBloc);
    //
    if (state is UserProfileDescriptionRegistrationState)
      return UserProfileDescriptionRegistrationStep(bloc: registrationBloc);
    if (state is FinalRegistrationState)
      return CheckpointRegistrationStep(
        bloc: registrationBloc,
        title: AppTranslations.of(context).text('registration_end_step_title'),
        titleSecondary: '',
        description:
            AppTranslations.of(context).text('registration_end_step_subtitle'),
        buttonTitle:
            AppTranslations.of(context).text('registration_end_step_btn_title'),
        callback: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => TabsPage()));
          firebaseAnalytics.sendAnalyticsEvent('complete_registration_M');
          facebookAnalytics.sendFacebookAnalyticsEventWithParams(
              facebookAnalytics.standardEventCompletedRegistration,
              0.00,
              'USD');
          yandexAnalytics
              .sendYandexAnalyticsEvent(yandexAnalytics.eventRegSuccessM);
        },
      );

    return Text(AppTranslations.of(context).text('blocking_error'));
  }

  endRegistarationCallback([Map data]) {
    if (data['ctrl']['code'] == 200) {
      userApi.getAccount(updateAccount);
    } else {
      Snackbar.showSnackbar(context);
    }
  }

  updateAccount(Map data) async {
    ProfileData user = App.me.user;
    if (data['ctrl']['code'] == 200) {
      var profile = data['data']['profile'];

      user.id = data['data']['id'];
      user.profile.name = profile['name'];
      user.profile.gender = profile['gender'];
      user.profile.birthday = profile['birthday'];
      user.profile.height = profile['height'];
      user.profile.weight = profile['weight'];
      user.profile.zodiac = profile['zodiac'];
      user.profile.citizenship = profile['citizenship']['id'];
      user.profile.city = profile['city']['id'];
      user.profile.about = profile['about'];
      user.profile.photos = profile['photos'] == null
          ? null
          : List<UserPhoto>.from(
              profile['photos']?.map((item) => UserPhoto.fromJson(item)));
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => TabsPage()));
    }
  }
}
