import 'package:loft/data/app.dart';
import 'package:loft/locale/app_translations_delegate.dart';
import 'package:loft/locator.dart';
import 'package:loft/model/push.dart';
import 'package:loft/services/navigation_service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:loft/router.dart' as router;
import 'package:loft/route_paths.dart' as routes;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();

  /// Initializing the AppMetrica SDK.
  await yandexAnalytics.initialize();

  runApp(Denim());
}

class Denim extends StatefulWidget {
  @override
  _DenimState createState() => _DenimState();
}

class _DenimState extends State<Denim> with WidgetsBindingObserver {
  AppTranslationsDelegate _newLocaleDelegate;
  App app = App.me;
  BuildContext scaffoldContext;
  final List<Message> messages = [];
  String globalLibraryVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    // Init communication
    pushNotificationService.initialize();
    pushNotificationService
        .getToken()
        .then((token) => App.me.initWebsocketConnection(true, token));

    // Locale
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: Locale('ru'));
    app.onLocaleChanged = onLocaleChange;

    // Yandex
    initLibrarayVersionState();

    // Facebook
    facebookAnalytics.activatedAppEvent();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.paused:
        App.me.close();
        break;
      case AppLifecycleState.resumed:
        App.me.initWebsocketConnection(false);
        break;
      case AppLifecycleState.inactive:
        App.me.close();
        break;
      case AppLifecycleState.detached:
        App.me.close();
        break;
    }
  }

  Future<void> initLibrarayVersionState() async {
    String libraryVersion;
    try {
      libraryVersion = await yandexAnalytics.getLibraryVersion();
    } on PlatformException {
      libraryVersion = 'Failed to get library version.';
    }

    if (!mounted) {
      return;
    }

    setState(() {
      globalLibraryVersion = libraryVersion;
    });
  }

  static const MaterialColor swatchColor = const MaterialColor(
    0xFFFF4870,
    const <int, Color>{
      50: Color.fromRGBO(255, 72, 112, 0.1),
      100: Color.fromRGBO(255, 72, 112, 0.2),
      200: Color.fromRGBO(255, 72, 112, 0.3),
      300: Color.fromRGBO(255, 72, 112, 0.4),
      400: Color.fromRGBO(255, 72, 112, 0.5),
      500: Color.fromRGBO(255, 72, 112, 0.6),
      600: Color.fromRGBO(255, 72, 112, 0.7),
      700: Color.fromRGBO(255, 72, 112, 0.8),
      800: Color.fromRGBO(255, 72, 112, 0.9),
      900: Color.fromRGBO(255, 72, 112, 1),
    },
  );

  @override
  Widget build(BuildContext context) {
    scaffoldContext = context;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: swatchColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      title: 'Denim',
      navigatorKey: locator<NavigationService>().navigatorKey,
      // TODO: implement web observer
      navigatorObservers:
          !kIsWeb ? <NavigatorObserver>[firebaseAnalytics.observer] : [],
      onGenerateRoute: router.generateRoute,
      initialRoute: routes.SplashScreenRoute,
      localizationsDelegates: [
        _newLocaleDelegate,
        AppTranslationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: app.supportedLocales(),
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
