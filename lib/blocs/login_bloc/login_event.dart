import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const <dynamic>[]]) : super(props);
}

class GetLoginSmsCode extends LoginEvent {
  final String code;
  GetLoginSmsCode({this.code});
}
