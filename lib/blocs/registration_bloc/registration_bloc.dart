import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:loft/services/image_picker/image_file_model.dart';
import './bloc.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  @override
  RegistrationState get initialState => InitialRegistrationState();

  @override
  Stream<RegistrationState> mapEventToState(
    RegistrationEvent event,
  ) async* {
    if (event is GetInitialStep) yield InitialRegistrationState();
    // if (event is GetSmsCode) {
    //   yield LoadingRegistrationStepState();
    //   // var smsCode = await authDao.getSmsCode();
    //   // print(['BLOC SMS GET:', smsCode]);
    //   // App.me.user.phone = event.phone;
    //   // yield SmsCodeRegistrationState(smsCode: smsCode.toString());
    // }
    if (event is GetSmsCodeStep) {
      yield SmsCodeRegistrationState(smsCode: event.code, phone: event.phone, status: event.status);
    }

    // if (event is GetPasswordStep) yield PasswordRegistrationState();

    // if (event is GetRepeatPasswordStep) {
    //   yield PasswordRepeatRegistrationState(password: event.password);
    // }

    if (event is GetLoadingState) yield LoadingRegistrationStepState();

    if (event is GetGenderStep) {
      yield LoadingRegistrationStepState();
      yield GenderRegistrationState();
    }

    if(event is GetSearchGenderStep) {
      yield SearchGenderRegistrationState();
    }

    if (event is GetNameStep) yield NameRegistrationState();
    if (event is GetBirthDayStep) yield BirthDayRegistrationState();
    if (event is GetCountryStep) yield CountryRegistrationState();
    if (event is GetCityStep) yield CityRegistrationState(country: event.country);
    // if (event is GetCitizenshipStep) yield CitizenshipRegistrationState();
    if (event is GetPhotoStep) yield PhotoRegistrationState();
    if (event is GetPhotoVerificationStep)
      yield PhotoVerificationRegistrationState();
    if (event is GetPhotoVerificationEndStep) {
      yield LoadingRegistrationStepState();
      var image = await _file(event.imageFile);
      yield PhotoVerificationEndRegistrationState(imageFile: image);
    }
    if (event is GetCheckpointStep) yield CheckpointRegistrationState();
    if (event is GetHeightStep) yield HeightRegistrationState();
    if (event is GetWeightStep) yield WeightRegistrationState();
    // questions
    if (event is GetQuestion1Step) yield Question1RegistrationState();
    // 
    if (event is GetUserProfileDescriptionStep)
      yield UserProfileDescriptionRegistrationState();
    if (event is GetFinalRegistrationStep) yield FinalRegistrationState();
  }

  Future<ImageFile> _file(ImageFile imageFile) {
    return Future.delayed(Duration(milliseconds: 0), () => imageFile);
  }
}
