import 'config/enviroment.dart';
import 'main.dart' as app;

void main() {
  Constants.setEnviroment(Enviroment.prod);
  app.main();
}
